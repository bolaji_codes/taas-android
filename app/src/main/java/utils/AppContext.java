package utils;

import android.app.Application;
import android.content.Context;



public class AppContext extends Application{
    private static AppContext instance;
    public static AppContext getInstance(){
        return instance;
    }

    public static Context getContext(){
        return instance;
    }

    @Override
    public void onCreate(){
        instance = this;
        super.onCreate();
    }
}
