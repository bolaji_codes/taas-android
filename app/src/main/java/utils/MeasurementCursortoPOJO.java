package utils;

import android.database.Cursor;

import com.bolaandroid.data.model.Measurement;
import com.bolaandroid.provider.MeasurementProvider;

import java.util.ArrayList;


public class MeasurementCursortoPOJO {
    Cursor cursor;

    public MeasurementCursortoPOJO(Cursor cursor){
        this.cursor=cursor;
    }
    public Measurement convert(){
            Measurement measurement;
            measurement=new Measurement(
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_NAME))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_GENDER))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_SHOULDER))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_CHEST))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_NECK))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_HEAD))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_TOPHALFLENGTH))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_TOPLENGTH))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_LONGSLEEVE))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_SHORTSLEEVE))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_ROUNDSLEEVE))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_ARMHOLE))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_HANDCUFF))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_WAIST))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_BUM))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_LAP))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_ANKLE))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_KNEELENGTH))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_TROUSERLNGTH))),
            getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_FLAB))),
                    getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_NIPPLE2NIPPLE))),
                    getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_UNDER_BUST))),
                    getStringz(cursor.getString(cursor.getColumnIndex(MeasurementProvider.COLUMN_FULL_LENGTH)))
            );



        return measurement;

    }

    public String getStringz(String name){
        if (name==null){
            return String.valueOf(0);
        }
        return name;

    }
}
