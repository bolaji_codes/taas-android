package utils;

import com.bolaandroid.data.model.remote.RetrofitClient;
import com.bolaandroid.data.model.remote.TAASService;

public class ApiUtils {

    public static final String BASE_URL = "https://www.taasafrica.com/";

    public static TAASService getTAASService() {
        return RetrofitClient.getClient(BASE_URL).create(TAASService.class);
    }
}