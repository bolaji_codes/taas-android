package utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.Measurement;
import com.bolaandroid.data.model.Order;
import com.bolaandroid.data.model.OrderSuccess;
import com.bolaandroid.data.model.Reference;
import com.bolaandroid.data.model.TransactionInitialization;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.taas.OrdersActivity;
import com.bolaandroid.taas.R;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import co.paystack.android.Transaction;
import co.paystack.android.exceptions.ExpiredAccessCodeException;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static co.paystack.android.utils.StringUtils.isEmpty;

/**
 * Created by Owner on 10/4/2017.
 */

public class PaymentProcessor {
    Context context;
    Card card;
    private Charge charge;
    private Transaction transaction;
    private ProgressDialog progressDialog;
    boolean isSlash = false; //class level initialization
    private TAASService mService;
    String reference,email,reference2;
    int price,stylePk,fabricPk;
    AlertDialog b;
    List<Integer> accessoriesPks= new ArrayList<>();
    ArrayList<Measurement> measurements=new ArrayList<>();
    String address;
    Boolean comeandMeasureMe;
    Integer deliveryFee;

    public PaymentProcessor(Context mContext, String address, String reference, String email, int price, int stylePk, int fabricPk, List<Integer> accessoriesPks, ArrayList<Measurement> measurements,Boolean comeandMeasureMe,Integer deliveryFee){
        this.deliveryFee = deliveryFee;
        this.context=mContext;
        this.price=price;
        this.reference=reference;
        this.email=email;
        this.stylePk=stylePk;
        this.fabricPk=fabricPk;
        this.accessoriesPks=accessoriesPks;
        this.measurements=measurements;
        this.address=address;
        this.comeandMeasureMe=comeandMeasureMe;
    }
    public void showDialog(){
            PaystackSdk.setPublicKey("pk_live_6aa708dd1b23f430205f9e1db02582aea238e923");
            PaystackSdk.initialize(context);
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            final LayoutInflater inflater = LayoutInflater.from(context);
            final View dialogView = inflater.inflate(R.layout.payment_dialog, null);
            dialogBuilder.setView(dialogView);
            final EditText mEditCardNum = (EditText) dialogView.findViewById(R.id.edit_card_number);
            mEditCardNum.addTextChangedListener(new FourDigitCardFormatWatcher());
            final TextView faqText = (TextView) dialogView.findViewById(R.id.faq);
            final EditText mEditCVC = (EditText) dialogView.findViewById(R.id.edit_cvv);
            final EditText mEditExpiry = (EditText) dialogView.findViewById(R.id.edit_expiry);
            final TextView priceText = (TextView) dialogView.findViewById(R.id.price);
            //mEditCardNum.setText("4084 0840 8408 4081");
             //mEditCVC.setText("408");
             // mEditExpiry.setText("01/20");
            int newprice = price/100;
            priceText.setText(" ₦"+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(newprice)));

            String lastInput ="";
            faqText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    faqDialog();
                }
            });
            mEditExpiry.addTextChangedListener(new TextWatcher() {
                //Make sure for mExpiryDate to be accepting Numbers only

                public void formatCardExpiringDate(Editable s){
                    String input = s.toString();
                    String mLastInput = "";

                    SimpleDateFormat formatter = new SimpleDateFormat("MM/yy",Locale.ENGLISH);
                    Calendar expiryDateDate = Calendar.getInstance();

                    try {
                        expiryDateDate.setTime(formatter.parse(input));
                    } catch (java.text.ParseException e) {
                        if (s.length() == 2 && !mLastInput.endsWith("/") && isSlash) {
                            isSlash = false;
                            int month = Integer.parseInt(input);
                            if (month <= 12) {
                                mEditExpiry.setText(mEditExpiry.getText().toString().substring(0, 1));
                                mEditExpiry.setSelection(mEditExpiry.getText().toString().length());
                            } else {
                                s.clear();
                                mEditExpiry.setText("");
                                mEditExpiry.setSelection(mEditExpiry.getText().toString().length());
                                Toast.makeText(context.getApplicationContext(), "Enter a valid month", Toast.LENGTH_LONG).show();
                            }
                        }else if (s.length() == 2 && !mLastInput.endsWith("/") && !isSlash) {
                            isSlash = true;
                            int month = Integer.parseInt(input);
                            if (month <= 12) {
                                mEditExpiry.setText(mEditExpiry.getText().toString() + "/");
                                mEditExpiry.setSelection(mEditExpiry.getText().toString().length());
                            }else if(month > 12){
                                mEditExpiry.setText("");
                                mEditExpiry.setSelection(mEditExpiry.getText().toString().length());
                                s.clear();
                            }


                        } else if (s.length() == 1) {

                            int month = Integer.parseInt(input);
                            if (month > 1 && month < 12) {
                                isSlash = true;
                                mEditExpiry.setText("0" + mEditExpiry.getText().toString() + "/");
                                mEditExpiry.setSelection(mEditExpiry.getText().toString().length());
                            }
                        }

                        mLastInput = mEditExpiry.getText().toString();
                        return;
                    }
                }
             @Override
             public void afterTextChanged(Editable s) {
                 try{
                     formatCardExpiringDate(s);
                 }catch(NumberFormatException e){
                     s.clear();
                     //Toast message here.. Wrong date formate

                 }
              }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });
            dialogBuilder.setTitle("Provide card details");

            dialogBuilder.setPositiveButton("PAY", null);
            b = dialogBuilder.create();
            b.setCanceledOnTouchOutside(false);
            b.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                    be.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int i=0,c=0;

                            EditText[] text={mEditCardNum,mEditCVC,mEditExpiry};
                            while(i<text.length){
                                if(text[i].getText().toString().equals("")){
                                    //Raise error on textfield and stop the code here;
                                    text[i].setError("This field is compulsory");
                                    c++;
                                }
                                i++;
                            }


                            validateCardForm(mEditCardNum,mEditCVC,mEditExpiry);
                            if(c==0){
                                if (card != null && card.isValid()) {
                                    progressDialog = new ProgressDialog(context);
                                    progressDialog.setMessage("Performing transaction... please wait");
                                    progressDialog.setCancelable(true);
                                    progressDialog.setCanceledOnTouchOutside(false);

                                    progressDialog.show();
                                    try {
                                        startAFreshCharge();
                                    } catch (Exception e) {
                                        Toast.makeText(context, String.format("An error occured while charging card: %s %s", e.getClass().getSimpleName(), e.getMessage()), Toast.LENGTH_SHORT).show();

                                    }
                                }else{
                                    //  Toast.makeText(context, "UNSuccessful", Toast.LENGTH_SHORT).show();
                                }

                            }else{
                                // Toast.makeText(context, "UnSSSuccessful", Toast.LENGTH_SHORT).show();
                            }


                        }
                    });
                }
            });
            b.show();
    }


    private void startAFreshCharge() {
        // initialize the charge
        charge = new Charge();
        mService = ApiUtils.getTAASService();
        Call<TransactionInitialization> call = mService.initTransaction(
                new Reference(reference,email,price)
        );
        call.enqueue(new Callback<TransactionInitialization>() {
            @Override
            public void onResponse(Call<TransactionInitialization> call, Response<TransactionInitialization> response) {
                if(response.isSuccessful()){charge.setAccessCode(response.body().getTransactionInitialization().getData().getAccessCode());
                    charge.setCard(card);
                    chargeCard();
                }else{
                    //if user try again after a second try, append 1 to the reference as paystack wont work with a formerly declined reference

                    reference = reference + "1";
                    progressDialog.dismiss();
                    Toast.makeText(context,"Unable to get access code",Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<TransactionInitialization> call, Throwable t) {
                //if user try again after a second try, append 1 to the reference as paystack wont work with a formerly declined reference
                reference=reference+"1";
                progressDialog.dismiss();
                Toast.makeText(context,"Unable to get access code ",Toast.LENGTH_SHORT).show();
            }
        });

        // new fetchAccessCodeFromServer().execute(backend_url+"/new-access-code");

    }

    private void chargeCard() {
        transaction = null;
        Activity activity=(Activity) context;
        PaystackSdk.chargeCard(activity, charge, new Paystack.TransactionCallback() {
            // This is called only after transaction is successful
            @Override
            public void onSuccess(Transaction transaction) {
                progressDialog.dismiss();
                b.dismiss();
                PaymentProcessor.this.transaction = transaction;
                confirmOrder(transaction.getReference(),stylePk,fabricPk,accessoriesPks);
                Toast.makeText(context,"Transaction Successful:" + transaction.getReference(), Toast.LENGTH_LONG).show();
                //new verifyOnServer().execute(transaction.getReference());
            }

            // This is called only before requesting OTP
            // Save reference so you may send to server if
            // error occurs with OTP
            // No need to dismiss dialog
            @Override
            public void beforeValidate(Transaction transaction) {
                PaymentProcessor.this.transaction = transaction;
                Toast.makeText(context, transaction.getReference(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                // If an access code has expired, simply ask your server for a new one
                // and restart the charge instead of displaying error
                PaymentProcessor.this.transaction = transaction;
                if (error instanceof ExpiredAccessCodeException) {
                    PaymentProcessor.this.startAFreshCharge();
                    PaymentProcessor.this.chargeCard();
                    return;
                }

                progressDialog.dismiss();

                if (transaction.getReference() != null) {
                    Toast.makeText(context, transaction.getReference() + " concluded with error: " + error.getMessage(), Toast.LENGTH_LONG).show();
                     //new verifyOnServer().execute(transaction.getReference());
                } else {
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        });
    }

    private void confirmOrder(String reference,int stylePk,int fabricPk,List<Integer> accessoriesPks){
        mService = ApiUtils.getTAASService();


        Call<OrderSuccess> call=mService.confirmOrder(new Order(
                reference,stylePk,fabricPk,accessoriesPks,measurements,address,comeandMeasureMe,deliveryFee
        ));
        call.enqueue(new Callback<OrderSuccess>() {
            @Override
            public void onResponse(Call<OrderSuccess> call, Response<OrderSuccess> response) {
                if(response.isSuccessful()){
                    Toast.makeText(context,"Confirmed order",Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context,
                            OrdersActivity.class);
                    context.startActivity(myIntent);
                }else{
                    Toast.makeText(context,"Error in updating transaction to server....Please contact us",Toast.LENGTH_SHORT).show();
                    Intent myIntent = new Intent(context,
                            OrdersActivity.class);
                    context.startActivity(myIntent);
                }
            }

            @Override
            public void onFailure(Call<OrderSuccess> call, Throwable t) {
                Toast.makeText(context,"Unable to post confirmed order to server",Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Method to validate the form, and set errors on the edittexts.
     */
    private void validateCardForm(EditText mEditCardNum,EditText mEditCVC,EditText mEditExpiry) {
        //validate fields
        String cardNum = mEditCardNum.getText().toString().trim().replaceAll("\\s+","")
        ;

        if (isEmpty(cardNum)) {
            mEditCardNum.setError("Empty card number");
            return;
        }

        //build card object with ONLY the number, update the other fields later
        card = new Card.Builder(cardNum, 0, 0, "").build();
        if (!card.validNumber()) {
            mEditCardNum.setError("Invalid card number");
            return;
        }

        //validate cvc
        String cvc = mEditCVC.getText().toString().trim();
        if (isEmpty(cvc)) {
            mEditCVC.setError("Empty cvc");
            return;
        }
        //update the cvc field of the card
        card.setCvc(cvc);

        //check that it's valid
        if (!card.validCVC()) {
            mEditCVC.setError("Invalid cvc");
            return;
        }

        //validate expiry month;
        String expiryText = mEditExpiry.getText().toString().trim();
        String sMonth=expiryText.substring(0, 2);
        int month = -1;
        try {
            month = Integer.parseInt(sMonth);
        } catch (Exception ignored) {
        }

        if (month < 1) {
            mEditExpiry.setError("Invalid month");
            return;
        }

        card.setExpiryMonth(month);

        String sYear = expiryText.substring(Math.max(expiryText.length() - 2, 0));
        int year = -1;
        try {
            year = Integer.parseInt(sYear);
        } catch (Exception ignored) {
        }

        if (year < 1) {
            mEditExpiry.setError("invalid year");
            return;
        }

        card.setExpiryYear(year);

        //validate expiry
        if (!card.validExpiryDate()) {
            mEditExpiry.setError("Invalid expiry");
        }
    }


    public static class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }

    public void faqDialog(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.faqs_diaog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("FAQs & Policy");
        b = dialogBuilder.create();
        b.setCanceledOnTouchOutside(true);
        b.show();
    }
}
