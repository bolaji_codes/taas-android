package utils;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;

import static utils.AppContext.getContext;

/**
 * Created by Owner on 12/29/2017.
 */

public class Context2ActivityConverter {
    Context context;

    public Context2ActivityConverter(Context context){
        this.context=context;

    }

    public Activity getActivity() {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }
}
