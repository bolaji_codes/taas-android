package com.bolaandroid.firebasenotification.service;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.bolaandroid.data.model.DesignerOrders;
import com.bolaandroid.taas.CustomActivity;
import com.bolaandroid.taas.DashboardActivity;
import com.bolaandroid.taas.OrdersActivity;
import com.bolaandroid.taas.RatingsActivity;
import com.bolaandroid.taas.UndeliveredOrdersActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.bolaandroid.firebasenotification.app.Config;
import com.bolaandroid.firebasenotification.utils.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        //if (remoteMessage.getNotification() != null) {
        //   Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
        //  handleNotification(remoteMessage.getNotification().getBody());
        //}
        Log.e(TAG, "Data structure: " + remoteMessage.getData().toString());

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }

        if(remoteMessage.getNotification()!=null){
            Log.d("TAAAAAAAAG","Message notification body:" + remoteMessage.getNotification().getBody());
        }else{
            Log.d("TAAAAAAAAG","Message notification body is NONE" );
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");
            String imageUrl=null;
            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            try{
                imageUrl = data.getString("image");
            }catch (Exception e){}
            String timestamp = data.getString("timestamp");
            //JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            //Log.e(TAG, "payload: " + payload.toString());
            //Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);


            // if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
            //   Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            //   pushNotification.putExtra("message", message);
            //   LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
            //    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            //    notificationUtils.playNotificationSound();
            //  }
            // else {
                // app is in background, show the notification in notification tray
            Intent resultIntent=new Intent();
            switch(title){
                case "TAAS Order":
                    resultIntent = new Intent(getApplicationContext(), UndeliveredOrdersActivity.class);
                    resultIntent.putExtra("message", message);
                    break;
                case "TAAS Vendor request approved":
                    resultIntent = new Intent(getApplicationContext(), DashboardActivity.class);
                    resultIntent.putExtra("message", message);
                    break;
                case "Order Status Update":
                    resultIntent = new Intent(getApplicationContext(), OrdersActivity.class);
                    resultIntent.putExtra("message", message);
                    break;
                case "New Ratings":
                    resultIntent = new Intent(getApplicationContext(), RatingsActivity.class);
                    resultIntent.putExtra("message", message);
                    break;
                case "Update TAAS":

                    final String appPackageName = getApplicationContext().getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        resultIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        resultIntent =new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
                    }
                    break;
                default:
                    resultIntent = new Intent(getApplicationContext(), CustomActivity.class);
                    resultIntent.putExtra("message", message);
                    break;

            }


                // check for image attachment
                if (imageUrl==null) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }
            //   }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "SException: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
