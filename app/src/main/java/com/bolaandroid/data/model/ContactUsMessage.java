package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Owner on 2/1/2018.
 */

public class ContactUsMessage implements Serializable {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("reason")
    @Expose
    private String reason;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("phone")
    @Expose
    private String phone;

    public ContactUsMessage(String name,String phone,String email,String message,String reason){
        this.name=name;this.phone=phone;this.email=email;this.message=message;this.reason=reason;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
