package com.bolaandroid.data.model.remote;

import com.bolaandroid.data.model.AvailabilityResponse;
import com.bolaandroid.data.model.ContactUsMessage;
import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.CustomStylesPaginatedResult;
import com.bolaandroid.data.model.DeliveryRate;
import com.bolaandroid.data.model.Designer;
import com.bolaandroid.data.model.DesignerOrders;
import com.bolaandroid.data.model.DesignerStats;
import com.bolaandroid.data.model.DetailedUserOrder;
import com.bolaandroid.data.model.EmailCred;
import com.bolaandroid.data.model.ExtraStyleData;
import com.bolaandroid.data.model.FabricCollection;
import com.bolaandroid.data.model.GetUserNames;
import com.bolaandroid.data.model.LoginResponse;
import com.bolaandroid.data.model.MeasureMePrice;
import com.bolaandroid.data.model.Order;
import com.bolaandroid.data.model.OrderSuccess;
import com.bolaandroid.data.model.PasswordChange;
import com.bolaandroid.data.model.Profile;
import com.bolaandroid.data.model.Rating;
import com.bolaandroid.data.model.Reference;
import com.bolaandroid.data.model.RegisterData;
import com.bolaandroid.data.model.TransactionInitialization;
import com.bolaandroid.data.model.User;
import com.bolaandroid.data.model.UserProfile;

import java.util.ArrayList;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TAASService{
    @GET("/api/custom-styles/")
    Call<CustomStylesPaginatedResult> getCustomStyles(@Query("offset") Integer offset,@Query("page") int page);

    @GET("/get-designer-styles/{pk}/")
    Call<CustomStylesPaginatedResult> getDesignerStyles(@Path("pk") int pk,@Query("limit") Integer limit,@Query("offset") Integer offset);

    @GET("/filter-styles/")
    Call<CustomStylesPaginatedResult> getFilteredStyles(
            @Query("limit") Integer limit,
            @Query("offset") Integer offset,
            @Query("designer") String designer,
            @Query("min") int min,
            @Query("max") int max,
            @Query("c1") String c1,
            @Query("c2") String c2,
            @Query("c3") String c3,
            @Query("c4") String c4,
            @Query("c5") String c5,
            @Query("c6") String c6,
            @Query("gender") String gender
    );

    @GET("/username-availability/{username}")
    Call<AvailabilityResponse> checkUsernameIsAvailable(@Path("username") String username);

    @GET("/email-availability/{email}")
    Call<AvailabilityResponse> checkEmailIsAvailable(@Path("email") String email);


    //@Headers("Authorization: Token 59cdb79d9177c54abf7a55b7318a6de3ddd20f67")
    @GET("/api/users/")
    Call<ArrayList<User>> getUsers();

    @GET("/style-code/{code}")
    Call<CustomStyles> getStyleByCode(@Path("code") String code);

    @GET("/custom-fabrics/{pk}")
    Call<ArrayList<CustomFabrics>> getCustomFabrics(@Path("pk") int pk);

    @GET("/custom-accessories/{pk}/{gender}")
    Call<ArrayList<CustomAccessories>> getCustomAccessories(@Path("pk") int pk,@Path("gender") String gender);



    @GET("update-apptoken/{token}")
    Call<CreateStyleResponse> updateUserappToken(@Path("token") String token);

    @POST("/auth/register/")
    Call<LoginResponse> register(
            @Body RegisterData body
            );

    @POST("/rate/{pk}/")
    Call<CreateStyleResponse> rate(
            @Path("pk") int pk,@Body Rating body
    );

    @POST("/auth/login/")
    Call<LoginResponse> login(
            @Body RegisterData body
    );

    @POST("/update-user/")
    Call<Profile> updateProfile(
            @Body Profile body
    );

    @POST("/init-transaction/")
    Call<TransactionInitialization> initTransaction(
            @Body Reference body
    );

    @POST("/confirm-order/")
    Call<OrderSuccess> confirmOrder(
            @Body Order body
            );

    @POST("/send-message-to-admin/")
    Call<ContactUsMessage> sendMessageToAdmin(
            @Body ContactUsMessage body
    );

    @POST("/get-user-name/")
    Call<GetUserNames> getUserNames(
            @Body User body
    );

    @Multipart
    @POST("/create-style/")
    Call<CreateStyleResponse> createStyle(@Part("title") RequestBody title, @Part("fabricCollection") RequestBody fabricCollection, @Part("category") RequestBody category, @Part("gender") RequestBody gender, @Part("custom_price") RequestBody price, @Part MultipartBody.Part photo1, @Part MultipartBody.Part photo2,@Part("can_use_same_fabric") Boolean canUseSameFabric,@Part("same_fabric_price") RequestBody sameFabricPrice,@Part("same_fabric_stock") RequestBody sameFabricStock);

    @Multipart
    @POST("/update-style/{pk}/")
    Call<CreateStyleResponse> updateStyle(@Path("pk") int pk,@Part("title") RequestBody title, @Part("fabricCollection") RequestBody fabricCollection, @Part("category") RequestBody category, @Part("custom_price") RequestBody price,@Part("can_use_same_fabric") Boolean canUseSameFabric,@Part("same_fabric_price") RequestBody sameFabricPrice,@Part("same_fabric_stock") RequestBody sameFabricStock);


    @Multipart
    @POST("/create-fabric/")
    Call<CreateStyleResponse> createFabric(@Part("title") RequestBody title,@Part("brand") RequestBody brand, @Part("fabricCollection") RequestBody fabricCollection, @Part("custom_price") RequestBody price, @Part MultipartBody.Part photo1,@Part("same_fabric_stock") RequestBody sameFabricStock);

    @Multipart
    @POST("/update-fabric/{pk}/")
    Call<CreateStyleResponse> updateFabric(@Path("pk") int pk,@Part("title") RequestBody title,@Part("brand") RequestBody brand, @Part("fabricCollection") RequestBody fabricCollection, @Part("custom_price") RequestBody price, @Part("same_fabric_stock") RequestBody sameFabricStock);


    @Multipart
    @POST("/create-accessory/")
    Call<CreateStyleResponse> createAccessory(@Part("title") RequestBody title, @Part("gender") RequestBody gender, @Part("custom_price") RequestBody price, @Part MultipartBody.Part photo1,@Part("same_fabric_stock") RequestBody sameFabricStock);

    @Multipart
    @POST("/update-accessory/{pk}/")
    Call<CreateStyleResponse> updateAccessory(@Path("pk") int pk, @Part("title") RequestBody title, @Part("gender") RequestBody gender, @Part("custom_price") RequestBody price,@Part("same_fabric_stock") RequestBody sameFabricStock);


    @POST("/get-user-orders/")
    Call<ArrayList<DetailedUserOrder>> getUserOrders(
            @Body User body
    );

    @Multipart
    @POST("/create-fabriccollection/")
    Call<CreateStyleResponse> createFabricCollection(
            @Part("name") RequestBody name
    );

    @GET("/get-designer-rating/{pk}/")
    Call<ArrayList<Rating>> getDesignerRatings(@Path("pk") int pk);

    @GET("/get-designer-ratings-by-designer/")
    Call<ArrayList<Rating>> getDesignerRatingsbyDesigner();

    @GET("/is-designer-check/{username}/")
    Call<Designer> getDesigner(@Path("username") String username);


    @GET("/designer-stats/")
    Call<DesignerStats> getDesignerStats();

    @GET("/get-designer-accessories-by-designer/")
    Call<ArrayList<CustomAccessories>> getDesignerAccessories();

    @GET("/get-designer-fabrics-by-designer/")
    Call<ArrayList<CustomFabrics>> getDesignerFabrics();

    @GET("/get-designer-fabriccollection/")
    Call<ArrayList<FabricCollection>> getDesignerFabricCollections();



    @GET("/get-designer-fabric-by-fabricCollection/{name}/")
    Call<ArrayList<CustomFabrics>> getFabricByDesignerCollection(@Path("name") String name);

    @GET("/get-designer-orders-by-designer/")
    Call<ArrayList<DesignerOrders>> getDesignerOrders();

    @GET("/get-undelivered-designer-orders-by-designer/")
    Call<ArrayList<DesignerOrders>> getDesignerUndeliveredOrders();

    @GET("/update-status/{pk}/{status}/")
    Call<CreateStyleResponse> updateOrderStatus(@Path("pk") int pk,@Path("status") String status);

    @GET("/get-designer-styles-by-designer/")
    Call<ArrayList<CustomStyles>> getDesignerStyles();

    @GET("/get-user-profile/")
    Call<UserProfile> getUserProfile();

    @GET("/api/shipping-rates/{customer_city}/{customer_state}/{customer_country}/{designer_city}/{designer_state}/{designer_country}/")
    Call<DeliveryRate> getDeliveryRate(@Path("customer_city") String customer_city,@Path("customer_state") String customer_state,@Path("customer_country") String customer_country,@Path("designer_city") String designer_city,@Path("designer_state") String designer_state,@Path("designer_country") String designer_country);

    @GET("/extra-style-data/{pk}/")
    Call<ExtraStyleData> getExtraData(@Path("pk") int pk);

    @POST("/update-user-profile/")
    Call<Profile> updateUserProfile(@Body Profile body);

    @Multipart
    @POST("/update-designer-profile/")
    Call<CreateStyleResponse> updateDesignerProfile(@Part("name") RequestBody name,@Part("delivery_statement") RequestBody delivery_statement,@Part("bio") RequestBody bio,@Part("address") RequestBody address,@Part("city") RequestBody city,@Part("state") RequestBody state,@Part("country") RequestBody country, @Part MultipartBody.Part dp, @Part MultipartBody.Part cover,@Part("holiday_mode") Boolean holidayMode);

    @POST("/auth/password/reset/")
    Call<ResponseBody> sendEmailForPasswordReset(@Body EmailCred body);

    @POST("/auth/password/reset/confirm/")
    Call<ResponseBody> confirmPasswordReset(@Body PasswordChange body);

    @GET("/api/custom-measurement-rate/")
    Call<MeasureMePrice> getMeasureMePrice();
}