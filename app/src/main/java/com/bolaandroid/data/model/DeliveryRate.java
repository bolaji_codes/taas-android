package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryRate {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("desitination_adrress")
    @Expose
    private String desitinationAdrress;
    @SerializedName("origin_adrress")
    @Expose
    private String originAdrress;
    @SerializedName("distance_text")
    @Expose
    private String distanceText;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("rate")
    @Expose
    private Integer rate;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesitinationAdrress() {
        return desitinationAdrress;
    }

    public void setDesitinationAdrress(String desitinationAdrress) {
        this.desitinationAdrress = desitinationAdrress;
    }

    public String getOriginAdrress() {
        return originAdrress;
    }

    public void setOriginAdrress(String originAdrress) {
        this.originAdrress = originAdrress;
    }

    public String getDistanceText() {
        return distanceText;
    }

    public void setDistanceText(String distanceText) {
        this.distanceText = distanceText;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

}
