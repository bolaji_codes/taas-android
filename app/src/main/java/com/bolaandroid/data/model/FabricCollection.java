package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FabricCollection implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("numberOfFabrics")
    @Expose
    private Integer numberOfFabrics;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getNumberOfFabrics() {
        return numberOfFabrics;
    }

    public void setNumberOfFabrics(Integer numberOfFabrics) {
        this.numberOfFabrics = numberOfFabrics;
    }
}
