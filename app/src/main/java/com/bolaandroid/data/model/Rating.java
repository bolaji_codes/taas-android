package com.bolaandroid.data.model;

/**
 * Created by Owner on 11/2/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Rating implements Serializable {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("quality")
    @Expose
    private String quality;
    @SerializedName("responsiveness")
    @Expose
    private String responsiveness;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;

    public Rating(Integer pk,String comment,String quality,String responsiveness,String deliveryTime){
        this.pk = pk;
        this.comment =comment;
        this.deliveryTime = deliveryTime;
        this.responsiveness =responsiveness;
        this.quality=quality;
    }
    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getResponsiveness() {
        return responsiveness;
    }

    public void setResponsiveness(String responsiveness) {
        this.responsiveness = responsiveness;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

}