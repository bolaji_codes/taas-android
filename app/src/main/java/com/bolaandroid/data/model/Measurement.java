package com.bolaandroid.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Measurement implements Serializable {


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("shoulder")
    @Expose
    private String shoulder;
    @SerializedName("chest")
    @Expose
    private String chest;
    @SerializedName("neck")
    @Expose
    private String neck;
    @SerializedName("head")
    @Expose
    private String head;
    @SerializedName("top_half_length")
    @Expose
    private String topHalfLength;
    @SerializedName("top_length")
    @Expose
    private String topLength;
    @SerializedName("long_sleeve")
    @Expose
    private String longSleeve;
    @SerializedName("short_sleeve")
    @Expose
    private String shortSleeve;
    @SerializedName("round_sleeve")
    @Expose
    private String roundSleeve;
    @SerializedName("arm_hole")
    @Expose
    private String armHole;
    @SerializedName("handcuff")
    @Expose
    private String handcuff;
    @SerializedName("waist")
    @Expose
    private String waist;
    @SerializedName("bum")
    @Expose
    private String bum;
    @SerializedName("lap")
    @Expose
    private String lap;
    @SerializedName("ankle")
    @Expose
    private String ankle;
    @SerializedName("knee_length")
    @Expose
    private String kneeLength;
    @SerializedName("trouser_length")
    @Expose
    private String trouserLength;
    @SerializedName("flab")
    @Expose
    private String flab;
    @SerializedName("nipple2nipple")
    @Expose
    private String nipple2nipple;
    @SerializedName("underbust")
    @Expose
    private String underbust;
    @SerializedName("full_length")
    @Expose
    private String fullLength;


    public Measurement(String name,String gender,String shoulder,String chest,String neck,String head,String topHalfLength,String topLength,String longSleeve,String shortSleeve,String roundSleeve,
                       String armHole,String handcuff,String waist,String bum,String lap,String ankle,String kneeLength,String trouserLength,String flab,String nipple2nipple,String underbust,String fullLength
                       ){
        this.name=name;
        this.gender=gender;
        this.shoulder=shoulder;
        this.chest=chest;
        this.neck=neck;
        this.head=head;
        this.topHalfLength=topHalfLength;
        this.topLength=topLength;
        this.longSleeve=longSleeve;
        this.shortSleeve=shortSleeve;
        this.roundSleeve=roundSleeve;
        this.armHole=armHole;
        this.handcuff=handcuff;
        this.waist=waist;
        this.bum=bum;
        this.lap=lap;
        this.ankle=ankle;
        this.kneeLength=kneeLength;
        this.trouserLength=trouserLength;
        this.flab=flab;
        this.nipple2nipple = nipple2nipple;
        this.fullLength=fullLength;
        this.underbust=underbust;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getShoulder() {
        return shoulder;
    }

    public void setShoulder(String shoulder) {
        this.shoulder = shoulder;
    }

    public String getChest() {
        return chest;
    }

    public void setChest(String chest) {
        this.chest = chest;
    }

    public String getNeck() {
        return neck;
    }

    public void setNeck(String neck) {
        this.neck = neck;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getTopHalfLength() {
        return topHalfLength;
    }

    public void setTopHalfLength(String topHalfLength) {
        this.topHalfLength = topHalfLength;
    }

    public String getTopLength() {
        return topLength;
    }

    public void setTopLength(String topLength) {
        this.topLength = topLength;
    }

    public String getLongSleeve() {
        return longSleeve;
    }

    public void setLongSleeve(String longSleeve) {
        this.longSleeve = longSleeve;
    }

    public String getShortSleeve() {
        return shortSleeve;
    }

    public void setShortSleeve(String shortSleeve) {
        this.shortSleeve = shortSleeve;
    }

    public String getRoundSleeve() {
        return roundSleeve;
    }

    public void setRoundSleeve(String roundSleeve) {
        this.roundSleeve = roundSleeve;
    }

    public String getArmHole() {
        return armHole;
    }

    public void setArmHole(String armHole) {
        this.armHole = armHole;
    }

    public String getHandcuff() {
        return handcuff;
    }

    public void setHandcuff(String handcuff) {
        this.handcuff = handcuff;
    }

    public String getWaist() {
        return waist;
    }

    public void setWaist(String waist) {
        this.waist = waist;
    }

    public String getBum() {
        return bum;
    }

    public void setBum(String bum) {
        this.bum = bum;
    }

    public String getLap() {
        return lap;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }

    public String getAnkle() {
        return ankle;
    }

    public void setAnkle(String ankle) {
        this.ankle = ankle;
    }

    public String getKneeLength() {
        return kneeLength;
    }

    public void setKneeLength(String kneeLength) {
        this.kneeLength = kneeLength;
    }

    public String getTrouserLength() {
        return trouserLength;
    }

    public void setTrouserLength(String trouserLength) {
        this.trouserLength = trouserLength;
    }

    public String getFlab() {
        return flab;
    }

    public void setFlab(String flab) {
        this.flab = flab;
    }

    public String getUnderbust() {
        return underbust;
    }

    public void setUnderbust(String underbust) {
        this.underbust = underbust;
    }

    public String getFullLength() {
        return fullLength;
    }

    public void setFullLength(String fullLength) {
        this.fullLength = fullLength;
    }

    public String getNipple2nipple() {
        return nipple2nipple;
    }

    public void setNipple2nipple(String nipple2nipple) {
        this.nipple2nipple = nipple2nipple;
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null) return false;
        if (!(obj instanceof Measurement)) return false;
        if (obj == this) return true;
        return this.getName().equals(((Measurement) obj).getName()) && this.getGender().equals(((Measurement) obj).getGender());}

    @Override
    public int hashCode(){
        return name.hashCode();
    }

}
