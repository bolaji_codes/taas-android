package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionInitialization {

    @SerializedName("TransactionInitialization")
    @Expose
    private TransactionInitialization_ transactionInitialization;

    public TransactionInitialization_ getTransactionInitialization() {
        return transactionInitialization;
    }

    public void setTransactionInitialization(TransactionInitialization_ transactionInitialization) {
        this.transactionInitialization = transactionInitialization;
    }

}
