package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Owner on 4/1/2018.
 */

public class PasswordChange implements Serializable{
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("new_password")
    @Expose
    private String password;


    public PasswordChange(String uid,String token,String password){
        this.uid=uid;
        this.token=token;
        this.password=password;
    }



}
