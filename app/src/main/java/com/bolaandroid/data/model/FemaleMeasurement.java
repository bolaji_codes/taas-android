package com.bolaandroid.data.model;

/**
 * Created by Owner on 10/6/2017.
 */

public class FemaleMeasurement {
    String name = null;
    boolean selected = false;

    public FemaleMeasurement(String name, boolean selected) {
        super();
        this.name = name;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}