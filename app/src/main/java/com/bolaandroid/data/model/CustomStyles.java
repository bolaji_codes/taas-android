package com.bolaandroid.data.model;


import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomStyles implements Serializable{

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("is_stocks_fabric_collection_out")
    @Expose
    private Boolean isStocksFabricCollectionOut;
    @SerializedName("photo1")
    @Expose
    private String photo1;
    @SerializedName("photo2")
    @Expose
    private String photo2;
    @SerializedName("photo3")
    @Expose
    private String photo3;
    @SerializedName("photo4")
    @Expose
    private String photo4;
    @SerializedName("custom_price")
    @Expose
    private Integer customPrice;
    @SerializedName("designer")
    @Expose
    private Designer designer;
    @SerializedName("fabricCollection")
    @Expose
    private FabricCollection fabricCollection;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("code")
    @Expose
    private String code;

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getName()  {
        return name;
    }
    @Override
    public String toString() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoto1() {
        return photo1;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public String getPhoto3() {
        return photo3;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public String getPhoto4() {
        return photo4;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    public Integer getCustomPrice() {
        return customPrice;
    }

    public void setCustomPrice(Integer customPrice) {
        this.customPrice = customPrice;
    }

    public Designer getDesigner()  {
        return designer;
    }

    public void setDesigner(Designer designer) {
        this.designer = designer;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public FabricCollection getFabricCollection() {
        return fabricCollection;
    }

    public void setFabricCollection(FabricCollection fabricCollection) {
        this.fabricCollection = fabricCollection;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean getIsStocksFabricCollectionOut() {
        return isStocksFabricCollectionOut;
    }

    public void setIsStocksFabricCollectionOut(Boolean isStocksFabricCollectionOut) {
        this.isStocksFabricCollectionOut = isStocksFabricCollectionOut;
    }

}