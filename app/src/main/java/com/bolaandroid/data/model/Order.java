package com.bolaandroid.data.model;


import android.test.InstrumentationTestCase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Order {

    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("style_pk")
    @Expose
    private Integer stylePk;
    @SerializedName("fabric_pk")
    @Expose
    private Integer fabricPk;
    @SerializedName("accessories")
    @Expose
    private List<Integer> accessories = null;
    @Expose
    private ArrayList<Measurement> measurement = null;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("comeandmeasureme")
    @Expose
    private Boolean comeAndMeasureMe;
    @SerializedName("delivery_fee")
    @Expose
    private Integer deliveryFee;

    public Order(String reference,int stylePk,int fabricPk,List<Integer> accessories,ArrayList<Measurement> measurement,String address,Boolean comeAndMeasureMe,Integer deliveryFee){
        this.deliveryFee = deliveryFee;
        this.reference=reference;
        this.stylePk = stylePk;
        this.fabricPk = fabricPk;
        this.accessories = accessories;
        this.measurement=measurement;
        this.address= address;
        this.comeAndMeasureMe = comeAndMeasureMe;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getStylePk() {
        return stylePk;
    }

    public void setStylePk(Integer stylePk) {
        this.stylePk = stylePk;
    }

    public Integer getFabricPk() {
        return fabricPk;
    }

    public void setFabricPk(Integer fabricPk) {
        this.fabricPk = fabricPk;
    }

    public List<Integer> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Integer> accessories) {
        this.accessories = accessories;
    }

    public ArrayList<Measurement> getMeasurement(){ return measurement;}

    public void setMeasurement(ArrayList<Measurement> measurement){
        this.measurement=measurement;
    }

}

