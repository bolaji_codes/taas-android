package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Designer implements Serializable {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country")
    @Expose
    private Country country;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("cover_photo")
    @Expose
    private String coverPhoto;
    @SerializedName("delivery_statement")
    @Expose
    private String deliveryStatement;
    @SerializedName("holiday_mode")
    @Expose
    private Boolean holidayMode;
    @SerializedName("disabled")
    @Expose
    private Boolean disabled;
    @SerializedName("dp")
    @Expose
    private String dp;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("average_rating")
    @Expose
    private Double averageRating;
    @SerializedName("average_responsiveness")
    @Expose
    private Integer averageResponsiveness;
    @SerializedName("average_delivery_time")
    @Expose
    private Integer averageDeliveryTime;
    @SerializedName("average_quality")
    @Expose
    private Integer averageQuality;
    @SerializedName("profile")
    @Expose
    private UserProfile profile;

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getDeliveryStatement() {
        return deliveryStatement;
    }

    public void setDeliveryStatement(String deliveryStatement) {
        this.deliveryStatement = deliveryStatement;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Integer getAverageResponsiveness() {
        return averageResponsiveness;
    }

    public void setAverageResponsiveness(Integer averageResponsiveness) {
        this.averageResponsiveness = averageResponsiveness;
    }

    public Integer getAverageDeliveryTime() {
        return averageDeliveryTime;
    }

    public void setAverageDeliveryTime(Integer averageDeliveryTime) {
        this.averageDeliveryTime = averageDeliveryTime;
    }

    public Integer getAverageQuality() {
        return averageQuality;
    }

    public void setAverageQuality(Integer averageQuality) {
        this.averageQuality = averageQuality;
    }

    public Boolean getHolidayMode() {
        return holidayMode;
    }

    public void setHolidayMode(Boolean holidayMode) {
        this.holidayMode = holidayMode;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }
}
