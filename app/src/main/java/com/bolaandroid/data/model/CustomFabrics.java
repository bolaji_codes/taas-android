package com.bolaandroid.data.model;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomFabrics implements Serializable {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("collection")
    @Expose
    private FabricCollection collection;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("stock")
    @Expose
    private Integer stock;
    @SerializedName("photo1")
    @Expose
    private String photo1;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("designer")
    @Expose
    private Designer designer;

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto1() {
        return photo1;
    }

    public void setPhoto1(String photo1) {
        this.photo1 = photo1;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Designer getDesigner() {
        return designer;
    }

    public void setDesigner(Designer designer) {
        this.designer = designer;
    }

    public FabricCollection getCollection() {
        return collection;
    }

    public void setCollection(FabricCollection collection) {
        this.collection = collection;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

}