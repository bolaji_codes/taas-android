package com.bolaandroid.data.model;

/**
 * Created by Owner on 9/14/2017.
 */

public class MaleMeasurement {

    String name = null;
    boolean selected = false;

    public MaleMeasurement(String name, boolean selected) {
        super();
        this.name = name;
        this.selected = selected;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
