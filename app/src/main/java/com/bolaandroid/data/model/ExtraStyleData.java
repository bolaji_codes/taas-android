package com.bolaandroid.data.model;

/**
 * Created by Owner on 3/29/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
    public class ExtraStyleData {

        @SerializedName("can_use_same_fabric")
        @Expose
        private Boolean canUseSameFabric;
        @SerializedName("stock")
        @Expose
        private Integer stock;
        @SerializedName("price")
        @Expose
        private Integer price;

        public Boolean getCanUseSameFabric() {
            return canUseSameFabric;
        }

        public void setCanUseSameFabric(Boolean canUseSameFabric) {
            this.canUseSameFabric = canUseSameFabric;
        }

        public Integer getStock() {
            return stock;
        }

        public void setStock(Integer stock) {
            this.stock = stock;
        }


        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }


}
