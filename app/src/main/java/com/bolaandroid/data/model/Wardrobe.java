package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Owner on 10/23/2017.
 */

public class Wardrobe {
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("style")
    @Expose
    private CustomStyles style;
    @SerializedName("fabrics")
    @Expose
    private CustomFabrics fabric;
    @SerializedName("accessories")
    @Expose
    private ArrayList<CustomAccessories> accessories;
    @SerializedName("measurements")
    @Expose
    private ArrayList<Measurement> measurements;
    @SerializedName("comeandmeasureme")
    @Expose
    private Boolean comeAndMeasureMe;
    @SerializedName("totalprice")
    @Expose
    private Integer totalPrice;
    @SerializedName("deliveryfee")
    @Expose
    private Integer deliveryFee;
    @SerializedName("address")
    @Expose
    private String address;

    public Wardrobe(String key,CustomStyles style,CustomFabrics fabric,ArrayList<CustomAccessories> accessories,ArrayList<Measurement> measurements, Boolean comeAndMeasureMe,String address,Integer deliveryFee,Integer totalPrice){
        this.key=key;
        this.style=style;
        this.fabric=fabric;
        this.accessories=accessories;
        this.measurements=measurements;
        this.comeAndMeasureMe=comeAndMeasureMe;
        this.totalPrice = totalPrice;
        this.address = address;
        this.deliveryFee = deliveryFee;
    }

    public String getKey(){ return key;}

    public void setKey(String key){
        this.key=key;
    }

    public CustomStyles getStyle() {
        return style;
    }

    public void setStyle(CustomStyles style) {
        this.style = style;
    }

    public CustomFabrics getFabric() {return fabric;}

    public void setFabric(CustomFabrics fabric) {
        this.fabric = fabric;
    }

    public ArrayList<CustomAccessories> getAccessories() {return accessories;}

    public void setAccessories(ArrayList<CustomAccessories> accessories) {
        this.accessories = accessories;
    }

    public ArrayList<Measurement> getMeasurements(){ return measurements;}

    public void setMeasurements(ArrayList<Measurement> measurements){
        this.measurements=measurements;
    }

    public Boolean getComeAndMeasureMe(){
        return comeAndMeasureMe;
    }

    public void setComeAndMeasureMe(Boolean comeAndMeasureMe){
        this.comeAndMeasureMe = comeAndMeasureMe;
    }

    public Integer getTotalPrice(){
        return totalPrice;
    }

    public Integer getDeliveryFee(){
        return deliveryFee;
    }

    public String getAddress(){
        return address;
    }




}
