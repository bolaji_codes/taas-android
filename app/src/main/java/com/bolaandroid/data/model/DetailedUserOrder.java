package com.bolaandroid.data.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailedUserOrder implements Serializable {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("delivery_fee")
    @Expose
    private Integer deliveryFee;
    @SerializedName("style")
    @Expose
    private CustomStyles style;
    @SerializedName("fabric")
    @Expose
    private CustomFabrics fabric;
    @SerializedName("is_rated")
    @Expose
    private Boolean isRated;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("pub_date")
    @Expose
    private String pubDate;
    @SerializedName("accessories")
    @Expose
    private List<CustomAccessories> accessories = null;
    @SerializedName("measurements")
    @Expose
    private List<Measurement> measurements = null;
    @SerializedName("comeandmeasureme")
    @Expose
    private Boolean comeAndMeasureMe;


    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public CustomStyles getCustomStyles() {
        return style;
    }

    public void setCustomStyles(CustomStyles style) {
        this.style = style;
    }

    public CustomFabrics getCustomFabrics() {
        return fabric;
    }

    public void setCustomFabrics(CustomFabrics fabric) {
        this.fabric = fabric;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public List<CustomAccessories> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<CustomAccessories> accessories) {
        this.accessories = accessories;
    }

    public Boolean getIsRated() {
        return isRated;
    }

    public void setIsRated(Boolean isRated) {
        this.isRated = isRated;
    }

    public List<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<Measurement> measurements) {
        this.measurements = measurements;
    }

    public void setComeAndMeasureMe(Boolean comeAndMeasureMe){
        this.comeAndMeasureMe =  comeAndMeasureMe;
    }

    public Boolean getComeAndMeasureMe(){
        return comeAndMeasureMe;
    }

    public Integer getDeliveryFee(){
        return deliveryFee;
    }

}
