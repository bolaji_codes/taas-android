package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderSuccess {

    @SerializedName("received order")
    @Expose
    private String receivedOrder;

    public String getReceivedOrder() {
        return receivedOrder;
    }

    public void setReceivedOrder(String receivedOrder) {
        this.receivedOrder = receivedOrder;
    }

}
