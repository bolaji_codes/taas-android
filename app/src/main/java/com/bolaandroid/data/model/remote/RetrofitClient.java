package com.bolaandroid.data.model.remote;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.AppContext;

public class RetrofitClient {


    private static Retrofit retrofit = null;

    public static Retrofit getClient(String baseUrl) {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }


    static OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            SharedPreferences prefs = AppContext.getContext().getSharedPreferences("TAG", Context.MODE_PRIVATE);
            Request originalRequest = chain.request();
            Request.Builder builder;
            if (prefs.contains("token")) {
                String name = prefs.getString("token", "No token here");


                builder = originalRequest.newBuilder().header("Authorization","Token" + " "+name);
            }else{
                builder = originalRequest.newBuilder();
            }

            Request newRequest = builder.build();
            return chain.proceed(newRequest);
        }
    }).build();

}


