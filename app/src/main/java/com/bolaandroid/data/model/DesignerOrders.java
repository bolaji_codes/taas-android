package com.bolaandroid.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DesignerOrders {

    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("style")
    @Expose
    private CustomStyles style;
    @SerializedName("fabric")
    @Expose
    private CustomFabrics fabric;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("pub_date")
    @Expose
    private String pubDate;
    @SerializedName("profile")
    @Expose
    private UserProfile profile;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("accessories")
    @Expose
    private List<CustomAccessories> accessories = null;
    @SerializedName("measurements")
    @Expose
    private List<Measurement> measurements = null;
    @SerializedName("comeandmeasureme")
    @Expose
    private Boolean comeAndMeasureMe;
    @SerializedName("delivery_fee")
    @Expose
    private Integer deliveryFee;


    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public CustomStyles getStyle() {
        return style;
    }

    public void setStyle(CustomStyles style) {
        this.style = style;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CustomFabrics getFabric() {
        return fabric;
    }

    public void setFabric(CustomFabrics fabric) {
        this.fabric = fabric;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public List<CustomAccessories> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<CustomAccessories> accessories) {
        this.accessories = accessories;
    }

    public void setComeAndMeasureMe(Boolean comeAndMeasureMe){
        this.comeAndMeasureMe =  comeAndMeasureMe;
    }

    public Boolean getComeAndMeasureMe(){
        return comeAndMeasureMe;
    }

    public List<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<Measurement> measurements) {
        this.measurements = measurements;
    }
    public Integer getDeliveryFee(){
        return deliveryFee;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Order)) {
            return false;
        }

        DesignerOrders order = (DesignerOrders) o;

        return order.getPk() == pk;
    }

}

