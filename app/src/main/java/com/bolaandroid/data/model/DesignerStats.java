package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DesignerStats {

    @SerializedName("rating")
    @Expose
    private Double rating;
    @SerializedName("designer")
    @Expose
    private Designer designer;
    @SerializedName("accessoriesNum")
    @Expose
    private Integer accessoriesNum;
    @SerializedName("stylesNum")
    @Expose
    private Integer stylesNum;
    @SerializedName("fabricsNum")
    @Expose
    private Integer fabricsNum;
    @SerializedName("fabricCollectionNumber")
    @Expose
    private Integer fabricCollectionNumber;
    @SerializedName("unDeliveredOrdersNum")
    @Expose
    private Integer unDeliveredOrdersNum;
    @SerializedName("ordersNum")
    @Expose
    private Integer ordersNum;

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getAccessoriesNum() {
        return accessoriesNum;
    }

    public void setAccessoriesNum(Integer accessoriesNum) {
        this.accessoriesNum = accessoriesNum;
    }

    public Integer getStylesNum() {
        return stylesNum;
    }

    public void setStylesNum(Integer stylesNum) {
        this.stylesNum = stylesNum;
    }

    public Integer getFabricsNum() {
        return fabricsNum;
    }

    public void setFabricsNum(Integer fabricsNum) {
        this.fabricsNum = fabricsNum;
    }

    public Designer getDesigner() {
        return designer;
    }

    public void setDesigner(Designer designer) {
        this.designer = designer;
    }

    public Integer getFabricCollectionNumber() {
        return fabricCollectionNumber;
    }

    public void setFabricCollectionNumber(Integer fabricCollectionNumber) {
        this.fabricCollectionNumber = fabricCollectionNumber;
    }

    public Integer getUnDeliveredOrdersNum() {
        return unDeliveredOrdersNum;
    }

    public void setUnDeliveredOrdersNum(Integer unDeliveredOrdersNum) {
        this.unDeliveredOrdersNum = unDeliveredOrdersNum;
    }

    public Integer getOrdersNum() {
        return ordersNum;
    }

    public void setOrdersNum(Integer ordersNum) {
        this.ordersNum = ordersNum;
    }

}