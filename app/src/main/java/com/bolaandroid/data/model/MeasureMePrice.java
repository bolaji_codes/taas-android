package com.bolaandroid.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MeasureMePrice {

    @SerializedName("measure_me_price")
    @Expose
    private Integer measureMePrice;

    public Integer getMeasureMePrice() {
        return measureMePrice;
    }

    public void setMeasureMePrice(Integer measureMePrice) {
        this.measureMePrice = measureMePrice;
    }

}
