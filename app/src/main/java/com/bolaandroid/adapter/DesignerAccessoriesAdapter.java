package com.bolaandroid.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bolaandroid.customlayout.SquareRelativeLayout;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.taas.EditAccessoriesActivity;
import com.bolaandroid.taas.EditFabricActivity;
import com.bolaandroid.taas.ImagesGallery;
import com.bolaandroid.taas.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Owner on 11/1/2017.
 */

public class DesignerAccessoriesAdapter extends RecyclerView.Adapter<DesignerAccessoriesAdapter.ViewHolder>{
private ArrayList<CustomAccessories> accessories;
    public AlertDialog dialog;

public DesignerAccessoriesAdapter(ArrayList<CustomAccessories> accessories){
        this.accessories=accessories;
        }

@Override
public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        SquareRelativeLayout v = (SquareRelativeLayout) LayoutInflater.from(parent.getContext())
        .inflate(R.layout.designer_styles_list_dashboard, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
        }

class ViewHolder extends RecyclerView.ViewHolder{
    SquareRelativeLayout relativeLayout;
    ImageView productPic,soldOutPic;


    public ViewHolder(SquareRelativeLayout rel){
        super(rel);
        relativeLayout=rel;
        productPic = (ImageView) itemView.findViewById(R.id.styleImage);
        soldOutPic = (ImageView) itemView.findViewById(R.id.sold_out_image);

    }

}

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position){
        final Context context = holder.productPic.getContext();
        final CustomAccessories accessory=accessories.get(position);
        Glide.with(context)
                .load(accessory.getPhoto1())
                .placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor())))
                .into(holder.productPic);
        if(accessory.getStock()!=null && accessory.getStock()<1 ){
            holder.soldOutPic.setVisibility(View.VISIBLE);
        }
        holder.productPic.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                ArrayList<String> strings = new ArrayList<String>();
                strings.add(accessory.getPhoto1());
                Intent intent = new Intent(context,
                        ImagesGallery.class).putExtra("images",strings);
                intent.putExtra("style code","No code for fabrics and accessories");
                context.startActivity(intent);
                return false;
            }
        });

        holder.productPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                detailsDialog(context,accessory);
            }
        });
    }
    @Override
    public int getItemCount() {
        return accessories.size();
    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }

    public GradientDrawable getColor(){
        GradientDrawable gradientDrawable=new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setColor(Color.rgb(192,192,192));
        return gradientDrawable;

    }

    public void showDialog(final Context context,final CustomAccessories accessories){
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Edit/View details?")
                .setMessage("What do you want to do with this style?")
                .setCancelable(true)
                .setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialogInterface,
                            int i)
                    {
                        dialogInterface.dismiss();
                    }



                })
                .setPositiveButton(
                        "View details",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialogInterface,
                                    int i)
                            {
                                detailsDialog(context,accessories);
                                dialogInterface.dismiss();
                            }
                        });

        AlertDialog alert=builder.create();
        alert.show();
    }

    public void detailsDialog(final Context context, final CustomAccessories accessories) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.accessories_view_details, null);
        dialogBuilder.setView(dialogView);
        final ImageView photo1 = (ImageView) dialogView.findViewById(R.id.photo1);
        final TextView price = (TextView) dialogView.findViewById(R.id.price);
        final TextView name = (TextView) dialogView.findViewById(R.id.name);
        final TextView gender = (TextView) dialogView.findViewById(R.id.gender);
        final TextView stock = (TextView) dialogView.findViewById(R.id.stock);
        Picasso.with(context).load(accessories.getPhoto1()).into(photo1);
        final ImageView close = (ImageView) dialogView.findViewById(R.id.close_icon);
        final ImageView edit = (ImageView) dialogView.findViewById(R.id.edit_icon);
        dialog = dialogBuilder.create();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        name.setText(accessories.getName());
        gender.setText(accessories.getGender());
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,
                        EditAccessoriesActivity.class).putExtra("accessory",accessories);
                context.startActivity(intent);
            }
        });
        if(accessories.getStock()==null){
            stock.setText("Not set");
        }else{
            stock.setText(accessories.getStock().toString());
        }
        price.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(accessories.getPrice())));
        dialog.show();
    }
}
