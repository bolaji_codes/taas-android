package com.bolaandroid.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.Measurement;
import com.bolaandroid.taas.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Owner on 11/3/2017.
 */

public class OrderMeasurementAdapter extends RecyclerView.Adapter<OrderMeasurementAdapter.ViewHolder> {
    List<Measurement> measurements;

    public OrderMeasurementAdapter(List<Measurement> measurements){
        this.measurements = measurements;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_measurement_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Context context = holder.name.getContext();
        final Measurement measurement = measurements.get(position);
        holder.name.setText(measurement.getName());
        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailsDialog(context,measurement);
            }
        });
    }
    @Override
    public int getItemCount() {
        return measurements.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;


        public ViewHolder(View view){
            super(view);
            name = (TextView) view.findViewById(R.id.name);
        }

    }

    public void detailsDialog(Context context,Measurement measurement) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.measurement_diaog, null);
        dialogBuilder.setView(dialogView);
        final TextView name = (TextView) dialogView.findViewById(R.id.title_name);
        final TextView gender = (TextView) dialogView.findViewById(R.id.gender_vaue);
        final TextView neck = (TextView) dialogView.findViewById(R.id.neck_value);
        final TextView shoulder = (TextView) dialogView.findViewById(R.id.shoulder_value);
        final TextView shortSleeve = (TextView) dialogView.findViewById(R.id.short_sleeve_value);
        final TextView longSleeve = (TextView) dialogView.findViewById(R.id.long_sleeve_value);
        final TextView roundSleeve = (TextView) dialogView.findViewById(R.id.round_sleeve_value);
        final TextView chestOrBust = (TextView) dialogView.findViewById(R.id.chest_or_bust_value);
        final TextView head = (TextView) dialogView.findViewById(R.id.head_value);
        final TextView armhole = (TextView) dialogView.findViewById(R.id.armhole_value);
        final TextView handcuff = (TextView) dialogView.findViewById(R.id.handcuff_value);
        final TextView topLength = (TextView) dialogView.findViewById(R.id.top_length_value);
        final TextView halfTopLength = (TextView) dialogView.findViewById(R.id.half_top_length_value);
        final TextView waist = (TextView) dialogView.findViewById(R.id.waist_value);
        final TextView bum = (TextView) dialogView.findViewById(R.id.bum_value);
        final TextView lap = (TextView) dialogView.findViewById(R.id.lap_value);
        final TextView knee_length = (TextView) dialogView.findViewById(R.id.knee_length_value);
        final TextView trouser_length = (TextView) dialogView.findViewById(R.id.trouser_length_value);
        final TextView ankle = (TextView) dialogView.findViewById(R.id.ankle_value);
        final TextView flab = (TextView) dialogView.findViewById(R.id.flab_value);
        final TextView nipple2nipple = (TextView) dialogView.findViewById(R.id.nipple2nipplevalue);
        final TextView underbust = (TextView) dialogView.findViewById(R.id.underbustvalue);
        final TextView fullLength = (TextView) dialogView.findViewById(R.id.full_lengthvalue);
        gender.setText(measurement.getGender());
        try{
            fullLength.setText(measurement.getFullLength());
        }catch (Exception e){fullLength.setText("Not given");}

        neck.setText(measurement.getNeck());
        name.setText(measurement.getName()+ "," + measurement.getGender() );
        shoulder.setText(measurement.getShoulder());
        roundSleeve.setText(measurement.getRoundSleeve());
        shortSleeve.setText(measurement.getShortSleeve());
        longSleeve.setText(measurement.getLongSleeve());
        chestOrBust.setText(measurement.getChest());
        head.setText(measurement.getHead());
        armhole.setText(measurement.getArmHole());
        handcuff.setText(measurement.getHandcuff());
        topLength.setText(measurement.getTopLength());
        halfTopLength.setText(measurement.getTopHalfLength());
        waist.setText(measurement.getWaist());
        bum.setText(measurement.getBum());
        lap.setText(measurement.getLap());
        knee_length.setText(measurement.getKneeLength());
        trouser_length.setText(measurement.getTrouserLength());
        ankle.setText(measurement.getAnkle());
        flab.setText(measurement.getFlab());
        if(measurement.getGender().equals("F")){
            try{

                nipple2nipple.setText(measurement.getNipple2nipple());
                if(measurement.getNipple2nipple().equals("")){
                    nipple2nipple.setText("Not given");
                }
            }catch (Exception e){
                nipple2nipple.setText("Not given");}
            try{
                underbust.setText(measurement.getUnderbust());
                if(measurement.getUnderbust().equals("")){
                    underbust.setText("Not given");
                }
            }catch (Exception e){underbust.setText("Not given");}

            flab.setText("Not needed");
        }else {
            nipple2nipple.setText("Not needed");
            underbust.setText("Not needed");
            halfTopLength.setText("Not needed");
        }
        try{

            fullLength.setText(measurement.getFullLength());
            if(measurement.getFullLength().equals("")){
                fullLength.setText("Not given");
            }
        }catch (Exception e){
            fullLength.setText("Not given");}
        final ImageView close = (ImageView) dialogView.findViewById(R.id.close_icon);
        final AlertDialog dialog = dialogBuilder.create();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
