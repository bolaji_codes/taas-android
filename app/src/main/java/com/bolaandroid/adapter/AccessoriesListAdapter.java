package com.bolaandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.taas.ImagesGallery;
import com.bolaandroid.taas.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Owner on 7/16/2017.
 */

public class AccessoriesListAdapter extends RecyclerView.Adapter<AccessoriesListAdapter.ViewHolder> {
    private ArrayList<CustomAccessories> mAccessories,selectedAccessories=new ArrayList<>();
    CardView v;

    public AccessoriesListAdapter(ArrayList<CustomAccessories> accessories){
        mAccessories=accessories;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.accessorieslist, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Context context = holder.accessoriesName.getContext();
        final CustomAccessories accessory=mAccessories.get(position);
        holder.accessoriesName.setText(accessory.getName());
        holder.checkCircle.setImageResource(R.drawable.check_circle_48dp);
        holder.price.setText(" ₦"+ NumberFormat.getNumberInstance(Locale.US).format(accessory.getPrice()));
        holder.currentCheckImage=R.drawable.check_circle_48dp;
        Picasso.with(context).load(accessory.getPhoto1()).placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor()))).into(holder.productPics);

        holder.productPics.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                pickingAccessories(accessory,holder,position);
            }
        });

        holder.checkCircle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                pickingAccessories(accessory,holder,position);
            }
        });

        holder.zoomPic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ArrayList<String> strings = new ArrayList<String>();
                strings.add(accessory.getPhoto1());
                Intent intent = new Intent(context,
                        ImagesGallery.class).putExtra("images",strings);
                intent.putExtra("style code","No code for fabrics and accessories");
                context.startActivity(intent);

            }
        });


    }

    private void pickingAccessories(CustomAccessories accessories,AccessoriesListAdapter.ViewHolder holder,int i){

        if(holder.currentCheckImage==R.drawable.check_circle_48dp) {
            holder.checkCircle.setImageResource(R.drawable.check_circle_48dp_green);
            selectedAccessories.add(accessories);
            holder.currentCheckImage=R.drawable.check_circle_48dp_green;
            Toast.makeText(holder.accessoriesName.getContext(), "picked", Toast.LENGTH_SHORT).show();
        }else{
            holder.checkCircle.setImageResource(R.drawable.check_circle_48dp);
            selectedAccessories.remove(accessories);
            holder.currentCheckImage=R.drawable.check_circle_48dp;
            Toast.makeText(holder.accessoriesName.getContext(), "unpicked", Toast.LENGTH_SHORT).show();
        }


    }


    public void sendAccessories(){
        Intent intent=new Intent("accessories_bundle");
        Bundle bundle=new Bundle();
        bundle.putSerializable("selectedAccessories",selectedAccessories);
        intent.putExtra("bundle",bundle);
        if (v!=null) {
            LocalBroadcastManager.getInstance(v.getContext()).sendBroadcastSync(intent);
        }
    }


    @Override
    public int getItemCount() {
        return mAccessories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        CardView cardView;
        ImageView productPics;
        ImageView zoomPic;
        TextView price;
        TextView accessoriesName;
        ImageView checkCircle;
        int currentCheckImage;


        public ViewHolder(CardView card){
            super(card);
            cardView=card;
            productPics = (ImageView) itemView.findViewById(R.id.img1);
            zoomPic = (ImageView) itemView.findViewById(R.id.zoom);
            checkCircle = (ImageView) itemView.findViewById(R.id.check);
            price = (TextView) itemView.findViewById(R.id.textprice);
            accessoriesName= (TextView) itemView.findViewById(R.id.accessories_name);

        }

    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }



}
