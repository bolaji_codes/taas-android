package com.bolaandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.taas.R;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Owner on 10/14/2017.
 */

public class OrderedPrdouctsAdapter extends RecyclerView.Adapter<OrderedPrdouctsAdapter.ViewHolder> {
    private ArrayList<String> mProducts=new ArrayList<>();
    private ArrayList<String> mProductsPrice = new ArrayList<>();
    private ArrayList<String> mProductsName = new ArrayList<>();

    public OrderedPrdouctsAdapter(ArrayList<String> mProducts,ArrayList<String> mProductsPrice,ArrayList<String> mProductsName){
        this.mProducts=mProducts;
        this.mProductsPrice = mProductsPrice;
        this.mProductsName = mProductsName;
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView productPic;
        TextView productPrice,productName;

        public ViewHolder(View view){
            super(view);
            productPic = (ImageView) view.findViewById(R.id.product_pic);
            productPrice = (TextView) view.findViewById(R.id.price);
            productName = (TextView) view.findViewById(R.id.style_name);

        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_selected_products, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Context context = holder.productPic.getContext();
        String pic = mProducts.get(position);
        Picasso.with(context)
                .load(pic)
                .into(holder.productPic);
        String price = mProductsPrice.get(position);
        holder.productPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(price)));
        String name = mProductsName.get(position);
        holder.productName.setText(name);
    }
}
