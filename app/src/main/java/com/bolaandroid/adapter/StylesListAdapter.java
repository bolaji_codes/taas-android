package com.bolaandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.interfaces.GoToNext;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.interfaces.RemoveOutOfStockStyles;
import com.bolaandroid.taas.DesignerActivity;
import com.bolaandroid.taas.ImagesGallery;
import com.bolaandroid.taas.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import utils.Context2ActivityConverter;

import static utils.AppContext.getContext;


public class StylesListAdapter
        extends RecyclerView.Adapter<StylesListAdapter.ViewHolder>{
    private ArrayList<CustomStyles> mStyles;
    int designerPk=0;
    public StylesListAdapter(ArrayList<CustomStyles> styles,int designerPk){
        mStyles=styles;
        this.designerPk=designerPk;
        Log.i("where new adapter size",String.valueOf(styles.size()));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.styleslistcard, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Context context = holder.designerName.getContext();
        final CustomStyles style=mStyles.get(position);
        holder.designerName.setText(style.getDesigner().getName());
        holder.productRatings.setText(String.valueOf(style.getDesigner().getAverageRating()));
        holder.designerCity.setText(style.getDesigner().getState()+",");
        holder.designerCountry.setText(style.getDesigner().getCountry().getName());
        holder.price.setText(" ₦"+NumberFormat.getNumberInstance(Locale.US).format(style.getCustomPrice()));
        holder.deliveryStatement.setText(style.getDesigner().getDeliveryStatement());
        //Picasso.with(context)
          //      .load(style.getPhoto1())
            //    .into(holder.productPics);
        Picasso.with(context).load(style.getPhoto1()).placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor()))).into(holder.productPics);
        Glide.with(context).load(style.getDesigner().getDp())
                .placeholder(new ColorDrawable(ContextCompat.getColor(context,R.color.ms_material_grey_400)))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.profilePics);
        holder.productPics.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int stylePk=style.getPk();
                int designerPk=style.getDesigner().getPk();
                String styleGender=style.getGender();
                Intent intent=new Intent("custom-params");
                intent.putExtra("stylePk",stylePk);
                intent.putExtra("styleGender",styleGender);
                intent.putExtra("designerPk",designerPk);
                intent.putExtra("i",true);
                Bundle bundle=new Bundle();
                bundle.putSerializable("style",style);
                intent.putExtra("bundle",bundle);
                LocalBroadcastManager.getInstance(context).sendBroadcastSync(intent);
                Context2ActivityConverter activityConverter=new Context2ActivityConverter(context);
                ((GoToNext) activityConverter.getActivity()).goToNext();
            }
        });
        holder.zoomPic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ArrayList<String> images=new ArrayList<>();
                images.add(style.getPhoto1());
                if(style.getPhoto2()!=null){
                    images.add(style.getPhoto2());
                }
                if(style.getPhoto3()!=null){
                    images.add(style.getPhoto3());
                }
                if(style.getPhoto4()!=null){
                    images.add(style.getPhoto4());
                }
                Intent i = new Intent(context, ImagesGallery.class);
                i.putExtra("images", images);
                i.putExtra("style code",style.getCode());
                context.startActivity(i);
            }
        });
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context,
                        DesignerActivity.class).putExtra("designer",style.getDesigner());
                context.startActivity(myIntent);
            }
        });
        if(designerPk!=0){
            holder.relativeLayout.setVisibility(View.GONE);
        }
        }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }

    @Override
    public int getItemCount() {
        return mStyles.size();
    }

    public void updateData(ArrayList<CustomStyles> newStyles,int page){
        Log.i("where size mStyle 1",String.valueOf(mStyles.size()));
        final int positionStart = mStyles.size() + 1;
        //mStyles.addAll(newStyles);
        this.notifyDataSetChanged();
        Log.i("where size mStyle 2",String.valueOf(mStyles.size()));
    }

    public void clearAndLoadData(ArrayList<CustomStyles> newStyles){
        mStyles=newStyles;
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        CardView cardView;
        ImageView productPics;
        TextView designerName;
        ImageView profilePics;
        ImageView zoomPic;
        TextView designerCity;
        TextView designerCountry;
        TextView productRatings;
        TextView price,deliveryStatement;
        RelativeLayout relativeLayout;


        public ViewHolder(CardView card){
            super(card);
            cardView=card;
            productPics = (ImageView) itemView.findViewById(R.id.img1);
            designerName = (TextView) itemView.findViewById(R.id.img_name);
            zoomPic = (ImageView) itemView.findViewById(R.id.zoom);
            profilePics =  (ImageView) itemView.findViewById(R.id.img2);
            designerCity = (TextView) itemView.findViewById(R.id.city);
            designerCountry = (TextView) itemView.findViewById(R.id.country);
            productRatings = (TextView) itemView.findViewById(R.id.ratings);
            price = (TextView) itemView.findViewById(R.id.textprice);
            deliveryStatement = (TextView) itemView.findViewById(R.id.delivery_statement);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.rel_layout);
        }

    }

}
