package com.bolaandroid.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bolaandroid.data.model.Rating;
import com.bolaandroid.taas.R;

import java.util.ArrayList;

/**
 * Created by Owner on 10/19/2017.
 */

public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ViewHolder>{

    private ArrayList<Rating> ratings;

    public ReviewListAdapter(ArrayList<Rating> ratings){
        this.ratings=ratings;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position){
        final Context context = holder.comment.getContext();
        final Rating rating=ratings.get(position);
        holder.userName.setText(rating.getUser().getFirstName() + " " + rating.getUser().getLastName() + "(@" + rating.getUser().getUsername() + ")");
        holder.overallValue.setText(rating.getRating());
        holder.comment.setText(rating.getComment());
        holder.quality.setText(convertToPercentage(rating.getQuality()));
        holder.responsiveness.setText(convertToPercentage(rating.getResponsiveness()));
        holder.delivery.setText(convertToPercentage(rating.getDeliveryTime()));

    }

    public String convertToPercentage(String value){
        Double doubleValue = Double.valueOf(value) * 20;
        int intValue = doubleValue.intValue();
        return String.valueOf(intValue) + "%";
    }

    @Override
    public int getItemCount() {
        return ratings.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        TextView userName;
        TextView overallValue,comment;
        TextView responsiveness,delivery,quality;


        public ViewHolder(View view){
            super(view);
            userName = (TextView) view.findViewById(R.id.name);
            overallValue = (TextView) view.findViewById(R.id.overallRating);
            responsiveness = (TextView) view.findViewById(R.id.responsiveness);
            delivery = (TextView) view.findViewById(R.id.delivery);
            quality = (TextView) view.findViewById(R.id.quality);
            comment = (TextView) view.findViewById(R.id.comment);
        }

    }
}
