package com.bolaandroid.adapter;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.DetailedUserOrder;
import com.bolaandroid.data.model.Rating;
import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.taas.R;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.Context2ActivityConverter;

/**
 * Created by Owner on 10/14/2017.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.MyViewHolder> implements View.OnClickListener{
    ArrayList<DetailedUserOrder> orders =  new ArrayList<>();
    private TAASService mService;
    ProgressDialog dialog;

    public OrdersAdapter(ArrayList<DetailedUserOrder> orders){
        this.orders=orders;
        mService = ApiUtils.getTAASService();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView date;
        TextView status,totalPrice,designerNumber,deliveryFee;
        TextView designer_name,measurementNo,comeAndMeasureMeInfo;
        TextView designer_number,rated;
        ImageView designerDP;
        ImageView statusCircle;
        Button rateButton,callDesigner;
        RelativeLayout rateDesignerLayout;
        RecyclerView recyclerView,recyclerView2;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.reference);
            date = (TextView) view.findViewById(R.id.datetime);
            status = (TextView) view.findViewById(R.id.status);
            rated = (TextView) view.findViewById(R.id.rated_text);
            designer_name = (TextView) view.findViewById(R.id.designer_name);
            totalPrice = (TextView) view.findViewById(R.id.totalPrice);
            designer_number = (TextView) view.findViewById(R.id.number);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
            designerDP = (ImageView) view.findViewById(R.id.designer_image);
            statusCircle = (ImageView) view.findViewById(R.id.status_circle);
            recyclerView2=(RecyclerView) view.findViewById(R.id.simpleRecyler);
            measurementNo = (TextView) view.findViewById(R.id.measurementNo);
            rateButton = (Button) view.findViewById(R.id.rate_button);
            designerNumber = (TextView) view.findViewById(R.id.number);
            rateDesignerLayout = (RelativeLayout) view.findViewById(R.id.rate_layout);
            comeAndMeasureMeInfo = (TextView) view.findViewById(R.id.come_and_measure_me);
            callDesigner = (Button) view.findViewById(R.id.call_designer);
            deliveryFee =  (TextView) view.findViewById(R.id.delivery_fee);
        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_cards, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onClick(View v){

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position){
        final Context context=holder.title.getContext();
        final DetailedUserOrder order=orders.get(position);
        holder.title.setText(order.getReference());
        holder.designer_name.setText(order.getCustomStyles().getDesigner().getName());
        holder.date.setText(order.getPubDate());
        holder.deliveryFee.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(order.getDeliveryFee()));
        final String designerPhoneNo;
        if(order.getComeAndMeasureMe()){
            holder.comeAndMeasureMeInfo.setVisibility(View.VISIBLE);
        }
        try{
            designerPhoneNo=order.getCustomStyles().getDesigner().getProfile().getPhone();
            if(order.getCustomStyles().getDesigner().getProfile().getPhone()!=null){
                holder.callDesigner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callDesigner(context,designerPhoneNo);

                    }
                });
            }else{
                holder.callDesigner.setVisibility(View.GONE);
            }
        }catch (NullPointerException e) {
            holder.designer_number.setText("Phone number not available");
        }
        
        switch(order.getStatus()){
            case "R":
                holder.status.setText("Order received by designer");
                holder.statusCircle.setImageResource(R.drawable.red_circle);
                break;
            case "S":
                holder.status.setText("Currently been sown");
                holder.statusCircle.setImageResource(R.drawable.yellow_circle);
                break;
            case "DT":
                holder.status.setText("To be delivered tomorrow");
                holder.statusCircle.setImageResource(R.drawable.green_circle);
                break;
            case "D":
                holder.status.setText("Delivered");
                holder.statusCircle.setImageResource(R.drawable.black_circle);
                break;
            default:
                holder.status.setText("Unknown status");
        }
        Picasso.with(context)
                .load(order.getCustomStyles().getDesigner().getDp())
                .into(holder.designerDP);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        OrderedPrdouctsAdapter orderedPrdouctsAdapter = new OrderedPrdouctsAdapter(getAllOrderPics(order)
                ,getAllOrderPrice(order),getAllOrderNames(order));
        holder.recyclerView.setAdapter(orderedPrdouctsAdapter);
        holder.totalPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(order.getAmount()));
        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView2.setLayoutManager(mLayoutManager2);
        OrderMeasurementAdapter orderMeasurementAdapter=new OrderMeasurementAdapter(order.getMeasurements());
        holder.recyclerView2.setAdapter(orderMeasurementAdapter);
        holder.measurementNo.setText("x"+order.getMeasurements().size());
        if(order.getIsRated()==false){
            holder.rateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rateDesignerDialog(context,position,order.getPk(),order.getCustomStyles().getDesigner().getName());
                }
            });
        }else{
            holder.rateButton.setVisibility(View.GONE);
            holder.rated.setVisibility(View.VISIBLE);
        }
    }

    public void rateDesignerDialog(final Context context,final int position, final int pk, String name){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.rate_designer_dialog, null);
        dialogBuilder.setView(dialogView);
        final RatingBar deliveryRatingBar= (RatingBar) dialogView.findViewById(R.id.deliveryRatingBar);
        final RatingBar responsivenessRatingBar= (RatingBar) dialogView.findViewById(R.id.responsivenessRatingBar);
        final RatingBar qualityRatingBar = (RatingBar) dialogView.findViewById(R.id.qualityRatingBar);
        final EditText commentEdit = (EditText) dialogView.findViewById(R.id.comment);
        dialogBuilder.setTitle("Rate "+ name+"'s" + " work");
        dialogBuilder.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                rate(pk,position,context,toStringz(responsivenessRatingBar.getRating()),toStringz(qualityRatingBar.getRating()),
                        toStringz(deliveryRatingBar.getRating()),commentEdit.getText().toString());
                dialog.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public String toStringz(Float num){
        return String.valueOf(Double.valueOf(num));
    }

    public void rate(int pk, final int position, final Context context, String reponsive, String quality, String delivery, String comment){
        dialog = new ProgressDialog(context);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                dialog.setMessage("Submitting....Please wait");
                dialog.show();
            }
        },100);
        Call<CreateStyleResponse> call = mService.rate(pk,new Rating(pk,comment,quality,reponsive,delivery));
        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if (response.isSuccessful()){
                    updateUIAfterRating(position);
                    Toast.makeText(context,"Thank you for your feedback",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }else{
                    Toast.makeText(context,"Server error",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                Toast.makeText(context,"No internet connection",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    public void updateUIAfterRating(int position){
        orders.get(position).setIsRated(true);
        this.notifyItemChanged(position);
    }
    public ArrayList<String> getAllOrderPics(DetailedUserOrder order){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(order.getCustomStyles().getPhoto1());
        arrayList.add(order.getCustomFabrics().getPhoto1());
        for (CustomAccessories temp : order.getAccessories()) {
            arrayList.add(temp.getPhoto1());
        }
        return arrayList;
    }

    public ArrayList<String> getAllOrderPrice(DetailedUserOrder order){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(order.getCustomStyles().getCustomPrice().toString());
        arrayList.add(order.getCustomFabrics().getPrice().toString());
        for (CustomAccessories temp : order.getAccessories()) {
            arrayList.add(temp.getPrice().toString());
        }
        return arrayList;
    }

    public ArrayList<String> getAllOrderNames(DetailedUserOrder order){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(order.getCustomStyles().getName());
        arrayList.add(order.getCustomFabrics().getName());
        for (CustomAccessories temp : order.getAccessories()) {
            arrayList.add(temp.getName());
        }
        return arrayList;
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void callDesigner(Context context,String number){
        Context2ActivityConverter activitty = new Context2ActivityConverter(context);
        int checkPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
        if (checkPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activitty.getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE},
                    1);
        } else {
            String uri = "tel:" + number.trim() ;
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(uri));

            context.startActivity(intent);
        }
    }

}
