package com.bolaandroid.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.Measurement;
import com.bolaandroid.data.model.Wardrobe;
import com.bolaandroid.interfaces.OnDeleteWardrobe;
import com.bolaandroid.interfaces.ShowRegisterDialog;
import com.bolaandroid.taas.CustomActivity;
import com.bolaandroid.taas.R;
import com.bolaandroid.taas.WardrobeActivity;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.paperdb.Paper;
import utils.PaymentProcessor;

/**
 * Created by Owner on 10/23/2017.
 */

public class WardrobeListAdapter extends RecyclerView.Adapter<WardrobeListAdapter.ViewHolder>{
    ArrayList<Wardrobe> arrayWardrobe = new ArrayList<>();
    Context context;

    public WardrobeListAdapter(ArrayList<Wardrobe> arrayWardrobe){
        this.arrayWardrobe=arrayWardrobe;
    }

    @Override
    public int getItemCount() {
        return arrayWardrobe.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wardrobe_adapter, parent, false);
        Paper.init(itemView.getContext());
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        context = holder.styleName.getContext();
        final Wardrobe wardrobe=arrayWardrobe.get(position);
        final CustomStyles style=wardrobe.getStyle();
        final CustomFabrics fabric=wardrobe.getFabric();
        final ArrayList<CustomAccessories> accessories = wardrobe.getAccessories();
        final ArrayList<Measurement> measurements = wardrobe.getMeasurements();

        Picasso.with(context)
                .load(style.getPhoto1())
                .into(holder.stylePic);
        Picasso.with(context)
                .load(fabric.getPhoto1())
                .into(holder.fabricImage);
        holder.styleName.setText(style.getName());
        holder.stylePrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(style.getCustomPrice()));
        int numberSelected = measurements.size();
        String usersSelected = String.valueOf(numberSelected);
        holder.usersSelected.setText("x"+usersSelected);
        holder.usersSelected2.setText("x"+usersSelected);
        holder.designerName.setText(style.getDesigner().getName());
        holder.change.setVisibility(View.GONE);
        holder.secondChange.setVisibility(View.GONE);
        holder.fabricName.setText(fabric.getName());
        holder.fabricBrand.setText(fabric.getBrand());
        holder.designerAddress.setText(wardrobe.getAddress());
        holder.designerAddress.setHorizontallyScrolling(true);
        holder.designerAddress.setSelected(true);
        try {
            holder.customerAddress.setText(wardrobe.getAddress());
            holder.customerAddress.setHorizontallyScrolling(true);
            holder.customerAddress.setSelected(true);
            holder.deliveryPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(wardrobe.getDeliveryFee()));
        }catch (Exception e){

        }

        holder.fabricPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(fabric.getPrice()));
        String str = context.getString(R.string.delete).toUpperCase();
        holder.delete.setText(str);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        SelectedAccessoriesAdapter selectedAccessoriesAdapter = new SelectedAccessoriesAdapter(accessories);
        holder.recyclerView.setAdapter(selectedAccessoriesAdapter);
        holder.totalprice = wardrobe.getTotalPrice();
        if(accessories.size()==0){
            holder.recyclerView.setVisibility(View.GONE);
            holder.noAccessoriesText.setVisibility(View.VISIBLE);
        }
        if(wardrobe.getComeAndMeasureMe()){
            holder.comeAndMeasureMe.setVisibility(View.VISIBLE);
        }
        holder.measurementNo.setText("x"+measurements.size());
        OrderMeasurementAdapter orderMeasurementAdapter=new OrderMeasurementAdapter(measurements);
        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.measurementRecycler.setLayoutManager(mLayoutManager2);
        holder.measurementRecycler.setAdapter(orderMeasurementAdapter);

        holder.totalPrice.setText("₦"+NumberFormat.getNumberInstance(Locale.US).format(holder.totalprice));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayWardrobe.remove(wardrobe);
                Paper.book("wardrobe").delete(wardrobe.getKey());
                WardrobeListAdapter.this.notifyDataSetChanged();
                ((OnDeleteWardrobe) context).onDeleteWardrobe();

            }
        });
        holder.orderProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = context.getSharedPreferences("TAG",Context.MODE_PRIVATE);
                if (prefs.contains("token")) {
                    String DATE_FORMAT_NOW = "yyyy.MM.dd-HH.mm.ss";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                    String token=prefs.getString("token","random");
                    String email=prefs.getString("email","john@bosco.com");
                    String reference=style.getName().replaceAll("\\s+","")+fabric.getName().replaceAll("\\s+","")+token.substring(0, 2)+sdf.format(cal.getTime());
                    List<Integer> accessoriesPkArray=new ArrayList<>();
                    for (int counter = 0; counter < accessories.size(); counter++) {
                        accessoriesPkArray.add(accessories.get(counter).getPk());
                    }
                    String address = null;
                    try {
                        address = wardrobe.getAddress();
                    }catch (Exception e){}

                    PaymentProcessor paymentProcessor=new PaymentProcessor(v.getContext(),address,reference,email,(holder.totalprice)*100,style.getPk(),fabric.getPk(),accessoriesPkArray,measurements,wardrobe.getComeAndMeasureMe(),wardrobe.getDeliveryFee());
                    paymentProcessor.showDialog();

                }else{
                    try {
                        ((ShowRegisterDialog) context).showRegisterDialog(false);
                    }catch (ClassCastException e){
                        Toast.makeText(context,"Sorry, you've to be logged in to order products",Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView stylePic,fabricImage;
        TextView deliveryPrice,customerAddress,comeAndMeasureMe,measurementNo,noAccessoriesText,secondChange,stylePrice,totalPrice,styleName,designerName,fabricPrice,usersSelected,designerAddress,fabricBrand,change,fabricName,usersSelected2;
        RecyclerView recyclerView,measurementRecycler;
        Button delete,orderProducts;
        int totalprice;

        public ViewHolder(View view){
            super(view);
            deliveryPrice = (TextView) view.findViewById(R.id.delivery_price);
            customerAddress = (TextView) view.findViewById(R.id.customer_address);
            stylePic = (ImageView) view.findViewById(R.id.image);
            fabricImage = (ImageView) view.findViewById(R.id.fabric_image);
            styleName = (TextView) view.findViewById(R.id.style_name);
            stylePrice = (TextView) view.findViewById(R.id.price);
            usersSelected = (TextView) view.findViewById(R.id.usersSelected);
            usersSelected2 = (TextView) view.findViewById(R.id.usersSelected2);
            designerName = (TextView) view.findViewById(R.id.designer_name);
            designerAddress = (TextView) view.findViewById(R.id.designer_address);
            change = (TextView) view.findViewById(R.id.change);
            secondChange = (TextView) view.findViewById(R.id.second_change);
            fabricName = (TextView) view.findViewById(R.id.fabric_name);
            fabricBrand = (TextView) view.findViewById(R.id.fabric_brand);
            fabricPrice = (TextView) view.findViewById(R.id.second_price);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
            totalPrice = (TextView) view.findViewById(R.id.total_price);
            delete = (Button) view.findViewById(R.id.button_add_to_basket);
            orderProducts = (Button) view.findViewById(R.id.button_order_products);
            noAccessoriesText = (TextView)view.findViewById(R.id.no_accessories_text);
            measurementRecycler = (RecyclerView)view.findViewById(R.id.measurement_recycler);
            measurementNo = (TextView) view.findViewById(R.id.measurementNo);
            comeAndMeasureMe = (TextView) view.findViewById(R.id.come_and_measure_me);
    }
}


}
