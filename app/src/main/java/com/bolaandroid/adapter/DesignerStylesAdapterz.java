package com.bolaandroid.adapter;


import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.customlayout.SquareRelativeLayout;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.taas.EditStyleActivity;
import com.bolaandroid.taas.ImagesGallery;
import com.bolaandroid.taas.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Owner on 9/24/2017.
 */

public class DesignerStylesAdapterz extends RecyclerView.Adapter<DesignerStylesAdapterz.ViewHolder>{
    private ArrayList<CustomStyles> styles;
    public AlertDialog dialog;

    public DesignerStylesAdapterz(ArrayList<CustomStyles> styles){
        this.styles=styles;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        SquareRelativeLayout v = (SquareRelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.designer_styles_list_dashboard, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position){
        final Context context = holder.productPic.getContext();
        final CustomStyles style=styles.get(position);
        try {
            if(style.getIsStocksFabricCollectionOut()!=null && style.getIsStocksFabricCollectionOut()){
                holder.soldOutPic.setVisibility(View.VISIBLE);
            }
        }
        catch (NullPointerException exception){

        }

        Picasso.with(context)
                .load(style.getPhoto1())
                .resize(600,600)
                .centerCrop()
                 .placeholder(getColor())
                .into(holder.productPic);
        holder.productPic.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ArrayList<String> strings = new ArrayList<String>();
                strings.add(style.getPhoto1());
                if(style.getPhoto2()!=null){
                    strings.add(style.getPhoto2());
                }
                if(style.getPhoto3()!=null){
                    strings.add(style.getPhoto3());
                }
                if(style.getPhoto4()!=null){
                    strings.add(style.getPhoto4());
                }
                Intent intent = new Intent(context,
                        ImagesGallery.class).putExtra("images",strings);
                intent.putExtra("style code",style.getCode());
                context.startActivity(intent);
                return true;
            }
        });
        holder.productPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailsDialog(context,style);


            }
        });

    }

    @Override
    public int getItemCount() {
        return styles.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        SquareRelativeLayout relativeLayout;
        ImageView productPic,soldOutPic;


        public ViewHolder(SquareRelativeLayout rel){
            super(rel);
            relativeLayout=rel;
            productPic = (ImageView) itemView.findViewById(R.id.styleImage);
            soldOutPic = (ImageView) itemView.findViewById(R.id.sold_out_image);

        }

    }

    public GradientDrawable getColor(){
        GradientDrawable gradientDrawable=new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setColor(Color.BLACK);
        return gradientDrawable;

    }

    public void showDialog(final Context context,final CustomStyles style){
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context,R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Edit/View details?")
                .setMessage("What do you want to do with this style?")
                .setCancelable(true)
                .setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialogInterface,
                            int i)
                    {
                        dialogInterface.dismiss();
                    }



                })
                .setPositiveButton(
                        "View details",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialogInterface,
                                    int i)
                            {
                                detailsDialog(context,style);
                                dialogInterface.dismiss();
                            }
                        });

        AlertDialog alert=builder.create();
        alert.show();
    }

    public void detailsDialog(final Context context, final CustomStyles style) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.viewdetails, null);
        dialogBuilder.setView(dialogView);
        final ImageView photo1 = (ImageView) dialogView.findViewById(R.id.photo1);
        final ImageView photo2 = (ImageView) dialogView.findViewById(R.id.photo2);
        final ImageView copyIcon = (ImageView) dialogView.findViewById(R.id.copy_icon);
        final TextView price = (TextView) dialogView.findViewById(R.id.price);
        final TextView name = (TextView) dialogView.findViewById(R.id.name);
        final TextView code = (TextView) dialogView.findViewById(R.id.code);
        final TextView fabricCollection = (TextView) dialogView.findViewById(R.id.fabricCollection);
        final TextView gender = (TextView) dialogView.findViewById(R.id.gender);
        final TextView category = (TextView) dialogView.findViewById(R.id.category);
        final ImageView close = (ImageView) dialogView.findViewById(R.id.close_icon);
        final ImageView share = (ImageView) dialogView.findViewById(R.id.share_icon);
        final ImageView editIcon = (ImageView) dialogView.findViewById(R.id.edit_icon);
        final AlertDialog dialog = dialogBuilder.create();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Picasso.with(context).load(style.getPhoto1()).placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor()))).into(photo1);
        if(style.getPhoto2()!=null){
            Picasso.with(context).load(style.getPhoto2()).placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor()))).into(photo2);
        }else{
            photo2.setVisibility(View.GONE);
        }
        editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,
                        EditStyleActivity.class).putExtra("style",style);
                context.startActivity(intent);
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> strings = new ArrayList<String>();
                strings.add(style.getPhoto1());
                if(style.getPhoto2()!=null){
                    strings.add(style.getPhoto2());
                }
                if(style.getPhoto3()!=null){
                    strings.add(style.getPhoto3());
                }
                if(style.getPhoto4()!=null){
                    strings.add(style.getPhoto4());
                }
                Intent intent = new Intent(context,
                        ImagesGallery.class).putExtra("images",strings);
                intent.putExtra("style code",style.getCode());
                context.startActivity(intent);
            }
        });
        copyIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(style.getCode(), style.getCode());
                cm.setPrimaryClip(clip);
                Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });
        name.setText(style.getName());
        gender.setText(style.getGender());
        code.setText(style.getCode());
        if(style.getFabricCollection()!=null){
            fabricCollection.setText(style.getFabricCollection().getName());

        }else{
            fabricCollection.setText("No fabric collection");
        }
        category.setText(style.getCategory().getName());
        price.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(Integer.valueOf(style.getCustomPrice())));
        dialog.show();
    }

    public void shareItem(final Context context, final String url, String styleCode) {
        final ProgressDialog dialog = new ProgressDialog(context);
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        String message = "You Love? Custom order this attire on The African Attire Shop(TAAS) App using the code "+styleCode+" to get started or click " + getURL(styleCode);
        shareIntent.putExtra(Intent.EXTRA_TEXT,message);
        Picasso.with(context.getApplicationContext()).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                dialog.dismiss();
                shareIntent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(context,bitmap));
                context.startActivity(Intent.createChooser(shareIntent, "Share product via"));
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Toast.makeText(context,"Unable to fetch image,please try again",Toast.LENGTH_LONG).show();
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        dialog.setMessage("Preparing to share...Please wait");
                        dialog.setIndeterminate(false);
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                    }
                },100);
            }
        });
    }

    public String getURL(String code){
        return "http://www.taasafrica.com/code/"+code;
    }

    public Uri getLocalBitmapUri(final Context context,Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }

}

