package com.bolaandroid.adapter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.taas.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Owner on 9/24/2017.
 */

public class SelectedAccessoriesAdapter extends RecyclerView.Adapter<SelectedAccessoriesAdapter.ViewHolder>{
    private ArrayList<CustomAccessories> mAccessories;

    public SelectedAccessoriesAdapter(ArrayList<CustomAccessories> accessories){
        mAccessories=accessories;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_accessories_layout, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position){
        final Context context = holder.productName.getContext();
        final CustomAccessories accessory=mAccessories.get(position);
        holder.productName.setText(accessory.getName());
        holder.productPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(accessory.getPrice()));
        Picasso.with(context)
                .load(accessory.getPhoto1())
                .placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor())))
                .into(holder.productPic);

    }

    @Override
    public int getItemCount() {
        return mAccessories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout relativeLayout;
        ImageView productPic;
        TextView productPrice;
        TextView productName;


        public ViewHolder(RelativeLayout rel){
            super(rel);
            relativeLayout=rel;
            productPic = (ImageView) itemView.findViewById(R.id.product_pic);
            productPrice = (TextView) itemView.findViewById(R.id.product_price);
            productName= (TextView) itemView.findViewById(R.id.product_title);

        }

    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }


}
