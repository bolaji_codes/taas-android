package com.bolaandroid.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.DesignerOrders;

import com.bolaandroid.data.model.MeasureMePrice;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.interfaces.ShowUpdateDialog;
import com.bolaandroid.taas.R;
import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.Context2ActivityConverter;

/**
 * Created by Owner on 10/14/2017.
 */

public class DesignerOrdersAdapter extends RecyclerView.Adapter<DesignerOrdersAdapter.MyViewHolder> {
    ArrayList<DesignerOrders> orders =  new ArrayList<>();
    boolean forUndeliveredOrders=false;
    int measureMePrice;
    Context context1;

    public DesignerOrdersAdapter(ArrayList<DesignerOrders> orders, boolean forUndeliveredOrders){
        this.orders=orders;
        this.forUndeliveredOrders=forUndeliveredOrders;
        getMeasuremePrice();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView date;
        TextView deliveryFee,designer_number,designer_name,status,totalPrice,measurementNo,comeAndMeasureMeInfo;
        ImageView designerDP;
        ImageView statusCircle;
        RecyclerView recyclerView,recyclerView2;
        Button updateButton,callClient;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.reference);
            date = (TextView) view.findViewById(R.id.datetime);
            measurementNo = (TextView) view.findViewById(R.id.measurementNo);
            status = (TextView) view.findViewById(R.id.status);
            designer_name = (TextView) view.findViewById(R.id.designer_name);
            designer_number = (TextView) view.findViewById(R.id.number);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
            designerDP = (ImageView) view.findViewById(R.id.designer_image);
            statusCircle = (ImageView) view.findViewById(R.id.status_circle);
            totalPrice = (TextView) view.findViewById(R.id.totalPrice);
            recyclerView2 = (RecyclerView) view.findViewById(R.id.simpleRecyler);
            updateButton=(Button) view.findViewById(R.id.update_button);
            comeAndMeasureMeInfo = (TextView) view.findViewById(R.id.come_and_measure_me);
            callClient = (Button) view.findViewById(R.id.call_client);
            deliveryFee =  (TextView) view.findViewById(R.id.delivery_fee);

        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.designer_order_cards, parent, false);
        context1=itemView.getContext();
        getMeasuremePrice();
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position){
        final Context context = holder.title.getContext();
        final DesignerOrders order=orders.get(position);
        holder.title.setText(order.getReference());
        holder.designer_name.setText(order.getUser().getFirstName()+" "+order.getUser().getLastName());
        holder.date.setText(order.getPubDate());
        holder.deliveryFee.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(order.getDeliveryFee()));
        final String designerPhoneNo="";
        if(order.getComeAndMeasureMe()){
            holder.comeAndMeasureMeInfo.setVisibility(View.VISIBLE);
            holder.comeAndMeasureMeInfo.setText("This customer requested to be measured(additional amount was paid");
        }
        try{
            if(!order.getAddress().equals("")){
                holder.designer_number.setText(order.getAddress());
            }else{
                holder.designer_number.setText(order.getProfile().getAddress()+","+order.getProfile().getCity());
            }

        }catch (NullPointerException exception){
            holder.designer_number.setText(order.getProfile().getAddress()+","+order.getProfile().getCity());
        }

        if(order.getProfile().getPhone()!=null){
            holder.callClient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callDesigner(context,order.getProfile().getPhone());

                }
            });
        }else{
            holder.callClient.setVisibility(View.GONE);
        }

        holder.totalPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(order.getAmount()));
        switch(order.getStatus()){
            case "R":
                holder.status.setText("Order received by designer");
                holder.statusCircle.setImageResource(R.drawable.red_circle);
                break;
            case "S":
                holder.status.setText("Currently been sown");
                holder.statusCircle.setImageResource(R.drawable.yellow_circle);
                break;
            case "DT":
                holder.status.setText("To be delivered tomorrow");
                holder.statusCircle.setImageResource(R.drawable.green_circle);
                break;
            case "D":
                holder.status.setText("Delivered");
                holder.statusCircle.setImageResource(R.drawable.black_circle);
                break;
            default:
                holder.status.setText("Unknown status");
        }
        Glide.with(context)
                .load(order.getStyle().getDesigner().getDp())
                .into(holder.designerDP);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        DesignerOrderedProductsAdapter orderedPrdouctsAdapter = new DesignerOrderedProductsAdapter(getAllOrderPics(order)
                ,getAllOrderPrice(order),getAllOrderNames(order));
        holder.recyclerView.setAdapter(orderedPrdouctsAdapter);
        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerView2.setLayoutManager(mLayoutManager2);
        OrderMeasurementAdapter orderMeasurementAdapter=new OrderMeasurementAdapter(order.getMeasurements());
        holder.recyclerView2.setAdapter(orderMeasurementAdapter);
        holder.measurementNo.setText("x"+order.getMeasurements().size());
        if(forUndeliveredOrders){

            holder.updateButton.setVisibility(View.VISIBLE);
            holder.updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ShowUpdateDialog) context).showUpdateDialog(position,order.getPk(),holder.status.getText().toString());
                }
            });
        }else {
            holder.callClient.setVisibility(View.GONE);
        }
    }



    public ArrayList<String> getAllOrderPics(DesignerOrders order){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(order.getStyle().getPhoto1());
        arrayList.add(order.getFabric().getPhoto1());
        for (CustomAccessories temp : order.getAccessories()) {
            arrayList.add(temp.getPhoto1());
        }
        return arrayList;
    }



    public ArrayList<String> getAllOrderPrice(DesignerOrders order){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(order.getStyle().getCustomPrice().toString());
        arrayList.add(order.getFabric().getPrice().toString());
        for (CustomAccessories temp : order.getAccessories()) {
            arrayList.add(temp.getPrice().toString());
        }
        return arrayList;
    }

    public ArrayList<String> getAllOrderNames(DesignerOrders order){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(order.getStyle().getName());
        arrayList.add(order.getFabric().getName());
        for (CustomAccessories temp : order.getAccessories()) {
            arrayList.add(temp.getName());
        }
        return arrayList;
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void callDesigner(Context context,String number){
        Context2ActivityConverter activitty = new Context2ActivityConverter(context);
        int checkPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
        if (checkPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activitty.getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE},
                    1);
        } else {
            String uri = "tel:" + number.trim() ;
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(uri));

            context.startActivity(intent);
        }
    }

    public void getMeasuremePrice(){
        TAASService mService = ApiUtils.getTAASService();
        Call<MeasureMePrice> call = mService.getMeasureMePrice();
        call.enqueue(new Callback<MeasureMePrice>() {
            @Override
            public void onResponse(Call<MeasureMePrice> call, Response<MeasureMePrice> response) {
                if(response.isSuccessful()){
                    measureMePrice = response.body().getMeasureMePrice();
                }else{
                    getMeasuremePrice();
                }
            }

            @Override
            public void onFailure(Call<MeasureMePrice> call, Throwable t) {
                getMeasuremePrice();
            }
        });
    }
}
