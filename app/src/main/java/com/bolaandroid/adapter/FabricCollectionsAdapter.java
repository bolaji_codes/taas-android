package com.bolaandroid.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bolaandroid.data.model.FabricCollection;
import com.bolaandroid.taas.FabricsActivity;
import com.bolaandroid.taas.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Owner on 11/3/2017.
 */

public class FabricCollectionsAdapter extends RecyclerView.Adapter<FabricCollectionsAdapter.ViewHolder>{
    ArrayList<FabricCollection> fabricCollections=new ArrayList<>();

    public FabricCollectionsAdapter(ArrayList<FabricCollection> fabricCollections){
        this.fabricCollections = fabricCollections;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fabric_collection_adapter, parent, false);

        return new ViewHolder(itemView);
    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }

    @Override
    public void onBindViewHolder(final FabricCollectionsAdapter.ViewHolder holder, final int position){
        final Context context = holder.fabricPic.getContext();
        final FabricCollection fabricCollection=fabricCollections.get(position);
        Picasso.with(context)
                .load(fabricCollection.getThumbnail())
                .placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor())))
                .into(holder.fabricPic);
        holder.name.setText(fabricCollection.getName());
        holder.numbers.setText(fabricCollection.getNumberOfFabrics().toString());
        holder.frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,
                        FabricsActivity.class).putExtra("fabricCollectionName",fabricCollection.getName());
                context.startActivity(intent);
            }
        });

    }
    @Override
    public int getItemCount() {
        return fabricCollections.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        FrameLayout frameLayout;
        ImageView fabricPic;
        TextView name,numbers;


        public ViewHolder(View view){
            super(view);
            frameLayout = (FrameLayout) view.findViewById(R.id.frame_layout);
            fabricPic = (ImageView) view.findViewById(R.id.fabric_image);
            name = (TextView) view.findViewById(R.id.name);
            numbers = (TextView) view.findViewById(R.id.fabricNum);

        }

    }

    public GradientDrawable getColor(){
        GradientDrawable gradientDrawable=new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setColor(Color.BLACK);
        return gradientDrawable;

    }
}
