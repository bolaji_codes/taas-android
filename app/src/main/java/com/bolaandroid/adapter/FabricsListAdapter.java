package com.bolaandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.interfaces.GoToNext;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.taas.ImagesGallery;
import com.bolaandroid.taas.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import utils.Context2ActivityConverter;

/**
 * Created by Owner on 7/11/2017.
 */

public class FabricsListAdapter extends RecyclerView.Adapter<FabricsListAdapter.ViewHolder> {
    private ArrayList<CustomFabrics> mFabrics;

    public FabricsListAdapter(ArrayList<CustomFabrics> fabrics){
        mFabrics=fabrics;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fabricslist, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Context context = holder.brandName.getContext();
        final CustomFabrics fabric=mFabrics.get(position);
        holder.brandName.setText(fabric.getBrand());
        holder.fabricName.setText(fabric.getName());
        holder.price.setText(" ₦"+ NumberFormat.getNumberInstance(Locale.US).format(fabric.getPrice()));
        Picasso.with(context).load(fabric.getPhoto1()).placeholder(new ColorDrawable(ContextCompat.getColor(context,getRandomColor()))).into(holder.productPics);
        holder.productPics.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent=new Intent("fabric2-params");
                intent.putExtra("ilm",true);
                Bundle bundle=new Bundle();
                bundle.putSerializable("fabric",fabric);
                intent.putExtra("bundle",bundle);
                LocalBroadcastManager.getInstance(context).sendBroadcastSync(intent);
                Context2ActivityConverter activityConverter=new Context2ActivityConverter(context);
                ((GoToNext) activityConverter.getActivity()).goToNext();
            }
        });

        holder.zoomPic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ArrayList<String> strings = new ArrayList<String>();
                strings.add(fabric.getPhoto1());
                Intent intent = new Intent(context,
                        ImagesGallery.class).putExtra("images",strings);
                intent.putExtra("style code","No code for fabrics and accessories");
                context.startActivity(intent);

            }
        });
    }
    @Override
    public int getItemCount() {
        return mFabrics.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        CardView cardView;
        ImageView productPics;
        TextView brandName;
        ImageView zoomPic;
        TextView price;
        TextView fabricName;

        public ViewHolder(CardView card){
            super(card);
            cardView=card;
            productPics = (ImageView) itemView.findViewById(R.id.img1);
            brandName = (TextView) itemView.findViewById(R.id.brand_name);
            zoomPic = (ImageView) itemView.findViewById(R.id.zoom);
            price = (TextView) itemView.findViewById(R.id.textprice);
            fabricName= (TextView) itemView.findViewById(R.id.fabric_name);
        }

    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }
}
