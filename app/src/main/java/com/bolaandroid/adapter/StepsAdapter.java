package com.bolaandroid.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;

import com.bolaandroid.fragment.AccessoriesFragment;
import com.bolaandroid.fragment.FabricsFragment;
import com.bolaandroid.fragment.MeasurementFragment;
import com.bolaandroid.fragment.ReviewFragment;
import com.bolaandroid.fragment.StylesFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.*;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by Owner on 6/29/2017.
 */

public class StepsAdapter  extends com.stepstone.stepper.adapter.AbstractFragmentStepAdapter {
    private static final String CURRENT_STEP_POSITION_KEY = "position";
    private static final String STYLE_PK_KEY = "pk";
    int designerPk;
    String styleCode;


    Context mContext;
    public StepsAdapter(FragmentManager fm, Context context,int designerPk,String styleCode) {
        super(fm, context);
        this.designerPk = designerPk;
        this.styleCode=styleCode;
    }
    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        if (position==0) {
            return new StepViewModel.Builder(context)
                    .setTitle("Style")
                    .create();
        }
        else if (position==1){
            return new StepViewModel.Builder(context)
                    .setTitle("Fabric")
                    .create();
        }
        else if (position==2){
            return new StepViewModel.Builder(context)
                    .setTitle("Accessories(Optional)")
                    .create();
        }
        else if (position==3){
            return new StepViewModel.Builder(context)
                    .setTitle("Measurements")
                    .create();
        }
        else if (position==4){
            return new StepViewModel.Builder(context)
                    .setTitle("Review")
                    .setEndButtonVisible(false)
                    .create();
        }
        return null;
    }


    @Override
    public Step createStep(int position) {
        switch(position){
            case 0:
                final StylesFragment step = new StylesFragment();
                Bundle b = new Bundle();
                if(designerPk !=0 ){
                    b.putInt("designerPk",designerPk);
                }
                b.putString("styleCode",styleCode);
                b.putInt(CURRENT_STEP_POSITION_KEY, position);
                step.setArguments(b);
                return step;
            case 1:
                final FabricsFragment step1 = new FabricsFragment();
                Bundle bi = new Bundle();
                bi.putInt(CURRENT_STEP_POSITION_KEY, position);
                step1.setArguments(bi);
                return step1;
            case 2:
                final AccessoriesFragment step2 = new AccessoriesFragment();
                Bundle bf = new Bundle();
                bf.putInt(CURRENT_STEP_POSITION_KEY, position);
                step2.setArguments(bf);
                return step2;
            case 3:
                final MeasurementFragment step3 = new MeasurementFragment();
                Bundle bl = new Bundle();
                bl.putInt(CURRENT_STEP_POSITION_KEY, position);
                step3.setArguments(bl);
                return step3;

            case 4:
                final ReviewFragment step4 = new ReviewFragment();
                Bundle bk = new Bundle();
                bk.putInt(CURRENT_STEP_POSITION_KEY, position);
                step4.setArguments(bk);
                return step4;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
    }

    @Override
    public int getCount() {
        return 5;
    }



}
