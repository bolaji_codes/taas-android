package com.bolaandroid.adapter;

/**
 * Created by Owner on 10/16/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bolaandroid.taas.R;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import com.bolaandroid.data.model.CustomStyles;
import java.util.Locale;

/**
 * Created by Owner on 9/24/2017.
 */

public class DesignerStylesAdapter extends RecyclerView.Adapter<DesignerStylesAdapter.ViewHolder>{
    private ArrayList<CustomStyles> styles;

    public DesignerStylesAdapter(ArrayList<CustomStyles> styles){
        this.styles=styles;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.designer_styles_list, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position){
        final Context context = holder.productPic.getContext();
        final CustomStyles style=styles.get(position);
        holder.productPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(style.getCustomPrice()));
        Picasso.with(context)
                .load(style.getPhoto1())
                .into(holder.productPic);

    }

    @Override
    public int getItemCount() {
        return styles.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout relativeLayout;
        ImageView productPic;
        TextView productPrice;


        public ViewHolder(RelativeLayout rel){
            super(rel);
            relativeLayout=rel;
            productPic = (ImageView) itemView.findViewById(R.id.product_pic);
            productPrice = (TextView) itemView.findViewById(R.id.textprice);

        }

    }


}

