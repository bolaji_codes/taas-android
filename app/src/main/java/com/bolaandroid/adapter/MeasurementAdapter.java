package com.bolaandroid.adapter;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.Measurement;
import com.bolaandroid.provider.MeasurementProvider;
import com.bolaandroid.taas.EditMeasurementActivity;
import com.bolaandroid.taas.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import utils.MeasurementCursortoPOJO;

/**
 * Created by Owner on 9/14/2017.
 */

public class MeasurementAdapter
        extends RecyclerView.Adapter<MeasurementAdapter.ViewHolder> implements Serializable{

    private Cursor cursor;
    private int nameColumnIndex;
    private int idColumnIndex;
    private int genderColumnIndex;
    Context mContext;
    ArrayList<Integer> usersSelected=new ArrayList<>();
    ArrayList<Measurement> measurements=new ArrayList<>();
    RelativeLayout v;
    boolean isOrderProcess=true;

    public MeasurementAdapter(Context context,boolean isOrderProcess){
        mContext=context;
        this.isOrderProcess=isOrderProcess;
    }
    public void swapCursor(Cursor c) {
        cursor = c;
        if(cursor!=null) {
            cursor.moveToFirst();

            nameColumnIndex = cursor.getColumnIndex(MeasurementProvider.COLUMN_NAME);
            idColumnIndex = cursor.getColumnIndex(MeasurementProvider.COLUMN_MEASUREMENTID);
            genderColumnIndex = cursor.getColumnIndex(MeasurementProvider.COLUMN_GENDER);
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
// create a new view
        v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.userslist, parent, false);
// wrap it in a ViewHolder
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder,final int i) {

        final Context context = viewHolder.name.getContext();

        final long id = getItemId(i);
// set the text

        cursor.moveToPosition(i);
        viewHolder.checkbox.setText(cursor.getString(nameColumnIndex));

        viewHolder.name.setText(cursor.getString(nameColumnIndex));


        viewHolder.editButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                // Start NewActivity.class
                Log.e("id is:",String.valueOf(id));
                Log.e("i is:",String.valueOf(i));
                editMeasurementActivity(id);

            }
        });
        if(isOrderProcess==true) {
            viewHolder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewHolder.checkbox.isChecked()) {
                        usersSelected.add(Integer.valueOf(i));
                        cursor.moveToPosition(i);
                        MeasurementCursortoPOJO measurementCursortoPOJO = new MeasurementCursortoPOJO(cursor);
                        Measurement measurement = measurementCursortoPOJO.convert();
                        measurements.add(measurement);
                    } else {
                        usersSelected.remove(Integer.valueOf(i));
                        cursor.moveToPosition(i);
                        MeasurementCursortoPOJO measurementCursortoPOJO = new MeasurementCursortoPOJO(cursor);
                        Iterator<Measurement> iter = measurements.iterator();
                        while (iter.hasNext()) {
                            Measurement meas = iter.next();
                            if (meas.getName().equals(cursor.getString(nameColumnIndex))) {
                                iter.remove();
                            }
                        }
                    }
                }
            });
            if(usersSelected!=null) {
                if (usersSelected.contains(i)) {
                    viewHolder.checkbox.setChecked(true);
                    cursor.moveToPosition(i);
                    MeasurementCursortoPOJO measurementCursortoPOJO = new MeasurementCursortoPOJO(cursor);
                    Measurement measurement = measurementCursortoPOJO.convert();
                    Iterator<Measurement> iter = measurements.iterator();
                    int line = 0;
                    while (iter.hasNext()) {
                        Measurement meas = iter.next();
                        if (meas.getName().equals(cursor.getString(nameColumnIndex))) {
                            line++;
                        }
                    }
                    if (line == 0) {
                        measurements.add(measurement);
                    }
                } else {
                    viewHolder.checkbox.setChecked(false);
                    cursor.moveToPosition(i);
                    MeasurementCursortoPOJO measurementCursortoPOJO = new MeasurementCursortoPOJO(cursor);
                    Measurement measurement = measurementCursortoPOJO.convert();
                    measurements.remove(measurement);
                }
            }
        }
        else{
            viewHolder.checkbox.setVisibility(View.GONE);
            viewHolder.name.setVisibility(View.VISIBLE);
        }
        viewHolder.deleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(context,R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Delete?")
                        .setMessage("Do you want to delete " + viewHolder.name.getText()+"'s"+" measurements")
                        .setCancelable(true)
                        .setNegativeButton(android.R.string.cancel, null)
                        .setPositiveButton(
                                R.string.delete,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialogInterface,
                                            int i)
                                    {

                                        deleteMeasurement(context, id);
                                        Toast.makeText(context,"Measurement successfully deleted",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                })
                        .setIcon(android.R.drawable.ic_dialog_alert);
                AlertDialog alert=builder.create();
                alert.show();
                Button nButton=alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                nButton.setTextColor(Color.GRAY);
        }
        });



}

    public ArrayList<Integer> getSelectedUsers(){
        return usersSelected;
    }


    public boolean sendUserCount(){

        if(usersSelected.size()!=0) {
            Intent intent=new Intent("usersSelected");
            Bundle bundle=new Bundle();
            bundle.putInt("usersSelected", usersSelected.size());
            bundle.putSerializable("selectedMeasurement",measurements);
            bundle.putBoolean("comeandmeasureme",false);
            intent.putExtra("bundle", bundle);
            LocalBroadcastManager.getInstance(v.getContext()).sendBroadcastSync(intent);
            return true;
        }
        return false;
    }

    @Override
    public long getItemId(int position) {
        cursor.moveToPosition(position);
        return cursor.getLong(idColumnIndex);
    }

    private void editMeasurementActivity(long id){
        Intent myIntent = new Intent(mContext,
                EditMeasurementActivity.class).putExtra("measurementId",(int) id);
        myIntent.putExtra("styleGender","onEdit");
        mContext.startActivity(myIntent);
    }
    @Override
    public int getItemCount() {
        try{
            Toast.makeText(mContext,cursor.getCount(),Toast.LENGTH_SHORT).show();
        }catch (Exception e){}
        return cursor!=null ? cursor.getCount() : 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout;
        TextView name;
        CheckBox checkbox;
        Button editButton;
        ImageView deleteImage;

        private ViewHolder(RelativeLayout layout) {
            super(layout);
            relativeLayout = layout;
            name = (TextView)layout.findViewById(R.id.code);
            checkbox = (CheckBox) layout.findViewById(R.id.checkBox1);
            editButton = (Button) layout.findViewById(R.id.edit);
            deleteImage = (ImageView) layout.findViewById(R.id.delete);

        }
    }

    private void deleteMeasurement(Context context, long id ) {
        context.getContentResolver()
                .delete(
                        ContentUris.withAppendedId(
                                MeasurementProvider.CONTENT_URI,
                                id),
                        null, null);
    }
/**

    @Override
    public Object getItem(int position){
        return null;
    }

    @Override
    public int getCount(){
        return cursor!=null ? cursor.getCount() : 0;
    }






    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        View view=convertView;
        ViewHolder holder;
        cursor.moveToPosition(position);
        if (view==null){
            view=inflater.inflate(R.layout.userslist,parent,false);
            holder=new ViewHolder();
            holder.checkbox=(CheckBox) view.findViewById(R.id.checkBox1);
            holder.name=(TextView) view.findViewById(R.id.code);
        }else{
            holder=(ViewHolder) view.getTag();

        }


        Toast.makeText(view.getContext(),"Ooops"+cursor.getString(nameColumnIndex),Toast.LENGTH_LONG).show();

        holder.checkbox.setText(cursor.getString(nameColumnIndex));
        holder.name.setText(cursor.getString(nameColumnIndex));
        holder.checkbox.setChecked(false);

        return view;

    }

    **/

}