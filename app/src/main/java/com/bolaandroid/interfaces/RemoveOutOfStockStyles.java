package com.bolaandroid.interfaces;

import com.bolaandroid.data.model.CustomStyles;

/**
 * Created by Owner on 3/27/2018.
 */

public interface RemoveOutOfStockStyles {

    public void removeOutOfStockStyles(int position);
}
