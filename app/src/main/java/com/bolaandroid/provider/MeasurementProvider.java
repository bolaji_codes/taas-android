package com.bolaandroid.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.bolaandroid.data.model.MaleMeasurement;

import java.util.ArrayList;


public class MeasurementProvider extends ContentProvider {
    // Database Columns
    public static final String COLUMN_MEASUREMENTID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_GENDER = "gender";
    public static final String COLUMN_SHOULDER="shoulder";
    public static final String COLUMN_CHEST = "chest";
    public static final String COLUMN_NECK="neck";
    public static final String COLUMN_HEAD = "head";
    public static final String COLUMN_TOPHALFLENGTH = "top_half_length";
    public static final String COLUMN_TOPLENGTH = "top_full_length";
    public static final String COLUMN_LONGSLEEVE = "long_sleeve";
    public static final String COLUMN_SHORTSLEEVE = "short_sleeve";
    public static final String COLUMN_ROUNDSLEEVE = "round_sleeve";
    public static final String COLUMN_ARMHOLE="arm_hole";
    public static final String COLUMN_HANDCUFF="handcuff";
    public static final String COLUMN_WAIST="waist";
    public static final String COLUMN_BUM="bum";
    public static final String COLUMN_LAP="lap";
    public static final String COLUMN_ANKLE="ankle";
    public static final String COLUMN_KNEELENGTH="knee_length";
    public static final String COLUMN_TROUSERLNGTH="trouser_length";
    public static final String COLUMN_FLAB="flab";
    public static final String COLUMN_NIPPLE2NIPPLE="nipple_2_nipple";
    public static final String COLUMN_FULL_LENGTH="full_length";
    public static final String COLUMN_UNDER_BUST="under_bust";

    // Database Related Constants →9
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "tasks";
    // The database itself
    SQLiteDatabase db;

    // Content Provider Uri and Authority
    public static final String AUTHORITY
            = "com.bolaandroid.provider.MeasurementProvider";
    public static final Uri CONTENT_URI
            = Uri.parse("content://" + AUTHORITY + "/male_measurement");
    // MIME types used for listing tasks or looking up a single
// task
    private static final String MALE_MEASUREMENTS_MIME_TYPE
            = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/vnd.com.bolaandroid.taas.male_measurements";
    private static final String MALE_MEASUREMENT_MIME_TYPE
            = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/vnd.com.bolaandroid.taas.male_measurement";
    // UriMatcher stuff
    private static final int LIST_MALE_MEASUREMENTS=0;
    private static final int ITEM_MALE_MEASUREMENTS=1;
    private static final UriMatcher URI_MATCHER = buildUriMatcher();

    /**
     * Builds up a UriMatcher for search suggestion and shortcut refresh
     * queries.
     */

    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, "male_measurement", LIST_MALE_MEASUREMENTS);
        matcher.addURI(AUTHORITY, "male_measurement/#", ITEM_MALE_MEASUREMENTS);
        return matcher;
    }


    /**
     * This method is required in order to query the supported types.
     */
    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case LIST_MALE_MEASUREMENTS:
                return MALE_MEASUREMENTS_MIME_TYPE;
            case ITEM_MALE_MEASUREMENTS:
                return MALE_MEASUREMENT_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
// you can't choose your own task id
        if( values.containsKey(COLUMN_MEASUREMENTID))
            throw new UnsupportedOperationException();
        long id = db.insertOrThrow(DATABASE_TABLE, null,
                values);
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, id);
    }


    @Override
    public int update(Uri uri, ContentValues values, String ignored1,
                      String[] ignored2) {
// you can't change a task id
        if( values.containsKey(COLUMN_MEASUREMENTID))
            throw new UnsupportedOperationException();
        int count = db.update(
                DATABASE_TABLE,
                values,
                COLUMN_MEASUREMENTID + "=?",
                new String[]{Long.toString(ContentUris.parseId(uri))});
        if (count > 0)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(Uri uri, String ignored1, String[] ignored2) {
        int count = db.delete(
                DATABASE_TABLE,
                COLUMN_MEASUREMENTID + "=?",
                new String[]{Long.toString(ContentUris.parseId(uri))});
        if (count > 0)
            getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public Cursor query(Uri uri, String[] ignored1, String selection,
                        String[] selectionArgs, String sortOrder) {
        String[] projection = new String[]{
                COLUMN_MEASUREMENTID,
                COLUMN_NAME,
                COLUMN_GENDER,
                COLUMN_SHOULDER,
                COLUMN_CHEST,
                COLUMN_NECK,
                COLUMN_HEAD,
                COLUMN_TOPHALFLENGTH,
                COLUMN_TOPLENGTH,
                COLUMN_LONGSLEEVE,
                COLUMN_SHORTSLEEVE,
                COLUMN_ROUNDSLEEVE,
                COLUMN_ARMHOLE,
                COLUMN_HANDCUFF,
                COLUMN_WAIST,
                COLUMN_BUM,
                COLUMN_LAP,
                COLUMN_ANKLE,
                COLUMN_KNEELENGTH,
                COLUMN_TROUSERLNGTH,
                COLUMN_FLAB,
                COLUMN_NIPPLE2NIPPLE,
                COLUMN_FULL_LENGTH,
                COLUMN_UNDER_BUST,
        };
        Cursor c;
        switch (URI_MATCHER.match(uri)) {
            case LIST_MALE_MEASUREMENTS:
                c = db.query(DATABASE_TABLE,
                        projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            case ITEM_MALE_MEASUREMENTS:
                c = db.query(DATABASE_TABLE, projection,
                        COLUMN_MEASUREMENTID + "=?",
                        new String[]{Long.toString(ContentUris.parseId
                                (uri))},
                        null, null, null, null);
                if (c.getCount() > 0) {
                    c.moveToFirst();
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }



    @Override
    public boolean onCreate() {
// Grab a connection to our database
        db = new DatabaseHelper(getContext()).getWritableDatabase();
        return true;
    }

    protected static class DatabaseHelper extends SQLiteOpenHelper {
        static final String DATABASE_CREATE =
                "create table " + DATABASE_TABLE + " (" +
                        COLUMN_MEASUREMENTID + " integer primary key autoincrement, " +
                        COLUMN_NAME  + " text not null, " +
                        COLUMN_GENDER  + " text not null, " +
                        COLUMN_SHOULDER  + " integer, " +
                        COLUMN_CHEST  + " integer, " +
                        COLUMN_NECK + " integer, " +
                        COLUMN_HEAD  + " integer, " +
                        COLUMN_TOPHALFLENGTH  + " integer, " +
                        COLUMN_TOPLENGTH  + " integer, " +
                        COLUMN_LONGSLEEVE  + " integer, " +
                        COLUMN_SHORTSLEEVE  + " integer, " +
                        COLUMN_ROUNDSLEEVE  + " integer, " +
                        COLUMN_ARMHOLE  + " integer, " +
                        COLUMN_HANDCUFF  + " integer , " +
                        COLUMN_WAIST  + " integer, " +
                        COLUMN_BUM   + " integer, " +
                        COLUMN_LAP   + " integer, " +
                        COLUMN_ANKLE   + " integer, " +
                        COLUMN_KNEELENGTH   + " integer, " +
                        COLUMN_TROUSERLNGTH   + " integer, " +
                        COLUMN_NIPPLE2NIPPLE + " integer, " +
                        COLUMN_FULL_LENGTH + " integer, " +
                        COLUMN_UNDER_BUST + " integer, " +
                        COLUMN_FLAB   + " integer);";
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion) {
            throw new UnsupportedOperationException();
        }
    }
    }
