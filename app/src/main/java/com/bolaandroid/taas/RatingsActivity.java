package com.bolaandroid.taas;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bolaandroid.adapter.RatingsAdapter;
import com.bolaandroid.data.model.Rating;
import com.bolaandroid.data.model.remote.TAASService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class RatingsActivity extends AppCompatActivity {
    TAASService mService;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ArrayList<Rating> ratings = new ArrayList<>();
    RatingsAdapter adapter;
    Toolbar toolbar;
    TextView noListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Your Ratings");
        mService = ApiUtils.getTAASService();
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        noListView = (TextView) findViewById(R.id.no_list_text);
        loadRatings();

    }

    public void loadRatings(){
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<Rating>> call = mService.getDesignerRatingsbyDesigner();
        call.enqueue(new Callback<ArrayList<Rating>>() {
            @Override
            public void onResponse(Call<ArrayList<Rating>> call, Response<ArrayList<Rating>> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    ratings = response.body();
                    adapter = new RatingsAdapter(ratings);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RatingsActivity.this);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(adapter);
                    if(ratings.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                }else{
                    progressBar.setVisibility(View.GONE);
                    final Snackbar snackbar = Snackbar.make(recyclerView, "Unable to fetch your ratings", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadRatings();
                        }
                    });
                    snackbar.show();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<Rating>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                final Snackbar snackbar = Snackbar.make(recyclerView, "No internet connection!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadRatings();
                    }
                });
                snackbar.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
