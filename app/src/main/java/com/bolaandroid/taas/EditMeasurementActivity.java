package com.bolaandroid.taas;


import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bolaandroid.provider.MeasurementProvider;

import java.util.ArrayList;

public class EditMeasurementActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>,View.OnClickListener{
    public static final String MEASUREMENT_ID = "measurementId";
    private static final String EXTRA_TITLE = "Add Measurements";
    public long measurementId;
    String measurementGender;
    CollapsingToolbarLayout collapsingToolbarLayout;
    VideoView videoview;
    String VideoURL = "https://www.taasafrica.com/media/videos/SinglePhotoVideo1518965115274.mp4";
    ProgressBar progressBar=null;
    MediaController mediacontroller;
    EditText editText;
    int[] measurements;
    String[] dbColumns;
    ProgressDialog dialogue;
    String ifOnEditMeasurement;
    Button neckButton,shoulderButton,shortSleeveButton,longSleeveButton,roundSleeveButton,nipple2nippleButton,
                headButton,underbustButton,chestButton,armholeButton,topLengthButton,handcuffButton,waistButton,
                shoulder2NippleButton,hipButton,lapButton,kneeLengthButton,trouserLengthButton,ankleButton,flabButton,fullLengthButton;

    public void getVariables(){
        measurements= new int[]{
                R.id.name,R.id.neck,R.id.shoulder,R.id.short_sleeve,
                R.id.long_sleeve,R.id.round_sleeve,R.id.toplength,
                R.id.tophalflength,R.id.chest,R.id.handcuff,R.id.head,
                R.id.armhole,R.id.waist,R.id.bum,R.id.lap,
                R.id.knee_length,R.id.trouser_length,
                R.id.ankle,R.id.flab,R.id.nipple2nipple,
                R.id.full_length,R.id.underbust
        };

        dbColumns= new String[]{
                MeasurementProvider.COLUMN_NAME, MeasurementProvider.COLUMN_NECK, MeasurementProvider.COLUMN_SHOULDER,
                MeasurementProvider.COLUMN_SHORTSLEEVE, MeasurementProvider.COLUMN_LONGSLEEVE, MeasurementProvider.COLUMN_ROUNDSLEEVE, MeasurementProvider.COLUMN_TOPLENGTH,
                MeasurementProvider.COLUMN_TOPHALFLENGTH, MeasurementProvider.COLUMN_CHEST, MeasurementProvider.COLUMN_HANDCUFF,
                MeasurementProvider.COLUMN_HEAD, MeasurementProvider.COLUMN_ARMHOLE, MeasurementProvider.COLUMN_WAIST,
                MeasurementProvider.COLUMN_BUM, MeasurementProvider.COLUMN_LAP, MeasurementProvider.COLUMN_KNEELENGTH,
                MeasurementProvider.COLUMN_TROUSERLNGTH, MeasurementProvider.COLUMN_ANKLE, MeasurementProvider.COLUMN_FLAB,MeasurementProvider.COLUMN_NIPPLE2NIPPLE,
                MeasurementProvider.COLUMN_FULL_LENGTH,MeasurementProvider.COLUMN_UNDER_BUST

        };

    }

    @Override
    public void onClick(View view){
        Intent i = new Intent(this, ImagesGallery.class);
        switch (view.getId()){
            case R.id.neck_button:
                ArrayList<String> neckImage=new ArrayList<>();
                neckImage.add("https://www.taasafrica.com/static/images/neck.jpg");
                i.putExtra("images", neckImage);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;
            case R.id.shoulder_button:
                ArrayList<String> shoulderImages=new ArrayList<>();
                shoulderImages.add("https://www.taasafrica.com/static/images/shoulder.jpg");
                i.putExtra("images", shoulderImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;
            case R.id.short_sleeve_button:
                ArrayList<String> sleeveButtonImages=new ArrayList<>();
                sleeveButtonImages.add("https://www.taasafrica.com/static/images/short_sleeve.jpg");
                i.putExtra("images", sleeveButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.round_sleeve_button:
                ArrayList<String> roundButtonImages=new ArrayList<>();
                roundButtonImages.add("https://www.taasafrica.com/static/images/round_sleeve.jpg");
                i.putExtra("images", roundButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.long_sleeve_button:
                ArrayList<String> longButtonImages=new ArrayList<>();
                longButtonImages.add("https://www.taasafrica.com/static/images/long_sleeve.jpg");
                i.putExtra("images", longButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.nipple2nipplebutton:
                ArrayList<String> nipple2nippleImages=new ArrayList<>();
                nipple2nippleImages.add("https://www.taasafrica.com/static/images/nipple2nipple.jpg");
                i.putExtra("images", nipple2nippleImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.chest_button:
                ArrayList<String> chestButtonImages=new ArrayList<>();
                chestButtonImages.add("https://www.taasafrica.com/static/images/bust.jpg");
                i.putExtra("images", chestButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.underbust_button:
                ArrayList<String> underbustButtonImages=new ArrayList<>();
                underbustButtonImages.add("https://www.taasafrica.com/static/images/underbust.jpg");
                i.putExtra("images", underbustButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.toplength_button:
                ArrayList<String> toplengthButtonImages=new ArrayList<>();
                toplengthButtonImages.add("https://www.taasafrica.com/static/images/top_length.jpg");
                i.putExtra("images", toplengthButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.handcuff_button:
                ArrayList<String> handcuffButtonImages=new ArrayList<>();
                handcuffButtonImages.add("https://www.taasafrica.com/static/images/wrist.jpg");
                i.putExtra("images", handcuffButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.armhole_button:
                ArrayList<String> armholeButtonImages=new ArrayList<>();
                armholeButtonImages.add("https://www.taasafrica.com/static/images/armhole.jpg");
                i.putExtra("images", armholeButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.waist_button:
                ArrayList<String> waistButtonImages=new ArrayList<>();
                waistButtonImages.add("https://www.taasafrica.com/static/images/waist.jpg");
                i.putExtra("images", waistButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.lap_button:
                ArrayList<String> lapButtonImages=new ArrayList<>();
                lapButtonImages.add("https://www.taasafrica.com/static/images/lap.jpg");
                i.putExtra("images", lapButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.hip_button:
                ArrayList<String> hipButtonImages=new ArrayList<>();
                hipButtonImages.add("https://www.taasafrica.com/static/images/hip.jpg");
                i.putExtra("images", hipButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.knee_length_button:
                ArrayList<String> kneeButtonImages=new ArrayList<>();
                kneeButtonImages.add("https://www.taasafrica.com/static/images/knee_length.jpg");
                i.putExtra("images", kneeButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.trouser_length_button:
                ArrayList<String> trouserButtonImages=new ArrayList<>();
                trouserButtonImages.add("https://www.taasafrica.com/static/images/trouser_length.jpg");
                i.putExtra("images", trouserButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.ankle_button:
                ArrayList<String> ankleButtonImages=new ArrayList<>();
                ankleButtonImages.add("https://www.taasafrica.com/static/images/ankle.jpg");
                i.putExtra("images", ankleButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.flab_button:
                ArrayList<String> flabButtonImages=new ArrayList<>();
                flabButtonImages.add("https://www.taasafrica.com/static/images/flab.jpg");
                i.putExtra("images", flabButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.full_length_button:
                ArrayList<String> fullLengthButtonImages=new ArrayList<>();
                fullLengthButtonImages.add("https://www.taasafrica.com/static/images/full_length.jpg");
                i.putExtra("images", fullLengthButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;

            case R.id.shoulder2nipple_button:
                ArrayList<String> shoulder2nippleButtonImages=new ArrayList<>();
                shoulder2nippleButtonImages.add("https://www.taasafrica.com/static/images/shoulder2nipple.jpg");
                i.putExtra("images", shoulder2nippleButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;


            case R.id.head_button:
                ArrayList<String> headButtonImages=new ArrayList<>();
                headButtonImages.add("https://www.taasafrica.com/static/images/head.jpg");
                i.putExtra("images", headButtonImages);
                i.putExtra("style code","No code for fabrics and accessories");
                this.startActivity(i);
                return;
        }
    }

    public void saveToDB(){
        getVariables();
        ContentValues values = new ContentValues();
        int i=0;
        while(i < measurements.length){
            if(i==0 & measurementId == 0){
                values.put(MeasurementProvider.COLUMN_GENDER,measurementGender);
            }
            if(i==0 & measurementId != 0){
                values.put(MeasurementProvider.COLUMN_GENDER,ifOnEditMeasurement);
                Log.e("ifOnEditMeasurement 2:",ifOnEditMeasurement);
            }
            editText = (EditText) findViewById(measurements[i]);
            String editValue=editText.getText().toString();
            int editTextLength=editValue.trim().length();
            if (editTextLength != 0){
                values.put(dbColumns[i],editValue);
            }else{
                values.put(dbColumns[i],0);
            }
            i++;
        }
        if (measurementId == 0) {
            Uri itemUri = this.getContentResolver()
                    .insert(MeasurementProvider.CONTENT_URI, values);
        }
        else{
            Uri uri=ContentUris.withAppendedId(
                    MeasurementProvider.CONTENT_URI,measurementId
            );
            int count = this.getContentResolver().update(
                    uri, values, null, null);
            if (count != 1)
                throw new IllegalStateException(
                        "Unable to update " + measurementId);
        }

        }


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityTransitions();
        setContentView(R.layout.activity_edit_measurement);

        measurementId=getIntent().getIntExtra("measurementId",0);
        measurementGender=getIntent().getStringExtra("styleGender");
        Log.e("measurementId is:",String.valueOf(measurementId));
        Log.e("measurementGender is:",String.valueOf(measurementGender));
        if(measurementId != 0){
            getSupportLoaderManager().initLoader(0,null,this);
        }
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), EXTRA_TITLE);
        supportPostponeEnterTransition();

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(EXTRA_TITLE);
        collapsingToolbarLayout.setExpandedTitleColor( ContextCompat.getColor(this,android.R.color.transparent));



        TextView title = (TextView) findViewById(R.id.title);
        title.setText(EXTRA_TITLE);

        dialogue = new ProgressDialog(EditMeasurementActivity.this);

        videoview = (VideoView) findViewById(R.id.VideoView);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        neckButton = (Button) findViewById(R.id.neck_button);
        shortSleeveButton = (Button) findViewById(R.id.short_sleeve_button);
        shoulderButton = (Button) findViewById(R.id.shoulder_button);
        longSleeveButton = (Button) findViewById(R.id.long_sleeve_button);
        roundSleeveButton = (Button) findViewById(R.id.round_sleeve_button);
        nipple2nippleButton = (Button) findViewById(R.id.nipple2nipplebutton);
        underbustButton = (Button) findViewById(R.id.underbust_button);
        chestButton = (Button) findViewById(R.id.chest_button);
        handcuffButton = (Button) findViewById(R.id.handcuff_button);
        topLengthButton = (Button) findViewById(R.id.toplength_button);
        armholeButton = (Button) findViewById(R.id.armhole_button);
        waistButton = (Button) findViewById(R.id.waist_button);
        lapButton = (Button) findViewById(R.id.lap_button);
        hipButton = (Button) findViewById(R.id.hip_button);
        kneeLengthButton = (Button) findViewById(R.id.knee_length_button);
        trouserLengthButton = (Button) findViewById(R.id.trouser_length_button);
        ankleButton = (Button) findViewById(R.id.ankle_button);
        flabButton = (Button) findViewById(R.id.flab_button);
        fullLengthButton = (Button) findViewById(R.id.full_length_button);
        shoulder2NippleButton = (Button) findViewById(R.id.shoulder2nipple_button);
        headButton= (Button) findViewById(R.id.head_button);


        neckButton.setOnClickListener(this);
        shoulderButton.setOnClickListener(this);
        shortSleeveButton.setOnClickListener(this);
        longSleeveButton.setOnClickListener(this);
        roundSleeveButton.setOnClickListener(this);
        nipple2nippleButton.setOnClickListener(this);
        chestButton.setOnClickListener(this);
        underbustButton.setOnClickListener(this);
        handcuffButton.setOnClickListener(this);
        armholeButton.setOnClickListener(this);
        topLengthButton.setOnClickListener(this);
        lapButton.setOnClickListener(this);
        hipButton.setOnClickListener(this);
        waistButton.setOnClickListener(this);
        kneeLengthButton.setOnClickListener(this);
        trouserLengthButton.setOnClickListener(this);
        ankleButton.setOnClickListener(this);
        fullLengthButton.setOnClickListener(this);
        flabButton.setOnClickListener(this);
        shoulder2NippleButton.setOnClickListener(this);
        headButton.setOnClickListener(this);


        try {
            // Start the MediaController
            mediacontroller = new MediaController(
                    EditMeasurementActivity.this);
            mediacontroller.setAnchorView(videoview);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(VideoURL);
            videoview.setMediaController(mediacontroller);
            videoview.setVideoURI(video);


        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoview.requestFocus();
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                if(progressBar!=null){
                    progressBar.setVisibility(View.GONE);

                }
                //videoview.start();
                mediacontroller.show(3000);
            }
        });

        Button button=(Button) findViewById(R.id.findSelected);
        // Capture button clicks
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EditText nameText = (EditText) findViewById(R.id.name);
                if (nameText.getText().toString().equals("")){
                    nameText.setError("This field must be filled");
                    Toast.makeText(EditMeasurementActivity.this,"Name field is compulsory",Toast.LENGTH_SHORT).show();
                }else{
                    new AsyncTaskEx().execute();

                }

            }
        });


    }

    public void showLoadingDialog(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this,R.style.SavingDialogTheme);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_saving_dialog, null);
        dialogBuilder.setView(dialogView);
        AlertDialog b = dialogBuilder.create();
        b.show();

    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri taskUri = ContentUris.withAppendedId(
                MeasurementProvider.CONTENT_URI, measurementId);
        return new CursorLoader(
                this,
                taskUri, null, null, null, null);
    }


    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor measurement) {
        getVariables();
        if (measurement.getCount() == 0) {
            this.runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    });
            return;
        }
        int i=0;
        while(i<measurements.length){
            editText = (EditText) findViewById(measurements[i]);
            editText.setText(
                    measurement.getString(
                            measurement.getColumnIndex(dbColumns[i])
                    )
            );
            i++;
        }
        if(measurementId != 0){
            ifOnEditMeasurement=measurement.getString(measurement.getColumnIndex(MeasurementProvider.COLUMN_GENDER));
            Log.e("ifOnEditMeasurement 1:",ifOnEditMeasurement);
        }





    }


    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
// nothing to reset for this fragment.
    }


    private class AsyncTaskEx extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... arg0){
            saveToDB();
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            Toast.makeText(
                    editText.getContext(),
                    "Measurement saved",
                    Toast.LENGTH_SHORT).show();
            animateOut();

            return;
        }

        @Override
        protected void onPreExecute(){
            dialogue.setMessage("Saving measurement... please wait");
            dialogue.setCancelable(true);
            dialogue.setCanceledOnTouchOutside(true);
            dialogue.show();
            return;
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (NullPointerException e) {
            return false;
        }
    }

    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    private void animateOut() {
        Animation slideAnim = AnimationUtils.loadAnimation(this,R.anim.slide_out_bottom);
        slideAnim.setFillAfter(true);;
        slideAnim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation paramAnimation) { }
            public void onAnimationRepeat(Animation paramAnimation) { }
            public void onAnimationEnd(Animation paramAnimation) {
                finish();
                // if you call NavUtils.navigateUpFromSameTask(activity); instead,
                // the screen will flicker once after the animation. Since FrontActivity is
                // in front of BackActivity, calling finish() should give the same result.
                overridePendingTransition(0, 0);
            }
        });
        findViewById(R.id.parent_container).startAnimation(slideAnim);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                animateOut();
        }
        return super.onOptionsItemSelected(item);
    }
}
