package com.bolaandroid.taas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.AvailabilityResponse;
import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.Designer;
import com.bolaandroid.data.model.EmailCred;
import com.bolaandroid.data.model.GetUserNames;
import com.bolaandroid.data.model.PasswordChange;
import com.bolaandroid.data.model.RegisterData;
import com.bolaandroid.data.model.User;
import com.bolaandroid.data.model.LoginResponse;
import com.bolaandroid.data.model.Profile;
import com.bolaandroid.data.model.Wardrobe;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.firebasenotification.app.Config;
import com.bolaandroid.fragment.ReviewFragment;
import com.bolaandroid.interfaces.GoToNext;
import com.bolaandroid.adapter.StepsAdapter;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.interfaces.RemoveOutOfStockStyles;
import com.bolaandroid.interfaces.ShowRegisterDialog;
import com.bolaandroid.interfaces.UpdateWardrobeCount;
import com.bumptech.glide.Glide;
import com.facebook.FacebookSdk;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessaging;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;


public class CustomActivity extends AppCompatActivity implements StepperLayout.StepperListener,GoToNext,UpdateWardrobeCount,ShowRegisterDialog{
    private static final String CURRENT_STEP_POSITION_KEY = "position";
    private final int SELECT_PHOTO = 1;
    int firstConstant,thirdConstant;
    String secondConstant;
    private StepperLayout mStepperLayout;
    CustomStyles style;
    CustomFabrics fabric;
    Bundle bundle = new Bundle();
    ArrayList<CustomAccessories> accessories;
    private TAASService mService;
    ProgressDialog dialogue;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    TextView txtViewCount;
    boolean usernameIsAvailable=false;
    int onBackClick=0;

    //beginning of added stuff
    private NavigationView navigationView;
    private DrawerLayout drawer;
    ActionBarDrawerToggle actionBarDrawerToggle;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtEmail;
    private Toolbar toolbar;

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    String styleCode="No style code";


    //end of added stuffs

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        dialogue = new ProgressDialog(CustomActivity.this);
        mService = ApiUtils.getTAASService();
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Intent in = getIntent();
        FacebookSdk.sdkInitialize(getApplicationContext());
        Log.d("AppLog", "key:" + FacebookSdk.getApplicationSignature(this));
        if(in!=null && in.getData()!=null ){
            if(in.getData().getScheme().equals("http") || in.getData().getScheme().equals("https")) {
                Uri data = in.getData();
                List<String> pathSegments = data.getPathSegments();
                if (pathSegments.get(0).equals("code")) {
                    if (pathSegments.size() > 0) {
                        styleCode = pathSegments.get(1);
                    }
                }

                if (pathSegments.get(0).equals("password-reset")) {
                    if (pathSegments.size() > 0) {
                        confirmPasswordDialog(pathSegments.get(1), pathSegments.get(2));
                    }
                }

            }
        }

        int startingStepPosition = savedInstanceState != null ? savedInstanceState.getInt(CURRENT_STEP_POSITION_KEY) : 0;
        if(getIntent().getIntExtra("designerPk",0)!=0){
                int designerPk = getIntent().getIntExtra("designerPk",0);
                mStepperLayout.setAdapter(new StepsAdapter(getSupportFragmentManager(), this,designerPk,styleCode), startingStepPosition);
                String designerTitle = getIntent().getStringExtra("designerTitle");
                setSupportActionBar(toolbar);
                getSupportActionBar().setTitle(designerTitle);
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
         else {
            mStepperLayout.setAdapter(new StepsAdapter(getSupportFragmentManager(), this, 0,styleCode), startingStepPosition);

            setUpNavigationView();


        }

        SharedPreferences prefs = this.getSharedPreferences("TAG",MODE_PRIVATE);
        String email=prefs.getString("email","User not logged in");
        if (prefs.contains("email") & !email.equals("User not logged in")) {
            isUserDesigner(this,email);
        }

        mStepperLayout.setListener(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-params"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver2,
                new IntentFilter("fabric2-params"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver3,
                new IntentFilter("accessories_bundle"));

        SharedPreferences pref = getSharedPreferences("TAG",MODE_PRIVATE);
        if(pref.contains("isDesigner")){
            boolean isDesigner=pref.getBoolean("isDesigner",false);
            if(isDesigner){
                Menu xMenu=navigationView.getMenu();
                final MenuItem menuItem=xMenu.findItem(R.id.nav_dashboard);
                menuItem.setVisible(true);
            }
        }


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    //Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        displayFirebaseRegId();

        if(!verifyStoragePermissions(this)){return;}

    }





    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                menuItem.setCheckable(false);
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_orders:
                        Intent myIntent = new Intent(CustomActivity.this,
                                OrdersActivity.class);
                        CustomActivity.this.startActivity(myIntent);
                        drawer.closeDrawers();

                        return true;
                    case R.id.nav_measurements:
                        Intent intent = new Intent(CustomActivity.this,
                                MeasurementActivity.class);
                        CustomActivity.this.startActivity(intent);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_notifications:
                        Intent intenz = new Intent(CustomActivity.this,
                                ContactUsActivity.class);
                        CustomActivity.this.startActivity(intenz);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_dashboard:
                        Intent intentz = new Intent(CustomActivity.this,
                                DashboardActivity.class);
                        CustomActivity.this.startActivity(intentz);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_wardrobe:
                        Intent wardrobeIntent = new Intent(CustomActivity.this,
                                WardrobeActivity.class);
                        CustomActivity.this.startActivity(wardrobeIntent);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_privacy_policy:
                        faqDialog();
                        drawer.closeDrawers();
                        return true;
                    case R.id.become_a_vendor:
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.taasafrica.com/vendor-application"));
                        startActivity(browserIntent);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_about_us:
                        Intent aboutIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.taasafrica.com"));
                        startActivity(aboutIntent);
                        drawer.closeDrawers();
                        return true;
                    default:
                        navItemIndex = 0;
                }


                return true;
            }
        });


        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        updateUserInfoinDrawer();
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        if(mStepperLayout.getCurrentStepPosition()==0){
            if(onBackClick>=1){
                finish();
                return;
            }
            onBackClick = onBackClick+ 1;
            Toast.makeText(this,"Click the back button again to exit TAAS",Toast.LENGTH_SHORT).show();
            return;
        }
        mStepperLayout.onBackClicked();
        return;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_STEP_POSITION_KEY, mStepperLayout.getCurrentStepPosition());
        outState.putInt("firstConstant",firstConstant);
        outState.putString("secondConstant",secondConstant);
        outState.putInt("thirdConstant",thirdConstant);
        bundle.putSerializable("style",style);
        bundle.putSerializable("accessories",accessories);
        bundle.putSerializable("fabric",fabric);
        outState.putBundle("bundle",bundle);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        firstConstant = savedInstanceState.getInt("firstConstant");
        secondConstant = savedInstanceState.getString("secondConstant");
        thirdConstant = savedInstanceState.getInt("thirdConstant");
        bundle=savedInstanceState.getBundle("bundle");
        style=(CustomStyles) bundle.getSerializable("style");
        fabric=(CustomFabrics) bundle.getSerializable("fabric");
        accessories=(ArrayList<CustomAccessories>) bundle.getSerializable("accessories");

    }


    public void onCompleted(View completeButton) {
        Toast.makeText(this, "StylePk:"+firstConstant+","+secondConstant, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, style.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(VerificationError verificationError) {
        Toast.makeText(this,  verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void goToNext(){
        mStepperLayout.proceed();
    }

    @Override
    public void showRegisterDialog(boolean openPaymentDialog){
            registerDialog(openPaymentDialog);
    }

    public void sendDesignerStyleLocation(){
        Intent intent= new Intent("designer_location");
        Bundle bundle1=new Bundle();
        bundle1.putSerializable("style",style);
        intent.putExtra("bundle",bundle1);
        LocalBroadcastManager.getInstance(this).sendBroadcastSync(intent);
    }


    public void getReviewVariables(){
        Intent intent= new Intent("bundle_review");
        Bundle bundle1=new Bundle();
        bundle1.putSerializable("style",style);
        bundle1.putSerializable("fabric",fabric);
        if(fabric==null) {
            Log.e("F1", "no initial fabric here");
        }
        bundle1.putSerializable("accessories",accessories);
        intent.putExtra("bundle",bundle1);
        LocalBroadcastManager.getInstance(this).sendBroadcastSync(intent);

    }



    @Override
    public void onStepSelected(int newStepPosition) {

        if(newStepPosition!=3) {
            mStepperLayout.setShowBottomNavigation(false);

        }
        if (newStepPosition==0){
            Toast.makeText(this,"Select a style you desire",Toast.LENGTH_LONG).show();
        }
        if (newStepPosition==1){
            Toast.makeText(this,"Select a fabric to use to sow chosen style",Toast.LENGTH_LONG).show();
            Intent intent=new Intent("fabric-params");
            intent.putExtra("stylePk",firstConstant);
            intent.putExtra("styleGender",secondConstant);
            Log.e("CUSTOM ACTIVITY SENT f:","CUSTOM ACTIVITY SENT f:"+secondConstant);
            LocalBroadcastManager.getInstance(this).sendBroadcastSync(intent);
        }
        if(newStepPosition==2) {
            Toast.makeText(this,"Optionally choose accessories",Toast.LENGTH_LONG).show();
            Intent intent=new Intent("accessories-params");
            intent.putExtra("designerPk",thirdConstant);
            intent.putExtra("styleGender",secondConstant);
            Log.e("CUSTOM ACTIVITY SENT A:","CUSTOM ACTIVITY SENT A:"+secondConstant);
            LocalBroadcastManager.getInstance(this).sendBroadcastSync(intent);
            mStepperLayout.setShowBottomNavigation(true);
           }
        if(newStepPosition==3){

            mStepperLayout.setShowBottomNavigation(true);
            sendDesignerStyleLocation();
        }
        if(newStepPosition==4){
            mStepperLayout.setShowBottomNavigation(true);
            getReviewVariables();

        }
    }
    @Override
    public void onReturn() {
        finish();
    }

    public BroadcastReceiver mMessageReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            firstConstant=intent.getIntExtra("stylePk",0);
            thirdConstant=intent.getIntExtra("designerPk",0);
            secondConstant=intent.getStringExtra("styleGender");
            Bundle bundle=intent.getBundleExtra("bundle");
            if (bundle!=null){
                style=(CustomStyles) bundle.getSerializable("style");
            }
        }
    };

    public BroadcastReceiver mMessageReceiver2= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle=intent.getBundleExtra("bundle");
            if (bundle!=null){
                fabric=(CustomFabrics) bundle.getSerializable("fabric");
            }
        }
    };

    public BroadcastReceiver mMessageReceiver3= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle=intent.getBundleExtra("bundle");
            if (bundle!=null){
                accessories=(ArrayList<CustomAccessories>) bundle.getSerializable("selectedAccessories");

            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list,menu);
        final View notifications = menu.findItem(R.id.menu_wardrobe).getActionView();
        txtViewCount = (TextView) notifications.findViewById(R.id.txtCount);
        updateWardrobeCount();
        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(CustomActivity.this,
                        WardrobeActivity.class);
                CustomActivity.this.startActivity(myIntent);
            }
        });
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        updateWardrobeCount();
        updateUserInfoinDrawer();
        displayFirebaseRegId();
    }


    public void updateWardrobeCount(){
        Paper.init(this);
        List<String> allKeys = Paper.book("wardrobe").getAllKeys();
        int i=allKeys.size();
        if (txtViewCount!=null) {
        txtViewCount.setText(String.valueOf(i));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_account:
                SharedPreferences pref= this.getSharedPreferences("TAG",MODE_PRIVATE);
                if(!pref.contains("email")){
                    registerDialog(false);
            }  else{

                    alreadyloginDialog();
                }


                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_wardrobe:
                Intent myIntent = new Intent(CustomActivity.this,
                        WardrobeActivity.class);
                CustomActivity.this.startActivity(myIntent);
                return true;
        }
        if(this.getIntent()==null) {
            if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void registerDialog(final boolean openPaymentDialog) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.registerdialog, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final Button button = (Button) dialogView.findViewById(R.id.login_button);
        final EditText f_name = (EditText) dialogView.findViewById(R.id.f_name);
        final EditText l_name = (EditText) dialogView.findViewById(R.id.l_name);
        final EditText username = (EditText) dialogView.findViewById(R.id.username);
        final EditText email = (EditText) dialogView.findViewById(R.id.email);
        final EditText phone = (EditText) dialogView.findViewById(R.id.phone);
        final EditText password = (EditText) dialogView.findViewById(R.id.password);
        final EditText address = (EditText) dialogView.findViewById(R.id.address);
        final EditText city = (EditText) dialogView.findViewById(R.id.city);
        final EditText state = (EditText) dialogView.findViewById(R.id.state);
        final Spinner spinner = (Spinner) dialogView.findViewById(R.id.countries_spinner);
        final ImageView usernameCheck = (ImageView) dialogView.findViewById(R.id.username_ok);
        final ProgressBar usernameProgressBar = (ProgressBar) dialogView.findViewById(R.id.username_progress);
        final ImageView emailCheck = (ImageView) dialogView.findViewById(R.id.email_ok);
        final ProgressBar emailProgressBar = (ProgressBar) dialogView.findViewById(R.id.email_progress);

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailProgressBar.setVisibility(View.VISIBLE);
                if(s.length()<=3){
                    emailCheck.setVisibility(View.GONE);
                    emailProgressBar.setVisibility(View.GONE);
                    email.setError(null);
                    return;
                }
                if(!isValidEmail(s)){
                    email.setError("Please input a valid email");
                    emailCheck.setVisibility(View.GONE);
                    emailProgressBar.setVisibility(View.GONE);
                    return;
                }
                Call<AvailabilityResponse> call=mService.checkEmailIsAvailable(s.toString());
                call.enqueue(new Callback<AvailabilityResponse>() {
                    @Override
                    public void onResponse(Call<AvailabilityResponse> call, Response<AvailabilityResponse> response) {
                        if(response.isSuccessful()){
                            if(!response.body().getResponse()){
                                emailCheck.setVisibility(View.VISIBLE);
                                emailProgressBar.setVisibility(View.GONE);
                                email.setError(null);
                                return;
                            }else{
                                emailCheck.setVisibility(View.GONE);
                                emailProgressBar.setVisibility(View.GONE);
                                email.setError("Email has already been registered, pls login with it");
                            }
                        }else {}
                    }

                    @Override
                    public void onFailure(Call<AvailabilityResponse> call, Throwable t) {
                        Toast.makeText(CustomActivity.this,"Error connecting to the internet" ,Toast.LENGTH_SHORT).show();
                    }
                });


            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usernameProgressBar.setVisibility(View.VISIBLE);
                if(s.length()<=3){
                    username.setError("Make your username longer than 3 characters");
                    usernameCheck.setVisibility(View.GONE);
                    usernameProgressBar.setVisibility(View.GONE);
                    return;
                }
                Call<AvailabilityResponse> call=mService.checkUsernameIsAvailable(s.toString());
                call.enqueue(new Callback<AvailabilityResponse>() {
                    @Override
                    public void onResponse(Call<AvailabilityResponse> call, Response<AvailabilityResponse> response) {
                        if(response.isSuccessful()){
                            if(!response.body().getResponse()){
                                usernameCheck.setVisibility(View.VISIBLE);
                                usernameProgressBar.setVisibility(View.GONE);
                                username.setError(null);
                                return;
                            }else{
                                usernameCheck.setVisibility(View.GONE);
                                usernameProgressBar.setVisibility(View.GONE);
                                username.setError("Username not available,choose another one");
                            }
                        }else {}
                    }

                    @Override
                    public void onFailure(Call<AvailabilityResponse> call, Throwable t) {
                        Toast.makeText(CustomActivity.this,"Error connecting to the internet" ,Toast.LENGTH_SHORT).show();
                    }
                });


            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.country_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getPosition("Nigeria"));
        dialogBuilder.setTitle("Register an account with us");
        dialogBuilder.setPositiveButton("REGISTER", null);
        final AlertDialog b = dialogBuilder.create();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.hide();
                loginDialog();
            }
        });
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button be= b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(email.getError()!=null ){
                            Toast.makeText(context,"Please provide a non-registered mail or login with the mail",Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(username.getError()!=null ){
                            Toast.makeText(context,"Please provide a non-registered username",Toast.LENGTH_LONG).show();
                            return;
                        }
                        int i=0,c=0;

                        EditText[] text={f_name,l_name,username,email,phone,password,address,city,state};
                        while(i<text.length){
                            if(text[i].getText().toString().equals("")){
                                //Raise error on textfield and stop the code here;
                                text[i].setError("This field is compulsory");
                                c++;
                            }
                            i++;
                        }
                        if(c==0){
                            Log.d("tag","sent register");
                            dialogue.setMessage("Registering... please wait");
                            dialogue.setCanceledOnTouchOutside(false);
                            dialogue.show();
                            register(openPaymentDialog,context,f_name.getText().toString(),l_name.getText().toString(),username.getText().toString(),email.getText().toString(),
                                    password.getText().toString(),address.getText().toString(),
                                    city.getText().toString(),state.getText().toString(),spinner.getSelectedItem().toString(),
                                    phone.getText().toString());


                            b.dismiss();
                        }

                    }
                });
            }
        });
        b.show();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public void alreadyloginDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.already_login_dialog, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final TextView loginInfo = (TextView) dialogView.findViewById(R.id.login_info);
        final SharedPreferences prefs = CustomActivity.this.getSharedPreferences("TAG",MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        String email=prefs.getString("email","User not logged in");
        loginInfo.setText("You are logged in as "+email);
        dialogBuilder.setPositiveButton("LOG OUT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {}
        });

        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //remove this and use a logout function to update UI and clear SF
                        editor.clear();
                        editor.apply();
                        b.dismiss();
                        updateUserInfoinDrawer();
                        Toast.makeText(context,"Successfully logged out",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        b.show();



    }

    public void loginDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.logindialog, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final EditText email = (EditText) dialogView.findViewById(R.id.email);
        final EditText password = (EditText) dialogView.findViewById(R.id.password);
        final TextView forgottenPassword = (TextView) dialogView.findViewById(R.id.fogottenPassword);
        dialogBuilder.setTitle("Provide your login credentials");
        dialogBuilder.setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        forgottenPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgottenPasswordDialog();
                b.dismiss();
            }
        });
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
               Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
               be.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       int i=0,c=0;

                       EditText[] text={email,password};
                       while(i<text.length){
                           if(text[i].getText().toString().equals("")){
                               //Raise error on textfield and stop the code here;
                               text[i].setError("This field is compulsory");
                               c++;
                           }
                           i++;
                       }
                       if(c==0){
                           dialogue.setMessage("Logging you in... please wait");
                           dialogue.setCancelable(false);
                           dialogue.setCanceledOnTouchOutside(false);
                           dialogue.show();
                            login(false,context,"","",email.getText().toString(),email.getText().toString(),
                                    password.getText().toString(),"","","","","");
                           b.dismiss();
                       }
                   }
               });
            }
        });
        b.show();



    }




    public void register(final boolean openPaymentDialog, final Context context, final String f_name, final String l_name, final String username, final String email, final String password, final String address, final String city,final String state, final String country, final String phone){
        Call<LoginResponse> call=mService.register(
                new RegisterData(email, username, password)
        );
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {

                    Log.i("sabdnd", "Successfully registered" + response.body().toString());
                    login(openPaymentDialog,context,f_name,l_name,username,email,password,address,city,state,country,phone);

                }else{
                    dialogue.dismiss();
                    Toast.makeText(context,"Not Successfully registered.Please try again",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                dialogue.dismiss();
                Toast.makeText(context,"Not Successfully registered.Please try again",Toast.LENGTH_SHORT).show();
                Log.e("hshshjshj", "Regisration Unable to submit post to API."
                        + t.getMessage());

            }
        });

    };


    public void login(final boolean openPaymentDialog, final Context context, final String f_name, final String l_name, final String username, final String email, final String password, final String address, final String city, final String state, final String country, final String phone){
        Call<LoginResponse> call=mService.login(
                new RegisterData(email, username, password)
        );
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {
                    Log.i("sabdnd", "Successfully logged in" + response.body().getAuthToken());
                    SharedPreferences.Editor editor = getSharedPreferences(
                            "TAG",MODE_PRIVATE
                    ).edit();
                    editor.putString("token",response.body().getAuthToken());
                    editor.putString("email",email);
                    editor.apply();

                    displayFirebaseRegId();
                    if (country!=""){
                        updateProfile(f_name,l_name,username, address, city,state, country,phone);
                    }else{
                        getUserName(username);
                        dialogue.dismiss();
                    }
                    if(openPaymentDialog){
                        Snackbar snackbar = Snackbar.make(navigationView, "Successfully logged in, you can now make the payment", Snackbar.LENGTH_SHORT);
                        snackbar.show();

                    }else{

                        Snackbar snackbar = Snackbar.make(navigationView, "Successfully logged in", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                isUserDesigner(context,username);

                }else{
                    dialogue.dismiss();

                    Snackbar snackbar = Snackbar.make(navigationView, "Incorrect id/password combination", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("hshshjshj", "Unable to submit post to API."
                        + t.getMessage());
                dialogue.dismiss();

                Snackbar snackbar = Snackbar.make(navigationView, "No internet connection,Try again", Snackbar.LENGTH_SHORT);
                snackbar.show();


            }
        });

    }



    public void isUserDesigner(final Context context,String username){
        Call<Designer> call=mService.getDesigner(username);
        //final MenuItem menuItem=menu.findItem(R.id.nav_dashboard);
        Menu xMenu=navigationView.getMenu();
        final MenuItem menuItem=xMenu.findItem(R.id.nav_dashboard);
        final SharedPreferences.Editor editor = getSharedPreferences(
                "TAG",MODE_PRIVATE
        ).edit();
        call.enqueue(new Callback<Designer>() {
            @Override
            public void onResponse(Call<Designer> call, Response<Designer> response) {
                if(response.isSuccessful()){
                    //show dashboard in menu
                    menuItem.setVisible(true);
                    editor.putBoolean("isDesigner",true);
                    editor.apply();
                }else{menuItem.setVisible(false);
                editor.putBoolean("isDesigner",false);}
            }

            @Override
            public void onFailure(Call<Designer> call, Throwable t) {
                menuItem.setVisible(false);
                editor.putBoolean("isDesigner",false);
            }
        });
    }

    public void updateProfile(final String f_name, final String l_name, final String username, String address, String city,String state, String country,String phone){
        Call<Profile> call=mService.updateProfile(
                new Profile(f_name,l_name,username, address, city, state, country,phone)
        );
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                dialogue.dismiss();
                if (response.isSuccessful()){
                    SharedPreferences.Editor editor = getSharedPreferences(
                            "TAG",MODE_PRIVATE
                    ).edit();
                    Intent intent=new Intent("get-user-address-in-review-fragment");
                    intent.putExtra("loadUserAddress",true);
                    LocalBroadcastManager.getInstance(CustomActivity.this).sendBroadcastSync(intent);
                    editor.putString("first_name",response.body().getFirstName());
                    editor.putString("last_name",response.body().getLastName());
                    editor.putString("country",response.body().getCountry());
                    editor.putString("city",response.body().getCity());
                    editor.putString("state",response.body().getState());
                    editor.apply();
                    Toast.makeText(CustomActivity.this,
                                    "Profile updated",
                                    Toast.LENGTH_LONG).show();
                    updateUserInfoinDrawer();

                }else{
                    Toast.makeText(CustomActivity.this,
                            "Error updating server",
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                dialogue.dismiss();
                Toast.makeText(CustomActivity.this,
                        "No connection to update profile",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getUserName(String username){
        Call<GetUserNames> call=mService.getUserNames(new User("",username,""));
        call.enqueue(new Callback<GetUserNames>() {
            @Override
            public void onResponse(Call<GetUserNames> call, Response<GetUserNames> response) {
                if (response.isSuccessful()){
                    SharedPreferences.Editor editor = getSharedPreferences(
                            "TAG",MODE_PRIVATE
                    ).edit();
                    editor.putString("first_name",response.body().getFirstName());
                    editor.putString("last_name",response.body().getLastName());
                    editor.putString("country",response.body().getCountry());
                    editor.putString("city",response.body().getCity());
                    editor.apply();
                    Intent intent=new Intent("get-user-address-in-review-fragment");
                    intent.putExtra("loadUserAddress",true);
                    LocalBroadcastManager.getInstance(CustomActivity.this).sendBroadcastSync(intent);
                    updateUserInfoinDrawer();
                }else{
                    Toast.makeText(CustomActivity.this,"Server error:Unable to get user profile",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetUserNames> call, Throwable t) {
                Toast.makeText(CustomActivity.this,"Network error:Unable to get user profile",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        imgProfile.setImageBitmap(selectedImage);
                        SharedPreferences.Editor editor = getSharedPreferences(
                                "TAG",MODE_PRIVATE
                        ).edit();
                        String realPathfromURI=getRealPathFromURI(imageUri);
                        Toast.makeText(getApplicationContext(),realPathfromURI,Toast.LENGTH_SHORT).show();
                        editor.putString("profile_uri",realPathfromURI);
                        editor.apply();


                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
        }
    }



    public void updateUserInfoinDrawer(){
        SharedPreferences prefs = this.getSharedPreferences("TAG",MODE_PRIVATE);
        navHeader = navigationView.getHeaderView(0);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
        Button editProfileButton = (Button) navHeader.findViewById(R.id.edit_profile_button);
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!verifyStoragePermissions(CustomActivity.this)){
                    return;
                }
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtEmail = (TextView) navHeader.findViewById(R.id.website);
        String name = prefs.getString("first_name","Anonymous") +" "+ prefs.getString("last_name","User");
        String email=prefs.getString("email","User not logged in");
        String profileImage=prefs.getString("profile_uri","null");
        if(!profileImage.equals("null")){
            File file = new File(profileImage);
            if(file.exists()){

                Glide.with(this.getApplicationContext()).load(file).into(imgProfile);
                //Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                //imgProfile.setImageURI(Uri.fromFile(file));

            }else{
                Toast.makeText(this, "Please update your profile image as the last one has been deleted", Toast.LENGTH_LONG).show();
            }

        }else{
            imgProfile.setImageResource(R.drawable.unknownprofile);

        }
        editProfileButton.setVisibility(View.INVISIBLE);
        Menu xMenu=navigationView.getMenu();
        if(prefs.contains("email")){
            editProfileButton.setVisibility(View.VISIBLE);
            editProfileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CustomActivity.this,
                            EditProfileActivity.class);
                    CustomActivity.this.startActivity(intent);
                }
            });
        }else{
            final MenuItem menuItem=xMenu.findItem(R.id.nav_dashboard);
            menuItem.setVisible(false);

        }
        txtName.setText(name);
        txtEmail.setText(email);
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (Build.VERSION.SDK_INT >= 19) {
            String[] proj = { MediaStore.Images.Media.DATA };
            CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();
            return result;
        }
        if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 18) {
            String[] proj = { MediaStore.Images.Media.DATA };
            String result = null;
            android.content.CursorLoader cursorLoader = new android.content.CursorLoader(
                    this,
                    contentUri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            if(cursor != null){
                int column_index =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        }

        if (Build.VERSION.SDK_INT < 11) {
            String[] proj = { MediaStore.Images.Media.DATA };
            Cursor cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index
                    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return null;
    }

    public void faqDialog(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogView = inflater.inflate(R.layout.faqs_diaog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("FAQs & Policy");
        AlertDialog b;
        b = dialogBuilder.create();
        b.setCanceledOnTouchOutside(true);
        b.show();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        if (!TextUtils.isEmpty(regId)) {
            //   Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_SHORT).show();
            sendRegistrationToServer(regId);
        }
        else {
            // Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Call<CreateStyleResponse> call = mService.updateUserappToken(token);
        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    // Toast.makeText(getApplicationContext(),"Successfully added token to user profile",Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(getApplicationContext(),"Server error...unable to add token to user profile",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                // Toast.makeText(getApplicationContext(),"No internet connection to update token DB",Toast.LENGTH_SHORT).show();
            }
        });

    }


    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    //persmission method.
    public static boolean verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            return false;
        }

        return true;

    }




    public void confirmPasswordDialog(final String uid,final String token) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.confirm_password_dialog, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final EditText password = (EditText) dialogView.findViewById(R.id.password);
        dialogBuilder.setTitle("Forgotten Password");
        dialogBuilder.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
            }
        });
        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String passwordText =password.getText().toString();
                        if(passwordText.length()<4){
                            password.setError("Please input a longer password");
                            return;
                        }
                        dialogue.setMessage("Changing... please wait");
                        dialogue.setCancelable(false);
                        dialogue.setCanceledOnTouchOutside(false);
                        dialogue.show();
                        Call<ResponseBody> call = mService.confirmPasswordReset(new PasswordChange(uid,token,passwordText));
                                call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                dialogue.dismiss();
                                if(response.isSuccessful()){
                                    Snackbar.make(navigationView,"Password reset successfully,please log in again with new password.",Snackbar.LENGTH_LONG).show();
                                    b.dismiss();

                                }
                                else{
                                    Toast.makeText(CustomActivity.this,
                                            "Sorry,a server error occured.",
                                            Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                dialogue.dismiss();
                                Toast.makeText(CustomActivity.this,
                                        "No connection to reset password.TRY AGAIN",
                                        Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        });
                    }
                });
            }
        });
        b.show();



    }




    public void forgottenPasswordDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.email_forgotten_password, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final EditText email = (EditText) dialogView.findViewById(R.id.email);
        dialogBuilder.setTitle("Forgotten Password");
        dialogBuilder.setPositiveButton("SEND", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
            }
        });
        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String emailText = email.getText().toString();
                        if(!isValidEmail(emailText)){
                            email.setError("Please input a valid email");
                            return;
                        }
                        dialogue.setMessage("Checking... please wait");
                        dialogue.setCancelable(false);
                        dialogue.setCanceledOnTouchOutside(false);
                        dialogue.show();
                        Call<ResponseBody> call = mService.sendEmailForPasswordReset(new EmailCred(emailText));
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                dialogue.dismiss();
                                if(response.isSuccessful()){
                                        Snackbar.make(navigationView,"Password reset have been sent to your mail",Snackbar.LENGTH_LONG).show();
                                        b.dismiss();

                                    }
                                    else{
                                        email.setError("This email is not registered with us");
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    dialogue.dismiss();
                                    Toast.makeText(CustomActivity.this,
                                            "No connection to reset password.TRY AGAIN",
                                            Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                            });
                    }
                });
            }
        });
        b.show();



    }

}


