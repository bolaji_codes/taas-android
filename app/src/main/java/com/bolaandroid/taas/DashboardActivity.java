package com.bolaandroid.taas;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.Designer;
import com.bolaandroid.data.model.DesignerStats;
import com.bolaandroid.data.model.remote.TAASService;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

import static com.bolaandroid.taas.CustomActivity.verifyStoragePermissions;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener{
    private TAASService mService;
    RelativeLayout ratingNumLayout,profileLayout,stylesLayout,fabricCollectionLayout,
            fabricsLayout,undeliveredLayout,accessoriesLayout,ordersLayout;
    TextView ratingNum,profile,styles,fabricCollection,fabrics,undelivered,accessories,orders;
    ProgressBar[] progressBars;
    CardView cardView;
    ImageView profileImage,defaultProfileImage;
    DesignerStats designerStats;
    Toolbar toolbar;
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mService = ApiUtils.getTAASService();
        //swipeRefreshLayout = (MultiSwipeRefreshLayout) findViewById(R.id.swipe);
        ratingNumLayout=(RelativeLayout) findViewById(R.id.rating_num_layout);
        profileLayout= (RelativeLayout) findViewById(R.id.profile_image_layout);
        stylesLayout=(RelativeLayout) findViewById(R.id.styles_layout);
        fabricCollectionLayout=(RelativeLayout) findViewById(R.id.fab_collection_layout);
        fabricsLayout=(RelativeLayout) findViewById(R.id.fabric_layout);
        undeliveredLayout=(RelativeLayout) findViewById(R.id.undelivered_num_layout);
        accessoriesLayout=(RelativeLayout) findViewById(R.id.accessories_num_layout);
        ordersLayout=(RelativeLayout) findViewById(R.id.orders_num_layout);
        cardView = (CardView) findViewById(R.id.view2);
        profileImage = (ImageView) findViewById(R.id.img_profile);
        defaultProfileImage = (ImageView) findViewById(R.id.profile_image);
        ratingNumLayout.setBackgroundColor(Color.parseColor("#808080"));
        profileLayout.setBackgroundColor(Color.parseColor("#008080"));
        stylesLayout.setBackgroundColor(ContextCompat.getColor(this,android.R.color.holo_green_light));
        fabricCollectionLayout.setBackgroundColor(ContextCompat.getColor(this,android.R.color.holo_blue_light));
        fabricsLayout.setBackgroundColor(Color.parseColor("#800080"));
        undeliveredLayout.setBackgroundColor(Color.parseColor("#FFA500"));
        accessoriesLayout.setBackgroundColor(Color.parseColor("#000000"));
        ordersLayout.setBackgroundColor(ContextCompat.getColor(this,R.color.colorAccent));
        ratingNum = (TextView) findViewById(R.id.rating_num);
        styles = (TextView) findViewById(R.id.styles_num);
        fabricCollection = (TextView) findViewById(R.id.fab_collection_num);
        fabrics = (TextView) findViewById(R.id.fabric_num);
        undelivered = (TextView) findViewById(R.id.undelivered_num);
        accessories = (TextView) findViewById(R.id.accessories_num);
        orders= (TextView) findViewById(R.id.orders_num);
        accessoriesLayout.setOnClickListener(this);
        stylesLayout.setOnClickListener(this);
        fabricsLayout.setOnClickListener(this);
        accessoriesLayout.setOnClickListener(this);
        ordersLayout.setOnClickListener(this);
        undeliveredLayout.setOnClickListener(this);
        fabricCollectionLayout.setOnClickListener(this);
        ratingNumLayout.setOnClickListener(this);
        profileLayout.setOnClickListener(this);
        progressBars=loadProgressBar();
        loadStats(true);
        if(!verifyStoragePermissions(this)){}

    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.accessories_num_layout:
                Intent intent2 = new Intent(DashboardActivity.this,
                        AccessoriesActivity.class);
                 DashboardActivity.this.startActivity(intent2);
                break;
            case R.id.styles_layout:
                Intent intent = new Intent(DashboardActivity.this,
                     StylesActivity.class);
                 DashboardActivity.this.startActivity(intent);
                break;
            case R.id.fabric_layout:
                Intent intent1 = new Intent(DashboardActivity.this,
                       FabricsActivity.class);
                DashboardActivity.this.startActivity(intent1);
                break;
            case R.id.orders_num_layout:
                 Intent intent4 = new Intent(DashboardActivity.this,
                      AllOrdersActivity.class);
                 DashboardActivity.this.startActivity(intent4);
                break;
            case R.id.undelivered_num_layout:
                 Intent intent5 = new Intent(DashboardActivity.this,
                      UndeliveredOrdersActivity.class);
                 DashboardActivity.this.startActivity(intent5);
                break;
            case R.id.fab_collection_layout:
                 Intent intent6 = new Intent(DashboardActivity.this,
                             FabricCollectionsActivity.class);
                 DashboardActivity.this.startActivity(intent6);
                break;
            case R.id.rating_num_layout:
                 Intent intent3 = new Intent(DashboardActivity.this,
                      RatingsActivity.class);
                 DashboardActivity.this.startActivity(intent3);
                break;
            case R.id.profile_image_layout:
                if(designerStats!=null){
                    detailsDialog(this,designerStats.getDesigner());
                }else{
                    Toast.makeText(this,"Please wait lets fetch your data first",Toast.LENGTH_SHORT).show();
                }
        }


    }

    public String toStringz(Integer number){
        return String.valueOf(number);
    }

    public void loadStats(boolean isLoadSpinner){
        if(isLoadSpinner){
            for(ProgressBar bar:progressBars){
                bar.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                bar.setVisibility(View.VISIBLE);
            }
        }

        Call<DesignerStats> call=mService.getDesignerStats();
        call.enqueue(new Callback<DesignerStats>() {
            @Override
            public void onResponse(Call<DesignerStats> call, Response<DesignerStats> response) {
                if(response.isSuccessful()){
                    for(ProgressBar bar:progressBars){
                        bar.setVisibility(View.GONE);
                    }
                    designerStats=new DesignerStats();
                    designerStats = response.body();
                    updateUIStats(designerStats.getDesigner().getDp(),toStringz(designerStats.getOrdersNum()),toStringz(designerStats.getFabricCollectionNumber()),toStringz(designerStats.getUnDeliveredOrdersNum()),
                            toStringz(designerStats.getAccessoriesNum()),toStringz(designerStats.getFabricsNum()),String.valueOf(designerStats.getRating()),toStringz(designerStats.getStylesNum()));

                }else{
                    for(ProgressBar bar:progressBars){
                        bar.setVisibility(View.GONE);
                    }
                    final Snackbar snackbar = Snackbar.make(styles, "Error getting your info!", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadStats(true);
                        }
                    });
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<DesignerStats> call, Throwable t) {
                for(ProgressBar bar:progressBars){
                    bar.setVisibility(View.GONE);
                }
                final Snackbar snackbar = Snackbar.make(styles, "No internet  connection!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadStats(true);
                    }
                });
                snackbar.show();
            }
        });
        //swipeRefreshLayout.setRefreshing(true);
    }

    private void updateUIStats(String dp,String ordersNum,String fabricCollectionNo,String undeliveredNum,String accessoriesNum,String fabricsNum,String rating,String stylesNum){
        orders.setText(ordersNum);
        undelivered.setText(undeliveredNum);
        accessories.setText(accessoriesNum);
        fabrics.setText(fabricsNum);
        ratingNum.setText(rating);
        styles.setText(stylesNum);
        fabricCollection.setText(fabricCollectionNo);
        if(dp!=null){
            defaultProfileImage.setVisibility(View.GONE);
            cardView.setVisibility(View.VISIBLE);
            Picasso.with(this).load(dp).into(profileImage);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item:
                //do stuffs here
                Toast.makeText(DashboardActivity.this,"Dont click",Toast.LENGTH_SHORT).show();
                return true;
            case android.R.id.home:
                finish();
                return true;
            case R.id.menu_refresh:
                loadStats(true);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public ProgressBar[] loadProgressBar(){
        ProgressBar ratingProgress,profileProgress,styleProgress,fabricCollectionProgress,
                fabricProgress,undeliveredOrderProgress,accessoriesProgress,orderProgress;
        ratingProgress = (ProgressBar) findViewById(R.id.rating_num_bar);
        styleProgress = (ProgressBar) findViewById(R.id.styles_bar);
        fabricCollectionProgress=(ProgressBar) findViewById(R.id.fab_collection_bar);
        fabricProgress=(ProgressBar) findViewById(R.id.fabric_bar);
        undeliveredOrderProgress=(ProgressBar) findViewById(R.id.undelivered_num_bar);
        accessoriesProgress=(ProgressBar) findViewById(R.id.accessories_num_bar);
        orderProgress=(ProgressBar) findViewById(R.id.orders_num_bar);
        ProgressBar[] progressBars = {ratingProgress,styleProgress,fabricCollectionProgress,fabricProgress,undeliveredOrderProgress,accessoriesProgress,orderProgress};
        return progressBars;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_dashboard,menu);
        MenuItem item = menu.findItem(R.id.menu_item);
        item.setVisible(false);
        return true;
    }

    public void detailsDialog(Context context, final Designer designer) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        final View dialogView = inflater.inflate(R.layout.profile_view_details, null);
        dialogBuilder.setView(dialogView);
        final ImageView dp = (ImageView) dialogView.findViewById(R.id.dp);
        final ImageView cover = (ImageView) dialogView.findViewById(R.id.cover);
        final TextView deliveryName = (TextView) dialogView.findViewById(R.id.delivery_name);
        final TextView name = (TextView) dialogView.findViewById(R.id.designer_name);
        final TextView designerBio = (TextView) dialogView.findViewById(R.id.designer_bio);
        final TextView addressName = (TextView) dialogView.findViewById(R.id.address_name);
        final TextView city = (TextView) dialogView.findViewById(R.id.city_name);
        final ImageView close = (ImageView) dialogView.findViewById(R.id.close_icon);
        final TextView holidayMode = (TextView) dialogView.findViewById(R.id.holiday_mode);
        final ImageView editProfile = (ImageView) dialogView.findViewById(R.id.edit_icon);
        dialog = dialogBuilder.create();
        dialogBuilder.setCancelable(true);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(DashboardActivity.this,
                        EditDesignerProfileActivity.class).putExtra("designer",designer);
                DashboardActivity.this.startActivity(intent1);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if(designer.getDp()!=null){
            Picasso.with(context).load(designer.getDp()).into(dp);
        }else{
            dp.setImageResource(R.drawable.ic_person_icon);
        }
        if(designer.getHolidayMode()){
            holidayMode.setText("ON");
        }else {
            holidayMode.setText("OFF");
        }

        Picasso.with(context).load(designer.getCoverPhoto()).into(cover);
        name.setText(designer.getName());
        city.setText(designer.getCity()+","+designer.getState()+","+designer.getCountry().getName());
        designerBio.setText(designer.getBio());
        addressName.setText(designer.getAddress());
        deliveryName.setText(designer.getDeliveryStatement());
        dialog.show();
    }

    @Override
    public void onResume(){
        super.onResume();
        loadStats(false);

        if(dialog!=null){
            dialog.dismiss();
        }


    }



}
