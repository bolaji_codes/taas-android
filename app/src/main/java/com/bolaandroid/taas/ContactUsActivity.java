package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bolaandroid.data.model.ContactUsMessage;
import com.bolaandroid.data.model.remote.TAASService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class ContactUsActivity extends AppCompatActivity {
    Toolbar toolbar;
    Spinner reasonSpinner;
    ProgressDialog dialog;
    EditText phoneEdit,nameEdit,emailEdit,messageEdit;
    TAASService mService;
    Button submitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mService = ApiUtils.getTAASService();
        getSupportActionBar().setTitle("Contact Us");
        reasonSpinner = (Spinner) findViewById(R.id.reason_spinner);
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.contact_reason_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reasonSpinner.setAdapter(adapter);
        dialog = new ProgressDialog(this);
        phoneEdit = (EditText) findViewById(R.id.phoneEdit);
        nameEdit = (EditText) findViewById(R.id.nameEdit);
        emailEdit = (EditText) findViewById(R.id.emailEdit);
        messageEdit = (EditText) findViewById(R.id.msgEdit);
        submitBtn = (Button) findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
    }

    public void sendMessage(){

        if(!cleanMessage()){
            loadDialog(false);
            return;
        }

        loadDialog(true);

        Call<ContactUsMessage> call = mService.sendMessageToAdmin(
                new ContactUsMessage(nameEdit.getText().toString(),
                        phoneEdit.getText().toString(),
                        emailEdit.getText().toString(),
                        messageEdit.getText().toString(),
                        reasonSpinner.getSelectedItem().toString())
        );
        call.enqueue(new Callback<ContactUsMessage>() {
            @Override
            public void onResponse(Call<ContactUsMessage> call, Response<ContactUsMessage> response) {
                loadDialog(false);
                if(response.isSuccessful()){
                    Toast.makeText(ContactUsActivity.this,"Message successfully sent.Thank you for your feedback",Toast.LENGTH_LONG).show();
                    ContactUsActivity.this.finish();
                }else {
                    Toast.makeText(ContactUsActivity.this,"Error sending message.Please try again later",Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<ContactUsMessage> call, Throwable t) {
                loadDialog(false);
                Toast.makeText(ContactUsActivity.this,"No internet connection.Please try again later",Toast.LENGTH_LONG).show();
            }
        });

    }

    public boolean cleanMessage(){
        boolean isClean=true;
       if (nameEdit.getText().toString().matches("")){
           nameEdit.setError("Your name is compulsory");
           isClean=false;
       }
        if (phoneEdit.getText().toString().matches("")){
            phoneEdit.setError("Your phone is compulsory");
            isClean=false;
        }
        if (emailEdit.getText().toString().matches("")){
            emailEdit.setError("Your email is compulsory");
            isClean=false;
        }

        if (messageEdit.getText().toString().matches("")){
            messageEdit.setError("Message is compulsory");
            isClean=false;
        }
        return isClean;



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadDialog(boolean shouldLoad) {
        if (shouldLoad) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.setMessage("Sending your message...please wait a moment");
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setIndeterminate(false);
                    dialog.show();
                }
            }, 100);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 100);
        }
    }
}
