package com.bolaandroid.taas;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.MeasurementAdapter;
import com.bolaandroid.provider.MeasurementProvider;

public class MeasurementActivity extends AppCompatActivity {
    RecyclerView recyclerView,recyclerView2;
    Button button;
    TextView noMaleMeasurement,noFemaleMeasurement;
    MeasurementAdapter adapter,adapter2 = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        noFemaleMeasurement=(TextView) findViewById(R.id.no_female_measurement_text);
        noMaleMeasurement = (TextView) findViewById(R.id.no_male_measurement_text);
        adapter=new MeasurementAdapter(this,false);
        adapter2=new MeasurementAdapter(this,false);
        recyclerView=(RecyclerView) findViewById(R.id.female_recycler);
        recyclerView2=(RecyclerView) findViewById(R.id.male_recycler);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(this));
        recyclerView2.setAdapter(adapter2);
        recyclerView2.setHasFixedSize(true);
        recyclerView2.setLayoutManager(
                new LinearLayoutManager(this));
        getSupportLoaderManager().initLoader(0,null,getMeasurements);
        getSupportLoaderManager().initLoader(1,null,getMeasurements);
        button=(Button) findViewById(R.id.findSelected);
        // Capture button clicks
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(MeasurementActivity.this,R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Gender")
                        .setMessage("What gender does the person belongs to?")
                        .setCancelable(true)
                        .setNegativeButton(R.string.male, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialogInterface,
                                    int i)
                            {
                                String male="M";
                                Intent myIntent = new Intent(MeasurementActivity.this,
                                        EditMeasurementActivity.class).putExtra(EditMeasurementActivity.MEASUREMENT_ID,0);
                                myIntent.putExtra("styleGender",male);
                                startActivity(myIntent);
                            }



                        })
                        .setPositiveButton(
                                R.string.female,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialogInterface,
                                            int i)
                                    {
                                        String female="F";
                                            // Start NewActivity.class
                                            Intent myIntent = new Intent(MeasurementActivity.this,
                                                    EditMeasurementActivity.class).putExtra(EditMeasurementActivity.MEASUREMENT_ID,0);
                                            myIntent.putExtra("styleGender",female);
                                            startActivity(myIntent);
                                    }
                                });

                AlertDialog alert=builder.create();
                alert.show();


            }
        });
    }


    private LoaderManager.LoaderCallbacks<Cursor> getMeasurements = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {

            if(id==0){
                return new CursorLoader(MeasurementActivity.this,
                        MeasurementProvider.CONTENT_URI, null,MeasurementProvider.COLUMN_GENDER + "=?",new String[] {"F"}, null);

            }else {
                return new CursorLoader(MeasurementActivity.this,
                        MeasurementProvider.CONTENT_URI, null,MeasurementProvider.COLUMN_GENDER + "=?",new String[] {"M"}, null);

            }
            }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            if(loader.getId()==0){
                    adapter.swapCursor(data);
                    noFemaleMeasurement.setText("You haven't added any female measurement yet.");
                if (data.getCount() == 0) {
                    noFemaleMeasurement.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }else{
                    noFemaleMeasurement.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }else{
                    adapter2.swapCursor(data);
                noMaleMeasurement.setText("You haven't added any male measurement yet.");
                if (data.getCount() == 0) {
                    noMaleMeasurement.setVisibility(View.VISIBLE);
                    recyclerView2.setVisibility(View.GONE);
                }else{
                    noMaleMeasurement.setVisibility(View.GONE);
                    recyclerView2.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            if(loader.getId()==0){
                adapter.swapCursor(null);
            }else{
                adapter2.swapCursor(null);
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
