package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.remote.TAASService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.ProgressRequestBody;

import static android.text.TextUtils.isEmpty;
import static com.bolaandroid.taas.AddNewStyles.verifyStoragePermissions;

public class AddAccessoriesActivity extends AppCompatActivity implements View.OnClickListener, ProgressRequestBody.UploadCallbacks{
    private final int SELECT_PHOTO = 1;
    EditText titleEdit,priceEdit;
    ImageView img1;
    Spinner gender;
    TAASService mService;
    File file1;
    Integer bigPercentage=0;
    ProgressDialog dialog;
    Button button;
    Long filesSize;
    Long mUploadedSize;
    Toolbar toolbar;
    NumberPicker numberPicker;
    ArrayList<File> arrayListFiles=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_accessories);
        mService = ApiUtils.getTAASService();
        dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Accessories");
        numberPicker = (NumberPicker) findViewById(R.id.number_picker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(100);
        numberPicker.setValue(1);
        titleEdit = (EditText) findViewById(R.id.name);
        img1 = (ImageView) findViewById(R.id.image1);
        priceEdit = (EditText) findViewById(R.id.price);
        button = (Button) findViewById(R.id.button);
        mUploadedSize=Long.valueOf(0);
        gender = (Spinner) findViewById(R.id.gender_spinner);
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.gender_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);
        img1.setOnClickListener(this);
        button.setOnClickListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        String realPathfromURI=getRealPathFromURI(imageUri);
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        if(img1.getDrawable().getConstantState() != getResources().getDrawable(R.mipmap.add_image_icon).getConstantState()){
                            arrayListFiles.remove(file1);
                        }
                        img1.setImageBitmap(selectedImage);
                            file1=new File(realPathfromURI);
                            if(file1.length()/1024 > 300){
                                file1=compressFile(file1);
                            }
                            arrayListFiles.add(file1);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
        }
    }

    public void addAccessoriestoServer(){
        if(!cleanForm()){
            return;
        }

        if(!verifyStoragePermissions(this)){
            return;
        }

        getArrayFileSize();

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                dialog.setMessage(bigPercentage+"%"+ " uploaded");
                dialog.setIndeterminate(true);
                dialog.setProgress(bigPercentage);
                dialog.show();
            }
        },100);

        MultipartBody.Part filePart;
        filePart=null;
        if(img1.getDrawable().getConstantState()!=getDrawable(R.mipmap.add_image_icon).getConstantState()){
            ProgressRequestBody fileBody = new ProgressRequestBody(file1, this);
            filePart = MultipartBody.Part.createFormData("photo1", file1.getName(), fileBody);

        }


        RequestBody title1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), titleEdit.getText().toString());

        RequestBody price1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), priceEdit.getText().toString());
        String genderAbbrev;
        if(gender.getSelectedItem().toString().equals("Male")){
            genderAbbrev="M";
        }else {
            genderAbbrev = "F";
        }
        RequestBody fabricStock =RequestBody.create(
                MediaType.parse("multipart/form-data"), String.valueOf(numberPicker.getValue()));

        RequestBody gender1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), genderAbbrev);
        Call<CreateStyleResponse> call=mService.createAccessory(title1,gender1,price1,filePart,fabricStock);
        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(AddAccessoriesActivity.this, "Successfully uploaded", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    AddAccessoriesActivity.this.finish();
                }else{
                    Toast.makeText(AddAccessoriesActivity.this, "Internal Server error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                Toast.makeText(AddAccessoriesActivity.this, "Unable to upload to server" , Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

    }



    public Long getArrayFileSize(){
        filesSize=Long.valueOf(0);
        for(File file:arrayListFiles){
            filesSize+=file.length();
        }
        return filesSize;
    }

    public File compressFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean cleanForm(){
        boolean canSendData=true;
        if(isEmpty(titleEdit.getText().toString().trim())){
            titleEdit.requestFocus();
            titleEdit.setError("Please fill in the title");
            canSendData=false;
        }
        if(isEmpty(priceEdit.getText().toString().trim())){
            priceEdit.requestFocus();
            priceEdit.setError("Please fill in the price");
            canSendData=false;
        }

        String actualPostionOfSpinner = (String) gender.getItemAtPosition(gender.getSelectedItemPosition());
        if(actualPostionOfSpinner.isEmpty()){
            setSpinnerError(gender,"FabricCollection can't be empty");
            canSendData=false;
        }

        if(img1.getDrawable().getConstantState()==getDrawable(R.mipmap.add_image_icon).getConstantState()){
            Toast.makeText(this,"You have to upload at least an image",Toast.LENGTH_SHORT).show();
            canSendData=false;
        }
        return canSendData;
    }

    private void setSpinnerError(Spinner spinner, String error){
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError(error); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
            spinner.performClick(); // to open the spinner list if error is found.

        }
    }

    @Override
    public void onProgressUpdate(long uploadedSize,File file) {
        if(file.getPath().equals(file1.getPath())){
            bigPercentage =(int)(100 * mUploadedSize / filesSize);
            mUploadedSize=uploadedSize;
        }else{
            Long totalSizeUploaded=mUploadedSize+uploadedSize;
            bigPercentage = (int)(100 * totalSizeUploaded / filesSize);
        }
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                dialog.setMessage(bigPercentage+"%"+ " uploaded" +".Please wait a sec");
            }
        },100);
    }

    @Override
    public void onError() {
        // do something on error
        Toast.makeText(AddAccessoriesActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFinish() {
        // do something on upload finished
        // for example start next uploading at queue
        Toast.makeText(AddAccessoriesActivity.this, "Finished...Glad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.image1:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.putExtra("image1",1);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                break;

            case R.id.button:
                addAccessoriestoServer();
                break;
            default:

                break;

        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (Build.VERSION.SDK_INT >= 19) {
            String[] proj = { MediaStore.Images.Media.DATA };
            CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();
            return result;
        }
        if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 18) {
            String[] proj = { MediaStore.Images.Media.DATA };
            String result = null;
            android.content.CursorLoader cursorLoader = new android.content.CursorLoader(
                    this,
                    contentUri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            if(cursor != null){
                int column_index =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        }

        if (Build.VERSION.SDK_INT < 11) {
            String[] proj = { MediaStore.Images.Media.DATA };
            Cursor cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index
                    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
