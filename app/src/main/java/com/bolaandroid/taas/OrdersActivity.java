package com.bolaandroid.taas;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.OrdersAdapter;
import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.DetailedUserOrder;
import com.bolaandroid.data.model.User;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.firebasenotification.app.Config;
import com.bolaandroid.firebasenotification.utils.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class OrdersActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    private TAASService mService;
    ArrayList<DetailedUserOrder> orders=new ArrayList<>();
    OrdersAdapter adapter;
    TextView noListView;
    LinearLayout linearlayout;
    private static final String TAG = OrdersActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        mService = ApiUtils.getTAASService();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        linearlayout = (LinearLayout) findViewById(R.id.linearlayout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Orders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        noListView = (TextView) findViewById(R.id.no_list_text);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        getUserOrders();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    //Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        displayFirebaseRegId();
    }

    public void getUserOrders(){
        swipeRefreshLayout.setRefreshing(true);
        SharedPreferences prefs = this.getSharedPreferences("TAG",MODE_PRIVATE);
        String email;
        if(prefs.contains("token")){
            email=prefs.getString("email",null);
        }else{
            Toast.makeText(this,"You have to be logged in to see this page",Toast.LENGTH_SHORT).show();
            recyclerView.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.GONE);
            noListView.setVisibility(View.VISIBLE);
            noListView.setText("You have to be logged in to see this page");
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        Call<ArrayList<DetailedUserOrder>> call=mService.getUserOrders(
                new User("",email,"")
        );
        call.enqueue(new Callback<ArrayList<DetailedUserOrder>>() {
            @Override
            public void onResponse(Call<ArrayList<DetailedUserOrder>> call, Response<ArrayList<DetailedUserOrder>> response) {
                if(response.isSuccessful()){
                    orders=response.body();
                    if(orders.isEmpty()){
                        swipeRefreshLayout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    loadlayout(orders);
                    swipeRefreshLayout.setRefreshing(false);

                }
                else{
                    final Snackbar snackbar= Snackbar.make(linearlayout,"Server error",Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY",new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {
                            getUserOrders();
                        }
                    });
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DetailedUserOrder>> call, Throwable t) {
                final Snackbar snackbar= Snackbar.make(linearlayout,"No internet connection",Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY",new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        getUserOrders();
                    }
                });
                snackbar.show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    public void loadlayout(ArrayList<DetailedUserOrder> orders){
        adapter = new OrdersAdapter(orders);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        getUserOrders();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {
            //   Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_SHORT).show();
            sendRegistrationToServer(regId);
        }
        else {
            // Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        Call<CreateStyleResponse> call = mService.updateUserappToken(token);
        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    // Toast.makeText(getApplicationContext(),"Successfully added token to user profile",Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(getApplicationContext(),"Server error...unable to add token to user profile",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                // Toast.makeText(getApplicationContext(),"No internet connection to update token DB",Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

}
