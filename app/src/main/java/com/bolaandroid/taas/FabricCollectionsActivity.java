package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.FabricCollectionsAdapter;
import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.FabricCollection;
import com.bolaandroid.data.model.remote.TAASService;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

import static android.text.TextUtils.isEmpty;

public class FabricCollectionsActivity extends AppCompatActivity {
    TAASService mService;
    RecyclerView recyclerView;
    FabricCollectionsAdapter adapter;
    ProgressBar progressBar;
    ProgressDialog dialog;
    Toolbar toolbar;
    TextView noListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fabric_collections);
        mService= ApiUtils.getTAASService();
        dialog = new ProgressDialog(this);
        noListView = (TextView) findViewById(R.id.no_list_text);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Fabric Collections");
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        loadFabricCollections();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_dashboard,menu);
        return true;
    }

    public void loadFabricCollections(){
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<FabricCollection>> call = mService.getDesignerFabricCollections();
        call.enqueue(new Callback<ArrayList<FabricCollection>>() {
            @Override
            public void onResponse(Call<ArrayList<FabricCollection>> call, Response<ArrayList<FabricCollection>> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    loadLayout(response.body());
                    if(response.body().isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }else{
                    progressBar.setVisibility(View.GONE);
                    final Snackbar snackbar= Snackbar.make(recyclerView,"Server error", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY",new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {
                        loadFabricCollections();
                        }
                    });
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FabricCollection>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                final Snackbar snackbar= Snackbar.make(recyclerView,"No internet connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY",new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        loadFabricCollections();
                    }
                });
                snackbar.show();
            }
        });
    }


    public void loadLayout(ArrayList<FabricCollection> fabricCollections){
        adapter=new FabricCollectionsAdapter(fabricCollections);
        recyclerView.setHasFixedSize(true);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            this.getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(displayMetrics);
        }catch (Exception e){}
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        if (width>=1180){
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
            );
        }else{
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        };
        //Toast.makeText(getContext(), "2:", Toast.LENGTH_SHORT).show();
        recyclerView.setAdapter(adapter);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item:
                addFabricCollectionDialog();
                return true;
            case R.id.menu_refresh:
                loadFabricCollections();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public void addFabricCollectionDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.addfabriccollection, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final EditText email = (EditText) dialogView.findViewById(R.id.fabricollection);
        dialogBuilder.setTitle("Add a Fabric Collection name");
        dialogBuilder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isEmpty(email.getText().toString().trim())){
                            email.setError("Please provide a name");
                            return;
                        }
                        addFabricCollection(email.getText().toString());
                        b.dismiss();
                    }
                });
            }
        });
        b.show();

    }

    public void addFabricCollection(String name){
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                dialog.setMessage("Creating...Please wait a moment");
                dialog.show();
            }
        },100);
        RequestBody title =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), name);
        Call<CreateStyleResponse> call = mService.createFabricCollection(title);
        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    dialog.dismiss();
                    Toast.makeText(FabricCollectionsActivity.this,"Successfully added",Toast.LENGTH_SHORT).show();
                    loadFabricCollections();
                }else{
                    dialog.dismiss();
                    Toast.makeText(FabricCollectionsActivity.this,"Server error...Please try again",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(FabricCollectionsActivity.this,"No internet connection...Please try again",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
