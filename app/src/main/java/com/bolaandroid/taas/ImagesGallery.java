package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bolaandroid.adapter.FullScreenImageAdapter;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ImagesGallery extends AppCompatActivity {
    ViewPager mViewPager;
    FullScreenImageAdapter mPagerAdapter;
    ArrayList<String> images_url;
    FloatingActionButton shareButton;
    Intent shareIntent;
    String message;
    Button btnClose;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_gallery);
        images_url = getIntent().getStringArrayListExtra("images");
        mPagerAdapter= new FullScreenImageAdapter(this,images_url);
        mViewPager=(ViewPager) findViewById(R.id.pager);
        btnClose = (Button) findViewById(R.id.btnClose);
        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mViewPager.setAdapter(mPagerAdapter);
        dialog= new ProgressDialog(this);

        String styleCode=getIntent().getStringExtra("style code");
        shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        message = "You Love? Custom order this attire on The African Attire Shop(TAAS) App using the code "+styleCode+" to get started or click " + getURL(styleCode);
        shareIntent.putExtra(Intent.EXTRA_TEXT,message);
        shareButton = (FloatingActionButton) findViewById(R.id.shareFabBtn);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    shareItem(images_url.get(0));

            }
        });
        if(styleCode.equals("No code for fabrics and accessories")){
            shareButton.setVisibility(View.GONE);
        }
    }

    public String getURL(String code){
        return "http://www.taasafrica.com/code/"+code;
    }

    public void shareItem(final String url) {
        final Context context=shareButton.getContext();
        Picasso.with(getApplicationContext()).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                dialog.dismiss();
                ClipboardManager cm = (ClipboardManager) ImagesGallery.this.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(message, message);
                cm.setPrimaryClip(clip);
                Toast.makeText(ImagesGallery.this, "Share text copied to clipboard", Toast.LENGTH_SHORT).show();
                shareIntent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                startActivity(Intent.createChooser(shareIntent, "Share product via"));
            }
            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                },100);
                Toast.makeText(context,"Unable to fetch image,please try again",Toast.LENGTH_LONG).show();
            }
            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        dialog.setMessage("Preparing to share...Please wait");
                        dialog.setIndeterminate(false);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    }
                },100);
            }
        });
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".my.package.name.provider", file);
            //bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

}
