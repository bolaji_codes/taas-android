package com.bolaandroid.taas;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bolaandroid.adapter.DesignerOrdersAdapter;
import com.bolaandroid.adapter.OrdersAdapter;
import com.bolaandroid.data.model.DesignerOrders;
import com.bolaandroid.data.model.Order;
import com.bolaandroid.data.model.remote.TAASService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class AllOrdersActivity extends AppCompatActivity {
    TAASService mService;
    RecyclerView recyclerView;
    DesignerOrdersAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    Toolbar toolbar;
    TextView noListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_orders);
        mService= ApiUtils.getTAASService();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        noListView = (TextView) findViewById(R.id.no_list_text);
        loadAllOrders();
    }

    public void loadAllOrders(){
        swipeRefreshLayout.setRefreshing(true);
        Call<ArrayList<DesignerOrders>> call = mService.getDesignerOrders();
        call.enqueue(new Callback<ArrayList<DesignerOrders>>() {
            @Override
            public void onResponse(Call<ArrayList<DesignerOrders>> call, Response<ArrayList<DesignerOrders>> response) {
                if (response.isSuccessful()){
                    loadlayout(response.body());
                    swipeRefreshLayout.setRefreshing(false);
                    if(response.body().isEmpty()){
                        swipeRefreshLayout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }else{
                    swipeRefreshLayout.setRefreshing(false);
                    final Snackbar snackbar= Snackbar.make(recyclerView,"Server error", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY",new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {


                            loadAllOrders();
                        }
                    });
                    snackbar.show();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<DesignerOrders>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                final Snackbar snackbar= Snackbar.make(recyclerView,"No internet connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY",new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {


                        loadAllOrders();
                    }
                });
                snackbar.show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadlayout(ArrayList<DesignerOrders> orders){
        adapter = new DesignerOrdersAdapter(orders,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
}
