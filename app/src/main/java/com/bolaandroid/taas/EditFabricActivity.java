package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.FabricCollection;
import com.bolaandroid.data.model.remote.TAASService;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.ProgressRequestBody;

import static android.text.TextUtils.isEmpty;
import static com.bolaandroid.taas.AddNewStyles.verifyStoragePermissions;

public class EditFabricActivity extends AppCompatActivity implements View.OnClickListener{
    private final int SELECT_PHOTO = 1;
    EditText titleEdit,priceEdit,brandEdit;
    ImageView img1;
    Spinner fabricCollection;
    TAASService mService;
    ProgressDialog dialog;
    Button button;
    ProgressBar progressBar;
    String fabricCollectionName;
    Toolbar toolbar;
    NumberPicker numberPicker;
    CustomFabrics fabric;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_fabric);
        mService = ApiUtils.getTAASService();
        dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Fabric");
        titleEdit = (EditText) findViewById(R.id.name);
        brandEdit = (EditText) findViewById(R.id.brand_name);
        img1 = (ImageView) findViewById(R.id.image1);
        priceEdit = (EditText) findViewById(R.id.price);
        button = (Button) findViewById(R.id.button);
        numberPicker = (NumberPicker) findViewById(R.id.number_picker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(100);
        numberPicker.setWrapSelectorWheel(true);
        fabricCollection = (Spinner) findViewById(R.id.fabric_spinner);
        fabric = (CustomFabrics) getIntent().getSerializableExtra("fabric");
        Picasso.with(this)
                .load(fabric.getPhoto1())
                .resize(700,700)
                .centerInside()
                .placeholder(getRandomColor())
                .into(img1);
        titleEdit.setText(fabric.getName());
        priceEdit.setText(String.valueOf(fabric.getPrice()));
        if(fabric.getStock()!=null){
            numberPicker.setValue(fabric.getStock());
        }else {
            numberPicker.setValue(100);
        }

        try{
            brandEdit.setText(fabric.getBrand());
        }catch (Exception e){}

        button.setOnClickListener(this);
        loadFabricCollection();

    }

    public void loadFabricCollection(){
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<FabricCollection>> call = mService.getDesignerFabricCollections();
        call.enqueue(new Callback<ArrayList<FabricCollection>>() {
            @Override
            public void onResponse(Call<ArrayList<FabricCollection>> call, Response<ArrayList<FabricCollection>> response) {
                if(response.isSuccessful()){
                    ArrayList<CharSequence> strings = new ArrayList<>();
                    ArrayList<FabricCollection> fabricCollections = response.body();
                    Iterator<FabricCollection> iterator= fabricCollections.iterator();
                    while (iterator.hasNext()){
                        FabricCollection collection=iterator.next();
                        if(collection.getNumberOfFabrics()==0){
                            iterator.remove();
                        }
                    }
                    if(fabricCollections.size()==0){
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(EditFabricActivity.this,R.style.AppCompatAlertDialogStyle);
                        builder.setTitle("Info")
                                .setMessage("You need to add fabric collection with at least a fabric in it before you can add a style or fabric")
                                .setCancelable(true)
                                .setNegativeButton(android.R.string.cancel, null)
                                .setPositiveButton(
                                        "ADD FABRIC COLLECTION",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialogInterface,
                                                    int i)
                                            {
                                                Intent intent = new Intent(EditFabricActivity.this,
                                                        FabricCollectionsActivity.class);
                                                EditFabricActivity.this.startActivity(intent);
                                            }
                                        })
                                .setIcon(android.R.drawable.ic_dialog_alert);
                        AlertDialog alert=builder.create();
                        alert.show();
                        Button nButton=alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                        nButton.setTextColor(Color.GRAY);
                    }
                    for(FabricCollection fabricCollection:fabricCollections){
                        strings.add(fabricCollection.getName());
                    }
                    ArrayAdapter<CharSequence> adapter =
                            new ArrayAdapter<CharSequence>(EditFabricActivity.this, android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fabricCollection.setAdapter(adapter);
                    if(fabric.getCollection()!=null){
                        fabricCollection.setSelection(adapter.getPosition(fabric.getCollection().getName()));
                    }
                    progressBar.setVisibility(View.GONE);
                }else{
                    loadFabricCollection();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FabricCollection>> call, Throwable t) {
                loadFabricCollection();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){

            case R.id.button:
                updateFabrictoServer();
                break;
            default:
                break;
        }
    }

    public boolean cleanForm(){
        boolean canSendData=true;
        if(isEmpty(titleEdit.getText().toString().trim())){
            titleEdit.requestFocus();
            titleEdit.setError("Please fill in the title");
            canSendData=false;
        }
        if(isEmpty(priceEdit.getText().toString().trim())){
            priceEdit.requestFocus();
            priceEdit.setError("Please fill in the price");
            canSendData=false;
        }

        String actualPostionOfSpinner = (String) fabricCollection.getItemAtPosition(fabricCollection.getSelectedItemPosition());
        try{
            if(actualPostionOfSpinner.isEmpty()){
                setSpinnerError(fabricCollection,"FabricCollection can't be empty");
                Toast.makeText(this,"You have to have at least a fabric collection before uploading a style or fabric",Toast.LENGTH_LONG).show();
                canSendData=false;
            }
        }catch (Exception e){
            if(actualPostionOfSpinner==null){
                setSpinnerError(fabricCollection,"Please wait...Let us retrieve your fabric collections first");
                Toast.makeText(this,"Please wait...Let us retrieve your fabric collections first",Toast.LENGTH_LONG).show();
                canSendData=false;
            }
        }

        if(img1.getDrawable().getConstantState()==getDrawable(R.mipmap.add_image_icon).getConstantState()){
            Toast.makeText(this,"You have to upload at least an image",Toast.LENGTH_SHORT).show();
            canSendData=false;
        }
        return canSendData;
    }


    private void setSpinnerError(Spinner spinner, String error){
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError(error); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
            spinner.performClick(); // to open the spinner list if error is found.

        }
    }

    public void updateFabrictoServer(){
        if(!cleanForm()){
            return;
        }

        if(!verifyStoragePermissions(this)){
            return;
        }

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                dialog.setMessage("Fabric updating...please wait");
                dialog.setIndeterminate(true);
                dialog.show();
            }
        },100);

        RequestBody fabricStock =RequestBody.create(
                MediaType.parse("multipart/form-data"), String.valueOf(numberPicker.getValue()));

        RequestBody title1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), titleEdit.getText().toString());
        RequestBody brand1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), brandEdit.getText().toString());
        RequestBody fabricCollection1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), fabricCollection.getSelectedItem().toString());

        RequestBody price1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), priceEdit.getText().toString());

        Call<CreateStyleResponse> call=mService.updateFabric(fabric.getPk(),title1,brand1,fabricCollection1,price1,fabricStock);
        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(EditFabricActivity.this, "Successfully uploaded", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    EditFabricActivity.this.finish();
                }else{
                    Toast.makeText(EditFabricActivity.this, "Internal Server error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                Toast.makeText(EditFabricActivity.this, "Unable to upload to server" , Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }
}
