package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.Profile;
import com.bolaandroid.data.model.UserProfile;
import com.bolaandroid.data.model.remote.TAASService;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

import static co.paystack.android.utils.StringUtils.isEmpty;

public class EditProfileActivity extends AppCompatActivity {
    Toolbar toolbar;
    TAASService mService;
    ProgressDialog dialog;
    Snackbar snackbar;
    TextView usernameText;
    ImageView profileImage;
    Button updateButton;
    Spinner countrySpinner;
    ArrayAdapter<CharSequence> adapter;
    EditText firstNameEdit, lastNameEdit, phoneEdit, emailEdit, addressEdit, cityEdit, stateEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mService = ApiUtils.getTAASService();
        toolbar.setTitleTextColor(0xFFFFFFFF);
        dialog = new ProgressDialog(this);
        profileImage = (ImageView) findViewById(R.id.img_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Profile");
        firstNameEdit = (EditText) findViewById(R.id.first_name);
        lastNameEdit = (EditText) findViewById(R.id.last_name);
        phoneEdit = (EditText) findViewById(R.id.phone);
        emailEdit = (EditText) findViewById(R.id.email);
        addressEdit = (EditText) findViewById(R.id.address);
        cityEdit = (EditText) findViewById(R.id.city);
        stateEdit = (EditText) findViewById(R.id.state);
        usernameText = (TextView) findViewById(R.id.username);
        countrySpinner = (Spinner) findViewById(R.id.countries_spinner);
        adapter=ArrayAdapter.createFromResource(this,R.array.country_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(adapter);
        emailEdit.setEnabled(false);
        getUserProfile();
        SharedPreferences prefs = this.getSharedPreferences("TAG", MODE_PRIVATE);
        String profileImageUri = prefs.getString("profile_uri", "null");
        if (!profileImageUri.equals("null")) {
            File file = new File(profileImageUri);
            if (file.exists()) {

                Glide.with(this.getApplicationContext()).load(file).into(profileImage);
                //Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                //imgProfile.setImageURI(Uri.fromFile(file));

            } else {
                profileImage.setImageResource(R.drawable.unknownprofile);
            }

        } else {
            profileImage.setImageResource(R.drawable.unknownprofile);

        }
        updateButton = (Button) findViewById(R.id.update_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cleanForm()){
                    return;
                }
                Profile profile = new Profile(firstNameEdit.getText().toString().trim(),
                        lastNameEdit.getText().toString().trim(),
                        usernameText.getText().toString().substring(1),
                        addressEdit.getText().toString().trim(),
                        cityEdit.getText().toString().trim(),
                        stateEdit.getText().toString().trim(),
                        countrySpinner.getSelectedItem().toString(),
                        phoneEdit.getText().toString().trim());
                updateUserProfile(profile);
            }
        });
    }

    public boolean cleanForm(){
        boolean cleanedForm;
        if(addressEdit.getText().toString().equals("")){
            addressEdit.setError("Empty field not allowed here");
            cleanedForm = false;
            return cleanedForm;
        }
        if(cityEdit.getText().toString().equals("")){
            cityEdit.setError("Empty field not allowed here");
            cleanedForm = false;
            return cleanedForm;
        }
        if(stateEdit.getText().toString().equals("")){
            stateEdit.setError("Empty field not allowed here");
            cleanedForm = false;
            return cleanedForm;
        }
        if(emailEdit.getText().toString().equals("")){
            emailEdit.setError("Empty field not allowed here");
            cleanedForm = false;
            return cleanedForm;
        }
        if(firstNameEdit.getText().toString().equals("")){
            firstNameEdit.setError("Empty field not allowed here");
            cleanedForm = false;
            return cleanedForm;
        }
        if(lastNameEdit.getText().toString().equals("")){
            lastNameEdit.setError("Empty field not allowed here");
            cleanedForm = false;
            return cleanedForm;
        }
        if(phoneEdit.getText().toString().equals("")){
            phoneEdit.setError("Empty field not allowed here");
            cleanedForm = false;
            return cleanedForm;
        }

        if(isEmpty(countrySpinner.getSelectedItem().toString().trim())){
            Toast.makeText(this,"Country field is compulsory",Toast.LENGTH_LONG).show();
            cleanedForm = false;
            return cleanedForm;
        }
        if(isEmpty(usernameText.getText().toString().trim())){
            Toast.makeText(this,"Username field is compulsory",Toast.LENGTH_LONG).show();
            cleanedForm = false;
            return cleanedForm;
        }
        return true;
    }

    public void updateUserProfile(Profile profile){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.setMessage("Updating your info...please wait a moment");
                dialog.setIndeterminate(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }, 100);
        Call<Profile> call = mService.updateUserProfile(profile);
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if(response.isSuccessful()){
                    Profile profile1 = response.body();
                    SharedPreferences.Editor editor = getSharedPreferences(
                            "TAG",MODE_PRIVATE
                    ).edit();
                    editor.putString("first_name",response.body().getFirstName());
                    editor.putString("last_name",response.body().getLastName());
                    editor.putString("country",response.body().getCountry());
                    editor.putString("city",response.body().getCity());
                    editor.putString("address",response.body().getAddress());
                    editor.putString("state",response.body().getState());
                    editor.apply();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 100);
                    Toast.makeText(EditProfileActivity.this,"Profile successfully updated",Toast.LENGTH_LONG).show();

                    EditProfileActivity.this.finish();
                }else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 100);
                    Toast.makeText(EditProfileActivity.this,"An error occured...Try again later",Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 100);
                Toast.makeText(EditProfileActivity.this,"No intenet connection...Try again later",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getUserProfile() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.setMessage("Fetching data...please wait a moment");
                dialog.setIndeterminate(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }, 100);
        Call<UserProfile> call = mService.getUserProfile();
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                if (response.isSuccessful()) {
                    UserProfile profile = response.body();
                    firstNameEdit.setText(profile.getUser().getFirstName());
                    lastNameEdit.setText(profile.getUser().getLastName());
                    emailEdit.setText(profile.getUser().getEmail());
                    addressEdit.setText(profile.getAddress());
                    cityEdit.setText(profile.getCity());
                    stateEdit.setText(profile.getState());
                    usernameText.setText("@" + profile.getUser().getUsername());
                    countrySpinner.setSelection(adapter.getPosition(profile.getCountry().getName()));

                    if(profile.getPhone()!=null){
                        phoneEdit.setText(profile.getPhone());
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 100);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    }, 100);
                    snackbar = Snackbar.make(addressEdit, "Error connecting to server", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getUserProfile();
                        }
                    });
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 100);
                snackbar = Snackbar.make(addressEdit, "No internet connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getUserProfile();
                    }
                });
                snackbar.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                return true;
        }
            return super.onOptionsItemSelected(item);

    }
}
