package com.bolaandroid.taas;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bolaandroid.adapter.DesignerFabricsAdapter;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.remote.TAASService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class FabricsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private TAASService mService;
    DesignerFabricsAdapter adapter;
    ProgressBar progressBar;
    ArrayList<CustomFabrics> fabrics=new ArrayList<>();
    String fabricCollectionName;
    Toolbar toolbar;
    TextView noListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fabrics);
        mService= ApiUtils.getTAASService();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Fabrics");
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        if(isForFabricCollection()){
            getSupportActionBar().setTitle(fabricCollectionName+" "+getString(R.string.fabric_collection));
        }
        noListView = (TextView) findViewById(R.id.no_list_text);
        getFabricsData();
    }



    public void getFabricsData(){
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<CustomFabrics>> call;
        if(isForFabricCollection()){
            call = mService.getFabricByDesignerCollection(fabricCollectionName);
        }else{
            call= mService.getDesignerFabrics();
        }
        call.enqueue(new Callback<ArrayList<CustomFabrics>>() {

            @Override
            public void onResponse(Call<ArrayList<CustomFabrics>> call, Response<ArrayList<CustomFabrics>> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    fabrics=response.body();
                    adapter = new DesignerFabricsAdapter(fabrics);
                    recyclerView.setLayoutManager(new GridLayoutManager(FabricsActivity.this, 4));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(adapter);
                    if(fabrics.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                        if(isForFabricCollection()){
                            noListView.setText("You haven't uploaded any fabric to " +fabricCollectionName +" fabric collection "+" yet.Tap 'ADD' to upload.");
                        }
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }else{
                    progressBar.setVisibility(View.GONE);
                    final Snackbar snackbar = Snackbar.make(recyclerView, "Error fetching fabrics!", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getFabricsData();
                        }
                    });
                    snackbar.show();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CustomFabrics>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                final Snackbar snackbar = Snackbar.make(recyclerView, "No internet  connection!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getFabricsData();
                    }
                });
                snackbar.show();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_list_dashboard,menu);

        return true;
    }


    public boolean isForFabricCollection(){
        boolean isForFabricCollection;
        Intent intent=this.getIntent();
        if(intent.hasExtra("fabricCollectionName")){
            fabricCollectionName=intent.getStringExtra("fabricCollectionName");
            isForFabricCollection = true;
        }else{
            isForFabricCollection = false;
        }
        return isForFabricCollection;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item:
                Intent intent = new Intent(FabricsActivity.this,
                       AddFabricsActivity.class);
                if(isForFabricCollection()){
                    intent.putExtra("fabricCollectionName",fabricCollectionName);
                }
                this.startActivity(intent);
                return true;
            case R.id.menu_refresh:
                getFabricsData();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(adapter!=null && adapter.dialog!=null){
            adapter.dialog.dismiss();
        }
        getFabricsData();


    }
}
