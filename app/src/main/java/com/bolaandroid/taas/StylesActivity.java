package com.bolaandroid.taas;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bolaandroid.adapter.DesignerStylesAdapterz;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.remote.TAASService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class StylesActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private TAASService mService;
    DesignerStylesAdapterz adapter;
    TextView noListView;
    ProgressBar progressBar;
    ArrayList<CustomStyles> styles=new ArrayList<>();
    private static final int MENU_SAVE = 1;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_styles);
        mService= ApiUtils.getTAASService();
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Styles");
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        noListView = (TextView) findViewById(R.id.no_list_text);

        getStylesData();
    }

    public void getStylesData(){
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<CustomStyles>> call= mService.getDesignerStyles();
        call.enqueue(new Callback<ArrayList<CustomStyles>>() {

            @Override
            public void onResponse(Call<ArrayList<CustomStyles>> call, Response<ArrayList<CustomStyles>> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    styles=response.body();
                    adapter = new DesignerStylesAdapterz(styles);
                    recyclerView.setLayoutManager(new GridLayoutManager(StylesActivity.this, 4));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(adapter);
                    if(styles.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }else{
                    progressBar.setVisibility(View.GONE);
                    final Snackbar snackbar = Snackbar.make(recyclerView, "Error fetching styles!", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getStylesData();
                        }
                    });
                    snackbar.show();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CustomStyles>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                final Snackbar snackbar = Snackbar.make(recyclerView, "No internet  connection!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getStylesData();
                    }
                });
                snackbar.show();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_dashboard,menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item:
                //do stuffs here
                Intent intent = new Intent(StylesActivity.this,
                    AddNewStyles.class);
                this.startActivity(intent);
                return true;
            case R.id.menu_refresh:
                getStylesData();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(adapter!=null && adapter.dialog!=null){
            adapter.dialog.dismiss();
        }

        getStylesData();


    }
}
