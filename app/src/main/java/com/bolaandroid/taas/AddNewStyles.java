package com.bolaandroid.taas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.FabricCollection;
import com.bolaandroid.data.model.remote.TAASService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.ProgressRequestBody;

import static android.text.TextUtils.isEmpty;

public class AddNewStyles extends AppCompatActivity implements View.OnClickListener,ProgressRequestBody.UploadCallbacks{
    private final int FIRST_PHOTO = 1;
    private final int SECOND_PHOTO = 2;
    EditText titleEdit,priceEdit,sameFabricEdit;
    ImageView img1,img2;
    Spinner gender,fabricCollection,category;
    TAASService mService;
    File file1,file2;
    Integer bigPercentage=0;
    ProgressDialog dialog;
    Button button;
    Long filesSize;
    Long mUploadedSize;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView numPickerQuestion;
    CheckBox sameFabricCheckBox;
    TextInputLayout sameFabricTextInputLayout;
    NumberPicker numberPicker;
    ArrayList<File> arrayListFiles=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_styles);
        mService = ApiUtils.getTAASService();
        dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Style");
        loadFabricCollection();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        titleEdit = (EditText) findViewById(R.id.name);
        img1 = (ImageView) findViewById(R.id.image1);
        img2 = (ImageView) findViewById(R.id.image2);
        priceEdit = (EditText) findViewById(R.id.edit_price);
        button = (Button) findViewById(R.id.button);
        numberPicker = (NumberPicker) findViewById(R.id.number_picker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(100);
        numberPicker.setValue(1);
        numberPicker.setWrapSelectorWheel(true);
        numPickerQuestion = (TextView) findViewById(R.id.num_text);
        mUploadedSize=Long.valueOf(0);
        sameFabricTextInputLayout = (TextInputLayout) findViewById(R.id.same_fabric_textinputlayout);
        fabricCollection = (Spinner) findViewById(R.id.fabric_spinner);
        sameFabricCheckBox = (CheckBox) findViewById(R.id.same_fabric_checkbox);
        sameFabricEdit = (EditText) findViewById(R.id.same_fabric_edittext);
        sameFabricCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sameFabricCheckBox.isChecked()){
                    sameFabricTextInputLayout.setVisibility(View.VISIBLE);
                    sameFabricEdit.setVisibility(View.VISIBLE);
                    numberPicker.setVisibility(View.VISIBLE);
                    numPickerQuestion.setVisibility(View.VISIBLE);
                }else {
                    sameFabricTextInputLayout.setVisibility(View.GONE);
                    numberPicker.setVisibility(View.GONE);
                    numPickerQuestion.setVisibility(View.GONE);
                    sameFabricEdit.setVisibility(View.GONE);
                }
            }
        });
        category = (Spinner) findViewById(R.id.category_spinner);
        gender = (Spinner) findViewById(R.id.gender_spinner);
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.gender_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);
        ArrayAdapter<CharSequence> adapter1=
                ArrayAdapter.createFromResource(this,R.array.category_array,
                        android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adapter1);
        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        button.setOnClickListener(this);
    }

    public File compressFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public void loadFabricCollection(){
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<FabricCollection>> call = mService.getDesignerFabricCollections();
        call.enqueue(new Callback<ArrayList<FabricCollection>>() {
            @Override
            public void onResponse(Call<ArrayList<FabricCollection>> call, Response<ArrayList<FabricCollection>> response) {
                if(response.isSuccessful()){
                    ArrayList<CharSequence> strings = new ArrayList<>();
                    ArrayList<FabricCollection> fabricCollections = response.body();
                    Iterator<FabricCollection> iterator= fabricCollections.iterator();
                    while (iterator.hasNext()){
                        FabricCollection collection=iterator.next();
                        if(collection.getNumberOfFabrics()==0){
                            iterator.remove();
                        }
                    }

                    if(fabricCollections.size()==0){
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(AddNewStyles.this,R.style.AppCompatAlertDialogStyle);
                        builder.setTitle("Info")
                                .setMessage("Unless you want your customers to only order this style using the same fabric as used in style picture,you will need to add fabric collection so you can put fabrics in it to give your customers more fabric options")
                                .setCancelable(true)
                                .setNegativeButton(android.R.string.cancel, null)
                                .setPositiveButton(
                                        "ADD FABRIC COLLECTION",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialogInterface,
                                                    int i)
                                            {
                                                Intent intent = new Intent(AddNewStyles.this,
                                                        FabricCollectionsActivity.class);
                                                AddNewStyles.this.startActivity(intent);
                                            }
                                        })
                                .setIcon(android.R.drawable.ic_dialog_alert);
                        AlertDialog alert=builder.create();
                        alert.show();
                        Button nButton=alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                        nButton.setTextColor(Color.GRAY);
                    }
                    for(FabricCollection fabricCollection:fabricCollections){
                        strings.add(fabricCollection.getName());
                    }
                    strings.add("Don't use any fabric collection");
                    ArrayAdapter<CharSequence> adapter =
                            new ArrayAdapter<CharSequence>(AddNewStyles.this, android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    fabricCollection.setAdapter(adapter);

                    progressBar.setVisibility(View.GONE);
                }else{
                    loadFabricCollection();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FabricCollection>> call, Throwable t) {
                final Snackbar snackbar = Snackbar.make(fabricCollection, "Error fetching your fabric collections!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadFabricCollection();
                    }
                });
                snackbar.show();

            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case FIRST_PHOTO:
                if(resultCode == RESULT_OK){
                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        String realPathfromURI=getRealPathFromURI(imageUri);
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        img1.setImageBitmap(selectedImage);
                        if(img1.getDrawable().getConstantState() != getResources().getDrawable(R.mipmap.add_image_icon).getConstantState()){
                            arrayListFiles.remove(file1);
                        }
                        file1=new File(realPathfromURI);
                        if(file1.length()/1024 > 300){
                            file1=compressFile(file1);
                        }
                        arrayListFiles.add(file1);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
                break;

            case SECOND_PHOTO:
                if(resultCode == RESULT_OK){
                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        String realPathfromURI=getRealPathFromURI(imageUri);
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        if(img2.getDrawable().getConstantState() != getResources().getDrawable(R.mipmap.add_image_icon).getConstantState()){
                            arrayListFiles.remove(file2);
                        }
                        img2.setImageBitmap(selectedImage);
                        file2=new File(realPathfromURI);
                        if(file2.length()/1024 > 300){
                            file2=compressFile(file2);
                        }
                        arrayListFiles.add(file2);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }

                break;
        }
    }

    public Long getArrayFileSize(){
        filesSize=Long.valueOf(0);
        for(File file:arrayListFiles){
            filesSize+=file.length();
        }
        return filesSize;
    }

    public boolean cleanForm(){
        boolean canSendData=true;
       if(isEmpty(titleEdit.getText().toString().trim())){
           titleEdit.requestFocus();
           titleEdit.setError("Please fill in the title");
           canSendData=false;
       }
       if(sameFabricCheckBox.isChecked()==false && fabricCollection.getSelectedItem().toString().equals("Don't use any fabric collection")){
           Toast.makeText(this,"You have to at least choose a fabric collection or use same fabric as in style",Toast.LENGTH_LONG).show();
           canSendData=false;
           return canSendData;
       }
        if(isEmpty(priceEdit.getText().toString().trim())){
            priceEdit.requestFocus();
            priceEdit.setError("Please fill in the price");
            canSendData=false;
        }
        String actualPostionOfSpinner = (String) fabricCollection.getItemAtPosition(fabricCollection.getSelectedItemPosition());

        try{
            if(actualPostionOfSpinner.isEmpty()){
                setSpinnerError(fabricCollection,"FabricCollection can't be empty");
                Toast.makeText(this,"You have to have at least a fabric collection before uploading a style or fabric",Toast.LENGTH_LONG).show();
                canSendData=false;
            }
        }catch (Exception e){
            if(actualPostionOfSpinner==null){
                setSpinnerError(fabricCollection,"Please wait...Let us retrieve your fabric collections first");
                Toast.makeText(this,"Please wait...Let us retrieve your fabric collections first",Toast.LENGTH_LONG).show();
                canSendData=false;
            }
        }


        String actualPostionOfGenderSpinner = (String) gender.getItemAtPosition(gender.getSelectedItemPosition());
        if(actualPostionOfGenderSpinner.isEmpty()){
            setSpinnerError(gender,"Gender cant be empty");
            canSendData=false;
        }
        String actualPostionOfCatgorySpinner = (String) category.getItemAtPosition(category.getSelectedItemPosition());
        if(actualPostionOfCatgorySpinner.isEmpty()){
            setSpinnerError(category,"Category cant be empty");
            canSendData=false;
        }
        if(img1.getDrawable().getConstantState()==getDrawable(R.mipmap.add_image_icon).getConstantState()){
            Toast.makeText(this,"You have to upload at least an image",Toast.LENGTH_SHORT).show();
            canSendData=false;
        }
        return canSendData;
    }

    private void setSpinnerError(Spinner spinner, String error){
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError(error); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
            spinner.performClick(); // to open the spinner list if error is found.

        }
    }

    public void addStyletoServer(){
        if(!cleanForm()){
            return;
        }

        if(!verifyStoragePermissions(this)){
            return;
        }

        getArrayFileSize();



        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                dialog.setMessage(bigPercentage+"%"+ " uploaded");
                dialog.setIndeterminate(true);
                dialog.setProgress(bigPercentage);
                dialog.show();
            }
        },100);


        MultipartBody.Part filePart,filePart1;
        filePart=null;
        filePart1=null;
        if(img1.getDrawable().getConstantState()!=getDrawable(R.mipmap.add_image_icon).getConstantState()){
            ProgressRequestBody fileBody = new ProgressRequestBody(file1, this);
            filePart = MultipartBody.Part.createFormData("photo1", file1.getName(), fileBody);

        }

        if(img2.getDrawable().getConstantState()!=getDrawable(R.mipmap.add_image_icon).getConstantState()){
            ProgressRequestBody fileBody1 = new ProgressRequestBody(file2, this);
            filePart1 = MultipartBody.Part.createFormData("photo2", file2.getName(), fileBody1);
        }

        if(filePart==null && filePart1!=null){
            //swap images if first image is empty
            filePart = filePart1;
            filePart1 = null;
        }


        RequestBody title1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), titleEdit.getText().toString());
        RequestBody fabricCollection1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), fabricCollection.getSelectedItem().toString());
        RequestBody category1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), category.getSelectedItem().toString());

        String genderAbbrev;
        if(gender.getSelectedItem().toString().equals("Male")){
            genderAbbrev="M";
        }else {
            genderAbbrev = "F";
        }
        RequestBody gender1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), genderAbbrev);
        RequestBody price1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), priceEdit.getText().toString());


        Call<CreateStyleResponse> call;
        Boolean sameFabric = false;
        if(sameFabricCheckBox.isChecked()){
            sameFabric = true;
            RequestBody sameFabricPrice =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), sameFabricEdit.getText().toString());
            RequestBody fabricStock =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), String.valueOf(numberPicker.getValue()));
            call=mService.createStyle(title1,
                    fabricCollection1,
                    category1,
                    gender1,
                    price1,
                    filePart,
                    filePart1,
                    sameFabric,
                    sameFabricPrice,
                    fabricStock

            );
        }else{
            call=mService.createStyle(title1,
                    fabricCollection1,
                    category1,
                    gender1,
                    price1,
                    filePart,
                    filePart1,
                    sameFabric,
                    null,
                    null

            );
        }


        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(AddNewStyles.this, "Successfully uploaded", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    AddNewStyles.this.finish();
                }else{
                    Toast.makeText(AddNewStyles.this, "Internal Server error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                Toast.makeText(AddNewStyles.this, t.getMessage() , Toast.LENGTH_SHORT).show();

                Toast.makeText(AddNewStyles.this, "Unable to upload to server" , Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

    }



    @Override
    public void onProgressUpdate(long uploadedSize,File file) {
        if(file.getPath().equals(file1.getPath())){
            bigPercentage =(int)(100 * mUploadedSize / filesSize);
            mUploadedSize=uploadedSize;
        }else{
            Long totalSizeUploaded=mUploadedSize+uploadedSize;
            bigPercentage = (int)(100 * totalSizeUploaded / filesSize);
        }
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                dialog.setMessage(bigPercentage+"%"+ " uploaded" +".Please wait a sec");
            }
        },100);
    }

    @Override
    public void onError() {
        // do something on error
        Toast.makeText(AddNewStyles.this, "ERROR", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFinish() {
        // do something on upload finished
        // for example start next uploading at queue
        Toast.makeText(AddNewStyles.this, "Finished...Glad", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.image1:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.putExtra("image1",FIRST_PHOTO);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, FIRST_PHOTO);
                break;
            case R.id.image2:
                Intent photoPickerIntent2 = new Intent(Intent.ACTION_PICK);
                photoPickerIntent2.putExtra("image1",SECOND_PHOTO);
                photoPickerIntent2.setType("image/*");
                startActivityForResult(photoPickerIntent2, SECOND_PHOTO);
                break;
            case R.id.button:
                addStyletoServer();
            default:

                break;

        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (Build.VERSION.SDK_INT >= 19) {
            String[] proj = { MediaStore.Images.Media.DATA };
            CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();
            return result;
        }
        if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 18) {
            String[] proj = { MediaStore.Images.Media.DATA };
            String result = null;
            android.content.CursorLoader cursorLoader = new android.content.CursorLoader(
                    this,
                    contentUri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            if(cursor != null){
                int column_index =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        }

        if (Build.VERSION.SDK_INT < 11) {
            String[] proj = { MediaStore.Images.Media.DATA };
            Cursor cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index
                    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    //persmission method.
    public static boolean verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            return false;
        }

        return true;

    }
}
