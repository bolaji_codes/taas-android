package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.Designer;
import com.bolaandroid.data.model.ExtraStyleData;
import com.bolaandroid.data.model.FabricCollection;
import com.bolaandroid.data.model.remote.TAASService;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.ProgressRequestBody;

import static android.text.TextUtils.isEmpty;

public class EditStyleActivity extends AppCompatActivity implements View.OnClickListener {

    private final int FIRST_PHOTO = 1;
    private final int SECOND_PHOTO = 2;
    EditText titleEdit,priceEdit,sameFabricEdit;
    ImageView img1,img2;
    Spinner fabricCollection,category;
    TAASService mService;
    ProgressDialog dialog;
    Button button;
    Long filesSize;
    Long mUploadedSize;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView numPickerQuestion;
    CheckBox sameFabricCheckBox;
    TextInputLayout sameFabricTextInputLayout;
    NumberPicker numberPicker;
    CustomStyles style;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_style);
        mService = ApiUtils.getTAASService();
        dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        style = (CustomStyles) getIntent().getSerializableExtra("style");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Style");
        loadFabricCollection();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        titleEdit = (EditText) findViewById(R.id.name);
        img1 = (ImageView) findViewById(R.id.image1);
        img2 = (ImageView) findViewById(R.id.image2);
        priceEdit = (EditText) findViewById(R.id.edit_price3);
        button = (Button) findViewById(R.id.button);
        numberPicker = (NumberPicker) findViewById(R.id.number_picker);
        numberPicker.setWrapSelectorWheel(true);
        numPickerQuestion = (TextView) findViewById(R.id.num_text);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(100);
        numberPicker.setValue(0);
        mUploadedSize=Long.valueOf(0);
        sameFabricTextInputLayout = (TextInputLayout) findViewById(R.id.same_fabric_textinputlayout);
        fabricCollection = (Spinner) findViewById(R.id.fabric_spinner);
        sameFabricCheckBox = (CheckBox) findViewById(R.id.same_fabric_checkbox);
        sameFabricEdit = (EditText) findViewById(R.id.same_fabric_edittext);
        category = (Spinner) findViewById(R.id.category_spinner);

        ArrayAdapter<CharSequence> adapter1=
                ArrayAdapter.createFromResource(this,R.array.category_array,
                        android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adapter1);
        button.setOnClickListener(this);
        titleEdit.setText(style.getName());
        priceEdit.setText(String.valueOf(style.getCustomPrice()));
        category.setSelection(adapter1.getPosition(style.getCategory().getName()));
        Picasso.with(this)
                .load(style.getPhoto1())
                .resize(700,700)
                .centerInside()
                .placeholder(getRandomColor())
                .into(img1);
        if(style.getPhoto2()!=null){
            try{
                Picasso.with(this)
                        .load(style.getPhoto2())
                        .resize(700,700)
                        .centerInside()
                        .placeholder(getRandomColor())
                        .into(img2);
            }catch (Exception e){
            }
        }else{
            img2.setVisibility(View.GONE);
        }
        getExtraStyleData(style.getPk());

        sameFabricCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sameFabricCheckBox.isChecked()){
                    sameFabricTextInputLayout.setVisibility(View.VISIBLE);
                    sameFabricEdit.setVisibility(View.VISIBLE);
                    numberPicker.setVisibility(View.VISIBLE);
                    numPickerQuestion.setVisibility(View.VISIBLE);
                }else {
                    sameFabricTextInputLayout.setVisibility(View.GONE);
                    numberPicker.setVisibility(View.GONE);
                    numPickerQuestion.setVisibility(View.GONE);
                    sameFabricEdit.setVisibility(View.GONE);
                }
            }
        });



    }

    public void loadFabricCollection(){
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<FabricCollection>> call = mService.getDesignerFabricCollections();
        call.enqueue(new Callback<ArrayList<FabricCollection>>() {
            @Override
            public void onResponse(Call<ArrayList<FabricCollection>> call, Response<ArrayList<FabricCollection>> response) {
                if(response.isSuccessful()){
                    ArrayList<CharSequence> strings = new ArrayList<>();
                    ArrayAdapter<CharSequence> adapter;
                    ArrayList<FabricCollection> fabricCollections = response.body();
                    Iterator<FabricCollection> iterator= fabricCollections.iterator();
                    while (iterator.hasNext()){
                        FabricCollection collection=iterator.next();
                        if(collection.getNumberOfFabrics()==0){
                            iterator.remove();
                        }
                    }

                    if(fabricCollections.size()==0){
                        AlertDialog.Builder builder;
                        builder = new AlertDialog.Builder(EditStyleActivity.this,R.style.AppCompatAlertDialogStyle);
                        builder.setTitle("Info")
                                .setMessage("You need to add fabric collection with at least a fabric in it before you can add a style or fabric")
                                .setCancelable(true)
                                .setNegativeButton(android.R.string.cancel, null)
                                .setPositiveButton(
                                        "ADD FABRIC COLLECTION",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialogInterface,
                                                    int i)
                                            {
                                                Intent intent = new Intent(EditStyleActivity.this,
                                                        FabricCollectionsActivity.class);
                                                EditStyleActivity.this.startActivity(intent);
                                            }
                                        })
                                .setIcon(android.R.drawable.ic_dialog_alert);
                        AlertDialog alert=builder.create();
                        alert.show();
                        Button nButton=alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                        nButton.setTextColor(Color.GRAY);
                    }

                    strings.add("Don't use any fabric collection");
                    for(FabricCollection fabricCollection:fabricCollections){

                        strings.add(fabricCollection.getName());
                    }
                    adapter =
                            new ArrayAdapter<CharSequence>(EditStyleActivity.this, android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    fabricCollection.setAdapter(adapter);
                    if(style.getFabricCollection()==null){
                        fabricCollection.setSelection(adapter.getPosition("Don't use any fabric collection"));
                    }else{
                        fabricCollection.setSelection(adapter.getPosition(style.getFabricCollection().getName()));
                    }
                    progressBar.setVisibility(View.GONE);
                }else{
                    loadFabricCollection();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FabricCollection>> call, Throwable t) {
                final Snackbar snackbar = Snackbar.make(fabricCollection, "Error fetching your fabric collections!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadFabricCollection();
                    }
                });
                snackbar.show();

            }
        });
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.image1:
                Toast.makeText(EditStyleActivity.this,"You can't change this picture",Toast.LENGTH_SHORT).show();
                break;
            case R.id.image2:
                Toast.makeText(EditStyleActivity.this,"You can't change this picture",Toast.LENGTH_SHORT).show();
                break;
            case R.id.button:
                updateStyletoServer();
            default:

                break;

        }
    }

    public void getExtraStyleData(final int pk){
        Call<ExtraStyleData> call=mService.getExtraData(pk);
        call.enqueue(new Callback<ExtraStyleData>() {
            @Override
            public void onResponse(Call<ExtraStyleData> call, Response<ExtraStyleData> response) {
                if(response.isSuccessful()){

                    if(response.body().getStock()!=null){
                        numberPicker.setValue(response.body().getStock());
                    }else{
                        numberPicker.setValue(5);
                    }
                    if(response.body().getPrice()!=null){
                        sameFabricEdit.setText(String.valueOf(response.body().getPrice()));
                    }
                    if(response.body().getCanUseSameFabric()){
                        sameFabricCheckBox.setChecked(true);
                        numberPicker.setVisibility(View.VISIBLE);
                        numPickerQuestion.setVisibility(View.VISIBLE);
                        sameFabricTextInputLayout.setVisibility(View.VISIBLE);
                        sameFabricEdit.setVisibility(View.VISIBLE);
                    }else{
                        sameFabricCheckBox.setChecked(false);
                    }
                }else {
                    getExtraStyleData(pk);
                }
            }

            @Override
            public void onFailure(Call<ExtraStyleData> call, Throwable t) {

            }
        });
    }


    public void updateStyletoServer(){
        if(!cleanForm()){
            return;
        }


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                dialog.setMessage("Style updating....Please wait");
                dialog.setIndeterminate(true);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        },100);



        RequestBody title1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), titleEdit.getText().toString());
        RequestBody fabricCollection1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), fabricCollection.getSelectedItem().toString());
        RequestBody category1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), category.getSelectedItem().toString());



        RequestBody price1 =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), priceEdit.getText().toString());


        Call<CreateStyleResponse> call;
        Boolean sameFabric = false;
        if(sameFabricCheckBox.isChecked()){
            sameFabric = true;
            RequestBody sameFabricPrice =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), sameFabricEdit.getText().toString());
            RequestBody fabricStock =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), String.valueOf(numberPicker.getValue()));
            call=mService.updateStyle(style.getPk(),title1,
                    fabricCollection1,
                    category1,
                    price1,
                    sameFabric,
                    sameFabricPrice,
                    fabricStock

            );
        }else{
            call=mService.updateStyle(style.getPk(),title1,
                    fabricCollection1,
                    category1,
                    price1,
                    sameFabric,
                    null,
                    null

            );
        }


        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(EditStyleActivity.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    EditStyleActivity.this.finish();
                }else{
                    Toast.makeText(EditStyleActivity.this, "Internal Server error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                Toast.makeText(EditStyleActivity.this, t.getMessage() , Toast.LENGTH_SHORT).show();

                Toast.makeText(EditStyleActivity.this, "Unable to upload to server" , Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }

    public boolean cleanForm(){
        boolean canSendData=true;
        if(isEmpty(titleEdit.getText().toString().trim())){
            titleEdit.requestFocus();
            titleEdit.setError("Please fill in the title");
            canSendData=false;
        }
        if(isEmpty(priceEdit.getText().toString().trim())){
            priceEdit.requestFocus();
            priceEdit.setError("Please fill in the price");
            canSendData=false;
        }
        String actualPostionOfSpinner = (String) fabricCollection.getItemAtPosition(fabricCollection.getSelectedItemPosition());

        try{
            if(actualPostionOfSpinner.isEmpty()){
                setSpinnerError(fabricCollection,"FabricCollection can't be empty");
                Toast.makeText(this,"You have to have at least a fabric collection before uploading a style or fabric",Toast.LENGTH_LONG).show();
                canSendData=false;
            }
        }catch (Exception e){
            if(actualPostionOfSpinner==null){
                setSpinnerError(fabricCollection,"Please wait...Let us retrieve your fabric collections first");
                Toast.makeText(this,"Please wait...Let us retrieve your fabric collections first",Toast.LENGTH_LONG).show();
                canSendData=false;
            }
        }



        String actualPostionOfCatgorySpinner = (String) category.getItemAtPosition(category.getSelectedItemPosition());
        if(actualPostionOfCatgorySpinner.isEmpty()){
            setSpinnerError(category,"Category cant be empty");
            canSendData=false;
        }
        if(img1.getDrawable().getConstantState()==getDrawable(R.mipmap.add_image_icon).getConstantState()){
            Toast.makeText(this,"You have to upload at least an image",Toast.LENGTH_SHORT).show();
            canSendData=false;
        }
        return canSendData;
    }

    private void setSpinnerError(Spinner spinner, String error){
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError(error); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
            spinner.performClick(); // to open the spinner list if error is found.

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
