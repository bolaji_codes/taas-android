package com.bolaandroid.taas;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.WardrobeListAdapter;
import com.bolaandroid.data.model.Wardrobe;
import com.bolaandroid.interfaces.OnDeleteWardrobe;
import com.bolaandroid.interfaces.ShowRegisterDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.paperdb.Paper;

public class WardrobeActivity extends AppCompatActivity implements OnDeleteWardrobe {
    ArrayList<Wardrobe> wardrobes = new ArrayList<>();
    RecyclerView recyclerView;
    TextView noListView;
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wardrobe);
        Paper.init(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.wardrobe_recycler);
        List<String> allKeys = Paper.book("wardrobe").getAllKeys();
        noListView = (TextView) findViewById(R.id.no_list_text);
        i=0;
        for(String key:allKeys){
            i++;
            Wardrobe wardrobe=Paper.book("wardrobe").read(key,new Wardrobe(null,null,null,null,null,null,null,null,null));
            wardrobes.add(wardrobe);

        }
        Collections.reverse(wardrobes);
        getSupportActionBar().setTitle("My Wardrobe"+"("+String.valueOf(i)+")");
        WardrobeListAdapter adapter = new WardrobeListAdapter(wardrobes);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        if(wardrobes.isEmpty()){
            recyclerView.setVisibility(View.GONE);
            noListView.setVisibility(View.VISIBLE);
        }else {
            noListView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDeleteWardrobe(){
        i=i-1;
        getSupportActionBar().setTitle("My Wardrobe"+"("+String.valueOf(i)+")");
    }

}
