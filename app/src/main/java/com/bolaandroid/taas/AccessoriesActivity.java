package com.bolaandroid.taas;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bolaandroid.adapter.DesignerAccessoriesAdapter;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.remote.TAASService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class AccessoriesActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private TAASService mService;
    DesignerAccessoriesAdapter adapter;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView noListView;
    ArrayList<CustomAccessories> accessories=new ArrayList<>();
    private static final int MENU_SAVE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accessories);
        mService= ApiUtils.getTAASService();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        noListView = (TextView) findViewById(R.id.no_list_text);
        getAccessoriesData();
    }

    public void getAccessoriesData(){
        progressBar.setVisibility(View.VISIBLE);
        Call<ArrayList<CustomAccessories>> call= mService.getDesignerAccessories();
        call.enqueue(new Callback<ArrayList<CustomAccessories>>() {

            @Override
            public void onResponse(Call<ArrayList<CustomAccessories>> call, Response<ArrayList<CustomAccessories>> response) {
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.GONE);
                    accessories=response.body();
                    adapter = new DesignerAccessoriesAdapter(accessories);
                    recyclerView.setLayoutManager(new GridLayoutManager(AccessoriesActivity.this, 4));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(adapter);
                    if(accessories.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }else{
                    progressBar.setVisibility(View.GONE);
                    final Snackbar snackbar = Snackbar.make(recyclerView, "Error fetching accessories!", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getAccessoriesData();
                        }
                    });
                    snackbar.show();
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CustomAccessories>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                final Snackbar snackbar = Snackbar.make(recyclerView, "No internet  connection!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getAccessoriesData();
                    }
                });
                snackbar.show();

            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list_dashboard,menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item:
                Intent intent = new Intent(AccessoriesActivity.this,
                      AddAccessoriesActivity.class);
                this.startActivity(intent);
                return true;
            case R.id.menu_refresh:
                getAccessoriesData();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(adapter!=null && adapter.dialog!=null){
            adapter.dialog.dismiss();
        }
        getAccessoriesData();


    }
}
