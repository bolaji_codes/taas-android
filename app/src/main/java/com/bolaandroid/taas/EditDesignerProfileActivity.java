package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.Designer;
import com.bolaandroid.data.model.Profile;
import com.bolaandroid.data.model.remote.TAASService;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.ProgressRequestBody;

import static co.paystack.android.utils.StringUtils.isEmpty;

public class EditDesignerProfileActivity extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks {
    private final int DP = 1;
    private final int COVER_PHOTO = 2;
    Toolbar toolbar;
    EditText name,description,deliveryStatement,address,city,state;
    ImageView dp,coverPhoto;
    Designer designer;
    Spinner countrySpinner;
    ProgressDialog dialog;
    Button updateButton;
    TAASService mService;
    CardView dpCardView,coverPhotoCardView;
    File file,file1;
    Switch holidaySwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_designer_profile);
        designer=(Designer) getIntent().getSerializableExtra("designer");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Designer Profile");
        dialog=new ProgressDialog(this);
        mService=ApiUtils.getTAASService();
        dp=(ImageView) findViewById(R.id.img_profile);
        coverPhoto=(ImageView) findViewById(R.id.cover_photo);
        name = (EditText) findViewById(R.id.name);
        description = (EditText) findViewById(R.id.description);
        deliveryStatement =(EditText) findViewById(R.id.delivery_statement);
        address = (EditText) findViewById(R.id.address);
        city = (EditText) findViewById(R.id.city);
        state = (EditText) findViewById(R.id.state);
        countrySpinner = (Spinner) findViewById(R.id.countries_spinner);
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.country_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(adapter);
        name.setText(designer.getName());
        description.setText(designer.getBio());
        deliveryStatement.setText(designer.getDeliveryStatement());
        address.setText(designer.getAddress());
        city.setText(designer.getCity());
        state.setText(designer.getState());
        if(designer.getDp()!=null){
            Picasso.with(this).load(designer.getDp()).into(dp);
        }else{
            dp.setImageResource(R.drawable.unknownprofile);
        }
        if(designer.getCoverPhoto()!=null){
            Picasso.with(this).load(designer.getCoverPhoto()).into(coverPhoto);
        }
        countrySpinner.setSelection(adapter.getPosition(designer.getCountry().getName()));

        updateButton = (Button) findViewById(R.id.update_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cleanForm()){
                    return;
                }

                updateDesignerProfile();
            }
        });
        holidaySwitch=(Switch) findViewById(R.id.holiday_mode_switch);
        if(designer.getHolidayMode()){
            holidaySwitch.setChecked(true);
        }
        dpCardView=(CardView) findViewById(R.id.view2);
        dpCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.putExtra("image1",1);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, DP);
            }
        });
        coverPhotoCardView=(CardView) findViewById(R.id.view3);
        coverPhotoCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.putExtra("image1",1);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, COVER_PHOTO);
            }
        });

    }

    public void updateDesignerProfile(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.setMessage("Updating your info...please wait a moment");
                dialog.setIndeterminate(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }, 100);

        RequestBody nameBody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), name.getText().toString());
        RequestBody deliveryBody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), deliveryStatement.getText().toString());
        RequestBody descriptionBody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), description.getText().toString());
        RequestBody addressBody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), address.getText().toString());
        RequestBody cityBody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), city.getText().toString());
        RequestBody stateBody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), state.getText().toString());
        RequestBody countryBody =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), countrySpinner.getSelectedItem().toString());


        Boolean holidayMode=false;
        if (holidaySwitch.isChecked()){
            holidayMode=true;
        }
        MultipartBody.Part filePart,filePart1;
        Call<CreateStyleResponse> call;
        //file=convertImageViewToBitmapFile(dp);
        if(file==null){
            filePart=null;
        }else{
            ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
            filePart = MultipartBody.Part.createFormData("dp", file.getName(), fileBody);
        }
        if(file1==null){
            filePart1=null;
        }else{
            ProgressRequestBody fileBody1 = new ProgressRequestBody(file1, this);
            filePart1 = MultipartBody.Part.createFormData("cover", file1.getName(), fileBody1);
        }

        call = mService.updateDesignerProfile(nameBody,deliveryBody,
                descriptionBody,addressBody,
                cityBody,stateBody,countryBody,filePart,filePart1,holidayMode);



        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()) {
                    Toast.makeText(EditDesignerProfileActivity.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    EditDesignerProfileActivity.this.finish();
                }else{
                    Toast.makeText(EditDesignerProfileActivity.this, "Internal Server error", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                Toast.makeText(EditDesignerProfileActivity.this, t.getMessage() , Toast.LENGTH_SHORT).show();

                Toast.makeText(EditDesignerProfileActivity.this, "Unable to upload to server" , Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    public File compressFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }


    public boolean cleanForm(){
        boolean cleanedForm;

        if(deliveryStatement.getText().toString().equals("")){
            deliveryStatement.setError("Your delivery statement is compulsory");
            cleanedForm = false;
            return cleanedForm;
        }
        if(name.getText().toString().equals("")){
            name.setError("Your nme is compulsory");
            cleanedForm = false;
            return cleanedForm;
        }
        if(address.getText().toString().equals("")){
            address.setError("Your address is compulsory");
            cleanedForm = false;
            return cleanedForm;
        }

        if(description.getText().toString().equals("")){
            description.setError("Your description is compulsory");
            cleanedForm = false;
            return cleanedForm;
        }

        if(city.getText().toString().equals("")){
            city.setError("Your city is compulsory");
            cleanedForm = false;
            return cleanedForm;
        }
        if(state.getText().toString().equals("")){
            state.setError("Your state is compulsory");
            cleanedForm = false;
            return cleanedForm;
        }

        if(isEmpty(countrySpinner.getSelectedItem().toString().trim())){
            Toast.makeText(this,"Country field is compulsory",Toast.LENGTH_LONG).show();
            cleanedForm = false;
            return cleanedForm;
        }

        if(dp.getDrawable().getConstantState()==getDrawable(R.drawable.unknownprofile).getConstantState()){
            cleanedForm = false;
            return cleanedForm;
        }
        return true;
    }


    @Override
    public void onProgressUpdate(long uploadedSize,File file) {

    }

    @Override
    public void onError() {
        // do something on error
        Toast.makeText(EditDesignerProfileActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFinish() {
        // do something on upload finished
        // for example start next uploading at queue
        Toast.makeText(EditDesignerProfileActivity.this, "Finished...Glad", Toast.LENGTH_SHORT).show();
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (Build.VERSION.SDK_INT >= 19) {
            String[] proj = { MediaStore.Images.Media.DATA };
            CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String result = cursor.getString(column_index);
            cursor.close();
            return result;
        }
        if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 18) {
            String[] proj = { MediaStore.Images.Media.DATA };
            String result = null;
            android.content.CursorLoader cursorLoader = new android.content.CursorLoader(
                    this,
                    contentUri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            if(cursor != null){
                int column_index =
                        cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        }

        if (Build.VERSION.SDK_INT < 11) {
            String[] proj = { MediaStore.Images.Media.DATA };
            Cursor cursor = this.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index
                    = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case DP:
                if(resultCode == RESULT_OK){
                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        String realPathfromURI=getRealPathFromURI(imageUri);
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                            dp.setImageBitmap(selectedImage);
                            file=new File(realPathfromURI);
                            file=compressFile(file);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case COVER_PHOTO:
                if(resultCode == RESULT_OK){
                    try {
                        final Uri imageUri = imageReturnedIntent.getData();
                        String realPathfromURI=getRealPathFromURI(imageUri);
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        coverPhoto.setImageBitmap(selectedImage);
                        file1=new File(realPathfromURI);
                        file1=compressFile(file1);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }
                break;
        }
    }
}
