package com.bolaandroid.taas;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bolaandroid.adapter.DesignerStylesAdapter;
import com.bolaandroid.adapter.ReviewListAdapter;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.CustomStylesPaginatedResult;
import com.bolaandroid.data.model.Designer;
import com.bolaandroid.data.model.Rating;
import com.bolaandroid.data.model.remote.TAASService;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class DesignerActivity extends AppCompatActivity implements View.OnClickListener{
    Designer designer;
    TAASService mService;
    RecyclerView recyclerView;
    RecyclerView recyclerView2;
    ArrayList<CustomStyles> styles=new ArrayList<>();
    DesignerStylesAdapter adapter;
    ProgressBar progressBar;
    Snackbar snackbar;
    ArrayList<Rating> ratings = new ArrayList<>();
    DonutProgress[] donutProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_designer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);
        mService = ApiUtils.getTAASService();
        designer=(Designer) getIntent().getSerializableExtra("designer");
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ImageView designerDP = (ImageView) findViewById(R.id.designer_pic);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView2 = (RecyclerView) findViewById(R.id.recycler_view2);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rel_layout3);
        TextView portfolio = (TextView) findViewById(R.id.portfolio);
        Button button = (Button) findViewById(R.id.more_button);
        relativeLayout.setOnClickListener(this);
        portfolio.setOnClickListener(this);
        button.setOnClickListener(this);
        recyclerView.setOnClickListener(this);
        ImageView designerCover = (ImageView) findViewById(R.id.place_image);
        TextView designerName = (TextView) findViewById(R.id.designer_name);
        TextView bio = (TextView) findViewById(R.id.desc);
        TextView deliveryStatement = (TextView) findViewById(R.id.delivery_statement);
        TextView ratingNumber = (TextView) findViewById(R.id.ratingNumber);
        RatingBar rating = (RatingBar) findViewById(R.id.ratingBar);
        Picasso.with(this).load(designer.getDp()).placeholder(getColor()).into(designerDP);
        Picasso.with(this).load(designer.getCoverPhoto()).placeholder(getColor()).into(designerCover);
        designerName.setText(designer.getName());
        deliveryStatement.setText(designer.getDeliveryStatement());
        bio.setText(designer.getBio());
        try{
            Double ratingDouble = designer.getAverageRating();
            ratingNumber.setText(ratingDouble.toString());
            rating.setRating(ratingDouble.floatValue());
        }catch (Exception e){
            Double ratingDouble = designer.getAverageRating();
            if(ratingDouble==null){
                ratingNumber.setText("This designer is not yet rated");
                rating.setVisibility(View.GONE);
            }

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        collapsingToolbarLayout.setTitle(" ");
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }
        // Hide Status Bar
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(designer.getName());
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
        DonutProgress qualityDonut = (DonutProgress) findViewById(R.id.donut_quality);
        DonutProgress responsiveDonut = (DonutProgress) findViewById(R.id.donut_responsive);
        DonutProgress deliveryDonut = (DonutProgress) findViewById(R.id.donut_delivery);
        qualityDonut.setDonut_progress(designer.getAverageQuality().toString());
        responsiveDonut.setDonut_progress(designer.getAverageResponsiveness().toString());
        deliveryDonut.setDonut_progress(designer.getAverageDeliveryTime().toString());

        getStylesData();
        //getDesignerReviews();
    }

    @Override
    public void onClick(View v) {
        Intent myIntent = new Intent(DesignerActivity.this,
                CustomActivity.class).putExtra("designerPk",designer.getPk());
        myIntent.putExtra("designerTitle",designer.getName());
        DesignerActivity.this.startActivity(myIntent);
    }


    public void getStylesData(){
        Call<CustomStylesPaginatedResult> call= mService.getDesignerStyles(designer.getPk(),5,0);
        call.enqueue(new Callback<CustomStylesPaginatedResult>() {
            @Override
            public void onResponse(Call<CustomStylesPaginatedResult> call, Response<CustomStylesPaginatedResult> response) {
                if(response.isSuccessful()){
                    styles=response.body().getResults();
                    progressBar.setVisibility(View.INVISIBLE);
                    if(styles.size()>5) {
                        ArrayList<CustomStyles> firstFiveStyles = new ArrayList<CustomStyles>(styles.subList(0,5));
                        adapter = new DesignerStylesAdapter(firstFiveStyles);
                    }else{
                        adapter = new DesignerStylesAdapter(styles);
                    }
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DesignerActivity.this,RecyclerView.HORIZONTAL,false);
                    mLayoutManager.setAutoMeasureEnabled(true);
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setNestedScrollingEnabled(false);
                    recyclerView.setAdapter(adapter);
                }else{

                    progressBar.setVisibility(View.INVISIBLE);
                    snackbar = Snackbar.make(recyclerView, "Error connecting to server", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getStylesData();
                            snackbar.dismiss();
                        }
                    });
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<CustomStylesPaginatedResult> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                snackbar = Snackbar.make(recyclerView, "No internet connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getStylesData();
                        snackbar.dismiss();
                    }
                });
                snackbar.show();
            }
        });

        Call<ArrayList<Rating>> call1=mService.getDesignerRatings(designer.getPk());
        call1.enqueue(new Callback<ArrayList<Rating>>() {
            @Override
            public void onResponse(Call<ArrayList<Rating>> call, Response<ArrayList<Rating>> response) {
                if(response.isSuccessful()){
                    ratings = response.body();
                    ReviewListAdapter reviewlistadapter;
                    if(ratings.size()>5){
                        ArrayList<Rating> firstFiveRatings = new ArrayList<Rating>(ratings.subList(0, 4));
                        reviewlistadapter = new ReviewListAdapter(firstFiveRatings);
                    }else{
                        reviewlistadapter = new ReviewListAdapter(ratings);
                    }

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DesignerActivity.this);
                    recyclerView2.setLayoutManager(mLayoutManager);
                    recyclerView2.setItemAnimator(new DefaultItemAnimator());
                    recyclerView2.setAdapter(reviewlistadapter);

                }else{
                    progressBar.setVisibility(View.INVISIBLE);
                    snackbar = Snackbar.make(recyclerView, "No internet connection", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getStylesData();
                            snackbar.dismiss();
                        }
                    });
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Rating>> call, Throwable t) {
                snackbar = Snackbar.make(recyclerView, "No internet connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getStylesData();
                        snackbar.dismiss();
                    }
                });
                Toast.makeText(DesignerActivity.this,"No internet connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public GradientDrawable getColor(){
        GradientDrawable gradientDrawable=new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setColor(Color.GRAY);
        return gradientDrawable;

    }

}
