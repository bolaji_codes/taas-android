package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.DesignerOrdersAdapter;
import com.bolaandroid.adapter.OrdersAdapter;
import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.DesignerOrders;
import com.bolaandroid.data.model.Order;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.interfaces.ShowUpdateDialog;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

public class UndeliveredOrdersActivity extends AppCompatActivity implements ShowUpdateDialog {
    TAASService mService;
    RecyclerView recyclerView;
    DesignerOrdersAdapter adapter;
    ProgressDialog dialog;
    boolean success=false;
    Toolbar toolbar;
    ArrayList<DesignerOrders> mOrders = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;
    TextView noListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_orders);
        mService= ApiUtils.getTAASService();
        dialog=new ProgressDialog(this);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setTitle("Undelivered Orders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        noListView = (TextView) findViewById(R.id.no_list_text);
        loadAllOrders();
    }

    public void loadAllOrders(){
        swipeRefreshLayout.setRefreshing(true);
        Call<ArrayList<DesignerOrders>> call = mService.getDesignerUndeliveredOrders();
        call.enqueue(new Callback<ArrayList<DesignerOrders>>() {
            @Override
            public void onResponse(Call<ArrayList<DesignerOrders>> call, Response<ArrayList<DesignerOrders>> response) {
                if (response.isSuccessful()){
                    mOrders = response.body();
                    loadlayout(mOrders);
                    if(response.body().isEmpty()){
                        swipeRefreshLayout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }else{
                    swipeRefreshLayout.setRefreshing(false);
                    final Snackbar snackbar= Snackbar.make(recyclerView,"Server error", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY",new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {

                            loadAllOrders();
                        }
                    });
                    snackbar.show();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<DesignerOrders>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                final Snackbar snackbar= Snackbar.make(recyclerView,"No internet connection", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY",new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {


                        loadAllOrders();
                    }
                });
                snackbar.show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadlayout(ArrayList<DesignerOrders> orders){
        adapter = new DesignerOrdersAdapter(orders,true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    public void showUpdateDialog(final int position,final int pk, final String currentStatus) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_order_status, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final Spinner status = (Spinner) dialogView.findViewById(R.id.update_status_spinner);
        ArrayList<CharSequence> strings = new ArrayList<>();
        for(String stats:fillSpinner(currentStatus)){
            strings.add(stats);
        }
        ArrayAdapter<CharSequence> adapter =
                new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, strings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        status.setAdapter(adapter);
        dialogBuilder.setTitle("Update order status");
        dialogBuilder.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateStatus(position,pk,status.getSelectedItem().toString());
                        b.dismiss();
                    }
                });
            }
        });
        b.show();
    }

    public void updateStatus(final int position, int pk,String status){
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                dialog.setMessage("Updating...Please wait");
                dialog.show();
            }
        },100);
        final String mStatus=getStatusAbrrev(status);
        Call<CreateStyleResponse> call = mService.updateOrderStatus(pk,mStatus);
        call.enqueue(new Callback<CreateStyleResponse>() {
            @Override
            public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                if(response.isSuccessful()){
                    if(mStatus=="D"){
                        dialog.dismiss();
                        mOrders.remove(position);
                        adapter.notifyItemRemoved(position);
                        Toast.makeText(UndeliveredOrdersActivity.this,"Successfully updated....Removed from undelivered order",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(UndeliveredOrdersActivity.this,"Successfully updated",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    mOrders.get(position).setStatus(mStatus);
                    adapter.notifyItemChanged(position);
                }else{
                    Toast.makeText(UndeliveredOrdersActivity.this,"Server error...Unable to upload",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                Toast.makeText(UndeliveredOrdersActivity.this,"No internet connection...Unable to upload",Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    public ArrayList<String> fillSpinner(String status){
        ArrayList<String> strings = new ArrayList<>();
        switch(status){
            case "Order received by designer":
                strings.add("Currently been sown");
                strings.add("To be delivered tomorrow");
                strings.add("Delivered");
                break;
            case "Currently been sown":
                strings.add("To be delivered tomorrow");
                strings.add("Delivered");
                break;
            case "To be delivered tomorrow":
                strings.add("Delivered");
                break;
        }
        return strings;
    }

    public String getStatusAbrrev(String status){
        String abbrev="default";
        switch (status){
            case "Order received by designer":
                abbrev = "R";
                break;
            case "Currently been sown":
                abbrev = "S";
                break;
            case "To be delivered tomorrow":
                abbrev = "DT";
                break;
            case "Delivered":
                abbrev = "D";
                break;
        }
        return abbrev;
    }
}
