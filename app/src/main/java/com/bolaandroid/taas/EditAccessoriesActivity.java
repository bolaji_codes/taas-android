package com.bolaandroid.taas;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.CreateStyleResponse;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.remote.TAASService;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.ProgressRequestBody;

import static android.text.TextUtils.isEmpty;
import static com.bolaandroid.taas.AddNewStyles.verifyStoragePermissions;

public class EditAccessoriesActivity extends AppCompatActivity implements View.OnClickListener{
        private final int SELECT_PHOTO = 1;
        EditText titleEdit,priceEdit;
        ImageView img1;
        Spinner gender;
        TAASService mService;
        ProgressDialog dialog;
        Button button;
        Toolbar toolbar;
        NumberPicker numberPicker;
        CustomAccessories accessory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_accessories);
        mService = ApiUtils.getTAASService();
        dialog = new ProgressDialog(this);
        dialog.setCanceledOnTouchOutside(false);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Accessories");
        numberPicker = (NumberPicker) findViewById(R.id.number_picker);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(100);
        numberPicker.setValue(1);
        titleEdit = (EditText) findViewById(R.id.name);
        img1 = (ImageView) findViewById(R.id.image1);
        priceEdit = (EditText) findViewById(R.id.price);
        button = (Button) findViewById(R.id.button);
        accessory = (CustomAccessories) getIntent().getSerializableExtra("accessory");
        gender = (Spinner) findViewById(R.id.gender_spinner);
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.gender_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter);
        if(accessory.getGender().equals("M")) {
            gender.setSelection(adapter.getPosition("Male"));
        }
            else{
                gender.setSelection(adapter.getPosition("Female"));
            }
        Picasso.with(this)
                .load(accessory.getPhoto1())
                .resize(700,700)
                .centerInside()
                .placeholder(getRandomColor())
                .into(img1);
        titleEdit.setText(accessory.getName());
        priceEdit.setText(String.valueOf(accessory.getPrice()));
        if(accessory.getStock()!=null){
            numberPicker.setValue(accessory.getStock());
        }else {
            numberPicker.setValue(100);
        }
        button.setOnClickListener(this);
    }


        public void uploadAccessoriestoServer(){
            if(!cleanForm()){
                return;
            }

            if(!verifyStoragePermissions(this)){
                return;
            }

            new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                    dialog.setMessage("Accessory updating...please wait");
                    dialog.setIndeterminate(true);
                    dialog.show();
                }
            },100);



            RequestBody title1 =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), titleEdit.getText().toString());

            RequestBody price1 =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), priceEdit.getText().toString());
            String genderAbbrev;
            if(gender.getSelectedItem().toString().equals("Male")){
                genderAbbrev="M";
            }else {
                genderAbbrev = "F";
            }
            RequestBody fabricStock =RequestBody.create(
                    MediaType.parse("multipart/form-data"), String.valueOf(numberPicker.getValue()));

            RequestBody gender1 =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), genderAbbrev);
            Call<CreateStyleResponse> call=mService.updateAccessory(accessory.getPk(),title1,gender1,price1,fabricStock);
            call.enqueue(new Callback<CreateStyleResponse>() {
                @Override
                public void onResponse(Call<CreateStyleResponse> call, Response<CreateStyleResponse> response) {
                    if(response.isSuccessful()){
                        Toast.makeText(EditAccessoriesActivity.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        EditAccessoriesActivity.this.finish();
                    }else{
                        Toast.makeText(EditAccessoriesActivity.this, "Internal Server error", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<CreateStyleResponse> call, Throwable t) {
                    Toast.makeText(EditAccessoriesActivity.this, "Unable to upload to server" , Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });

        }
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch(item.getItemId()) {
                case android.R.id.home:
                    finish();
                    return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onClick(View v){
            switch(v.getId()){
                case R.id.button:
                    uploadAccessoriestoServer();
                    break;
                default:

                    break;

            }
        }

        public boolean cleanForm(){
            boolean canSendData=true;
            if(isEmpty(titleEdit.getText().toString().trim())){
                titleEdit.requestFocus();
                titleEdit.setError("Please fill in the title");
                canSendData=false;
            }
            if(isEmpty(priceEdit.getText().toString().trim())){
                priceEdit.requestFocus();
                priceEdit.setError("Please fill in the price");
                canSendData=false;
            }

            String actualPostionOfSpinner = (String) gender.getItemAtPosition(gender.getSelectedItemPosition());
            if(actualPostionOfSpinner.isEmpty()){
                setSpinnerError(gender,"FabricCollection can't be empty");
                canSendData=false;
            }

            if(img1.getDrawable().getConstantState()==getDrawable(R.mipmap.add_image_icon).getConstantState()){
                Toast.makeText(this,"You have to upload at least an image",Toast.LENGTH_SHORT).show();
                canSendData=false;
            }
            return canSendData;
        }

        private void setSpinnerError(Spinner spinner, String error){
            View selectedView = spinner.getSelectedView();
            if (selectedView != null && selectedView instanceof TextView) {
                spinner.requestFocus();
                TextView selectedTextView = (TextView) selectedView;
                selectedTextView.setError(error); // any name of the error will do
                selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
                selectedTextView.setText(error); // actual error message
                spinner.performClick(); // to open the spinner list if error is found.

            }
        }

        public int getRandomColor(){
            List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                    R.color.ms_black_87_opacity,
                    R.color.ms_material_blue_500,
                    R.color.ms_errorColor,
                    R.color.brown);
            Random random = new Random();
            return colorz.get(random.nextInt(colorz.size()));

        }
}
