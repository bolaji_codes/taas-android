package com.bolaandroid.taas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;

import retrofit2.Call;
import retrofit2.Callback;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.data.model.RegisterData;
import com.bolaandroid.data.model.LoginResponse;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.data.model.User;

import java.util.ArrayList;

import retrofit2.Response;
import utils.ApiUtils;

public class UserListActivity extends AppCompatActivity {

    private TAASService mService;
    EditText username,password,email;
    TextView response001;
    Button button;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        email = (EditText) findViewById(R.id.et_email);
        username = (EditText) findViewById(R.id.et_title);
        password = (EditText) findViewById(R.id.et_body);
        response001 = (TextView) findViewById(R.id.tv_response);
        button = (Button) findViewById(R.id.btn_submit);
        mService = ApiUtils.getTAASService();



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UserListActivity.this,"Clicked",Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = getSharedPreferences(
                        "TAG",1
                ).edit();
                editor.putString("token","59cdb79d9177c54abf7a55b7318a6de3ddd20f67");
                editor.apply();


            }
        });

        button.setOnLongClickListener(new View.OnLongClickListener(){

            @Override
            public boolean onLongClick(View v) {
                /**
                SharedPreferences prefs = getSharedPreferences("TAG",1);
                if (prefs.contains("token")) {
                    String name = prefs.getString("token", "No token here");
                    Toast.makeText(UserListActivity.this, name, Toast.LENGTH_SHORT).show();
                    response001.setText(name);
                }
                 **/
                showUsers();
                return false;

            }
        });


    }

    public void register(){
        Call<LoginResponse> call=mService.register(
                new RegisterData(email.getText().toString().trim(),
                        username.getText().toString().trim(),
                        password.getText().toString().trim())
        );
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {

                    Log.i("sabdnd", "Successfully registered" + response.body().toString());
                    login();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("hshshjshj", "Unable to submit post to API."
                        + t.getMessage());

            }
        });

    };

    public void showUsers(){
        Call<ArrayList<User>> call=mService.getUsers();
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                if(response.isSuccessful()) {
                    Toast.makeText(UserListActivity.this,
                            "Successful", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(UserListActivity.this,
                            "Not Successfulllll",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.e("hshshjshj", "Unable to submit post to API."
                        + t.getMessage());
                Toast.makeText(UserListActivity.this,
                        "Not Successful",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void login(){
        Call<LoginResponse> call=mService.login(
                new RegisterData(email.getText().toString().trim(),
                        username.getText().toString().trim(),
                        password.getText().toString().trim())
        );
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()) {
                    showResponse(response.body().getAuthToken());
                    Log.i("sabdnd", "Successfully logged in" + response.body().toString());
                    Toast.makeText(UserListActivity.this,
                            "Successfully logged in",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("hshshjshj", "Unable to submit post to API."
                        + t.getMessage());

            }
        });

    };

    public void showResponse(String response) {
        if(response001.getVisibility() == View.GONE) {
            response001.setVisibility(View.VISIBLE);
        }
        response001.setText(response);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list,menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_account:
                registerDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void registerDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.registerdialog, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final Button button = (Button) dialogView.findViewById(R.id.login_button);
        final EditText username = (EditText) dialogView.findViewById(R.id.username);
        final EditText email = (EditText) dialogView.findViewById(R.id.email);
        final EditText password = (EditText) dialogView.findViewById(R.id.password);
        final EditText address = (EditText) dialogView.findViewById(R.id.address);
        Spinner spinner = (Spinner) dialogView.findViewById(R.id.countries_spinner);

        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(this,R.array.country_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        dialogBuilder.setTitle("Register an account with us");
        dialogBuilder.setPositiveButton("REGISTER", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
        Button nButton=b.getButton(DialogInterface.BUTTON_NEGATIVE);
        nButton.setTextColor(Color.GRAY);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.hide();
                loginDialog();
            }
        });
    }

    public void loginDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.logindialog, null);
        dialogBuilder.setView(dialogView);
        final Context context=this;
        final EditText email = (EditText) dialogView.findViewById(R.id.email);
        final EditText password = (EditText) dialogView.findViewById(R.id.password);
        dialogBuilder.setTitle("Provide your login credentials");
        dialogBuilder.setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();

    }
}
