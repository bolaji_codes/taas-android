package com.bolaandroid.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.MeasurementAdapter;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.MeasureMePrice;
import com.bolaandroid.data.model.Measurement;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.interfaces.GoToNext;
import com.bolaandroid.provider.MeasurementProvider;
import com.bolaandroid.taas.EditMeasurementActivity;
import com.bolaandroid.taas.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.Context2ActivityConverter;

import static android.content.Context.MODE_PRIVATE;
import static java.lang.Integer.parseInt;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeasurementFragment extends
        Fragment implements Step,LoaderManager.LoaderCallbacks<Cursor> {
    // ListView listView;
    RecyclerView recyclerView;
    MeasurementAdapter adapter = null;
    int measureMePrice=1000;
    View v;
    Button button,comeAndMeasureMe,chooseSize;
    public long taskId;
    boolean styleGenderChecker=false;
    String styleGender;
    TextView sowForWho,noListText;
    CustomStyles style;
    boolean comeandmeasuremepermission=false;

    public MeasurementFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver2,
                new IntentFilter("designer_location"));
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("accessories-params"));
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("styleGender",styleGender);
        Log.e("ONSAVE:","SAVE ACTIVATED");
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        try {
            if(savedInstanceState!=null) {
                styleGender = (String) savedInstanceState.get("styleGender");
                Log.e("ONRETREATED:","ONRETREATED ACTIVATED");
            }
        }
        catch (Exception e){}

    }
    @Override
    public Loader<Cursor> onCreateLoader(int ignored, Bundle args) {
        return new CursorLoader(getActivity(),
                MeasurementProvider.CONTENT_URI, null,MeasurementProvider.COLUMN_GENDER + "=?",new String[] {styleGender}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader,Cursor cursor) {
        adapter.swapCursor(cursor);
        String fullStyleGenderName;
        if(styleGender.equals("M")){
            fullStyleGenderName="Male";
        }else {fullStyleGenderName="Female";}
        noListText.setText("You haven't added any "+ fullStyleGenderName + " measurement yet.");
        if (cursor.getCount() == 0) {
            noListText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            noListText.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=inflater.inflate(R.layout.fragment_measurement, container, false);
        getMeasuremePrice();
        if(savedInstanceState!=null){
            adapter = new MeasurementAdapter(getActivity(),true);
            styleGender = (String) savedInstanceState.get("styleGender");
            styleGenderChecker=true;
        }else {
            adapter = new MeasurementAdapter(getActivity(),true);
        }
        Log.e("ONCREATEVIEW","ONCREATEVIEW");
        comeAndMeasureMe = (Button) v.findViewById(R.id.comeandmeasureme);
        chooseSize = (Button) v.findViewById(R.id.measurementguide);
        sowForWho=(TextView) v.findViewById(R.id.sowForWho);
        noListText=(TextView) v.findViewById(R.id.no_list_text);
        comeAndMeasureMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumberCustomMeasurementDialog();
            }
        });
        chooseSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSizes(styleGender);
            }
        });
        button=(Button) v.findViewById(R.id.findSelected);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(getContext(),R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Gender")
                        .setMessage("What gender does the person belongs to?")
                        .setCancelable(true)
                        .setNegativeButton(R.string.male, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialogInterface,
                                    int i)
                            {
                                String male="M";
                                if(styleGender.equals(male)){
                                    // Start NewActivity.class
                                    Intent myIntent = new Intent(getActivity(),
                                            EditMeasurementActivity.class).putExtra(EditMeasurementActivity.MEASUREMENT_ID,0);
                                    myIntent.putExtra("styleGender",styleGender);
                                    startActivity(myIntent);
                                }
                                else{
                                    showGenderSureDialog(male);
                                }
                            }



                        })
                        .setPositiveButton(
                                R.string.female,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialogInterface,
                                            int i)
                                    {
                                        String female="F";
                                        if(styleGender.equals(female)){
                                            // Start NewActivity.class
                                            Intent myIntent = new Intent(getActivity(),
                                                    EditMeasurementActivity.class).putExtra(EditMeasurementActivity.MEASUREMENT_ID,0);
                                            myIntent.putExtra("styleGender",styleGender);
                                            startActivity(myIntent);
                                        }
                                        else{
                                            showGenderSureDialog(female);
                                        }
                                    }
                                });

                AlertDialog alert=builder.create();
                alert.show();


            }
        });
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity()));
        return v;
    }

    public BroadcastReceiver mMessageReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            styleGender=intent.getStringExtra("styleGender");
            Log.e("MEAS FRAG RECEIVED:","MEAS FRAG RECEIVED:"+styleGender);

        }
    };


    public void showGenderSureDialog(final String gender){
        String full;
        if(gender=="M"){
            full="Male";
        }else{
            full="Female";
        }
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext(),R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Gender")
                .setMessage("Just curious, but you didn't chose a "+full+"'s " + "style")
                .setCancelable(true)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialogInterface,
                            int i)
                    {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(
                        "I know",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialogInterface,
                                    int i)
                            {
                                // Start NewActivity.class
                                Intent myIntent = new Intent(getActivity(),
                                        EditMeasurementActivity.class).putExtra(EditMeasurementActivity.MEASUREMENT_ID,0);
                                myIntent.putExtra("styleGender",gender);
                                startActivity(myIntent);

                            }
                        });

        AlertDialog alert=builder.create();
        alert.show();
        Button nButton=alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nButton.setTextColor(Color.GRAY);



    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        if(adapter.sendUserCount()==true){
            return null;
        }
        if(comeandmeasuremepermission){
            comeandmeasuremepermission = false;
            return null;
        }


        return new VerificationError("You've to select someone we would sow for");
    }

    @Override
    public void onSelected() {
        //update UI when selected
        getLoaderManager().initLoader(0,null,this);
        getLoaderManager().restartLoader(0,null,this);
        Log.e("ONSELECTED:","ONSELECTED ACTIVATED");
        getMeasuremePrice();

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    public void showNumberCustomMeasurementDialog() {
        final Context context=recyclerView.getContext();
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.number_custom_measurement_dialog, null);
        dialogBuilder.setView(dialogView);
        final TextView text = (TextView) dialogView.findViewById(R.id.how_many_people);
        final TextView introText = (TextView) dialogView.findViewById(R.id.come_measure_me_info);
        final Spinner numberCustomMeasurements = (Spinner) dialogView.findViewById(R.id.number_of_request_custom_measurement_users);
        java.util.ArrayList<CharSequence> strings = new java.util.ArrayList<>();

        introText.setText("For you to use this service, you have to be in the same location with the designer , which is " + style.getDesigner().getState() + " and it costs an additional "+ NumberFormat.getNumberInstance(Locale.US).format(measureMePrice) +" Naira. To use, please fill this form below:");
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(context,R.array.number_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        numberCustomMeasurements.setAdapter(adapter);
        dialogBuilder.setTitle("Measure me request info");
        dialogBuilder.setPositiveButton("NEXT", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {}
        });

        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int num_custom_measurements = Integer.parseInt(numberCustomMeasurements.getSelectedItem().toString());

                        ArrayList<Measurement> customMeasurements = new ArrayList<>();
                        Measurement customMeasurement=new Measurement(
                                "Come and measure me","unknown","0","0",
                                "0","0","0","0","0",
                                "0","0","0","0","0",
                                "0","0","0","0","0","0","0","0","0"
                        );
                        for(int i=0;i<num_custom_measurements;i++){
                            customMeasurements.add(customMeasurement);
                        }

                        comeandmeasuremepermission = true;
                        Log.e("1custMeasuement size is",String.valueOf(customMeasurements.size()));
                        Intent intent=new Intent("usersSelected");
                        Bundle bundle=new Bundle();
                        ArrayList<Integer> numberUsersToBeMeasured=new ArrayList<>();
                        bundle.putInt("usersSelected", num_custom_measurements);
                        bundle.putSerializable("selectedMeasurement",customMeasurements);
                        bundle.putBoolean("comeandmeasureme",true);
                        bundle.putInt("measureMePrice",measureMePrice);
                        intent.putExtra("bundle", bundle);
                        LocalBroadcastManager.getInstance(v.getContext()).sendBroadcastSync(intent);
                        Context2ActivityConverter activityConverter=new Context2ActivityConverter(v.getContext());
                        ((GoToNext) activityConverter.getActivity()).goToNext();
                        b.dismiss();
                    }
                });
            }
        });
        b.show();
    }


    public void showSizes(String gender) {
        final Context context=recyclerView.getContext();
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.choose_size_layout, null);
        dialogBuilder.setView(dialogView);
        final Spinner numberCustomMeasurements = (Spinner) dialogView.findViewById(R.id.choose_size_spinner);
        final ImageView measurementImage = (ImageView) dialogView.findViewById(R.id.measurement_image);
        final TextView measurementGender = (TextView) dialogView.findViewById(R.id.measurement_gender);
        java.util.ArrayList<CharSequence> strings = new java.util.ArrayList<>();
        if(styleGender.equals("M")){
            measurementGender.setText("MALE SIZE CHART");
            measurementImage.setImageResource(R.drawable.male_measurement);
        }else{
            measurementGender.setText("FEMALE SIZE CHART");
            measurementImage.setImageResource(R.drawable.female_measurement);
        }

        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(context,R.array.size_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        numberCustomMeasurements.setAdapter(adapter);
        numberCustomMeasurements.setSelection(adapter.getPosition("M"));
        dialogBuilder.setTitle("Choose a size");
        dialogBuilder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {}
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button be=b.getButton(AlertDialog.BUTTON_POSITIVE);
                be.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int num_custom_measurements = 1;
                        String chozenSize=numberCustomMeasurements.getSelectedItem().toString();
                        ArrayList<Measurement> customMeasurements = new ArrayList<>();
                        Measurement customMeasurement=new Measurement(
                                chozenSize,chozenSize,"0","0",
                                "0","0","0","0","0",
                                "0","0","0","0","0",
                                "0","0","0","0","0","0","0","0","0"
                        );
                        for(int i=0;i<num_custom_measurements;i++){
                            customMeasurements.add(customMeasurement);
                        }

                        comeandmeasuremepermission = true;
                        Log.e("1custMeasuement size is",String.valueOf(customMeasurements.size()));
                        Intent intent=new Intent("usersSelected");
                        Bundle bundle=new Bundle();
                        ArrayList<Integer> numberUsersToBeMeasured=new ArrayList<>();
                        bundle.putInt("usersSelected", num_custom_measurements);
                        bundle.putSerializable("selectedMeasurement",customMeasurements);
                        bundle.putBoolean("comeandmeasureme",false);
                        intent.putExtra("bundle", bundle);
                        LocalBroadcastManager.getInstance(v.getContext()).sendBroadcastSync(intent);
                        Context2ActivityConverter activityConverter=new Context2ActivityConverter(v.getContext());
                        ((GoToNext) activityConverter.getActivity()).goToNext();
                        b.dismiss();
                    }
                });
            }
        });
        b.show();
    }


    public BroadcastReceiver mMessageReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle=new Bundle();
            bundle=intent.getBundleExtra("bundle");
            Log.e("sss2","sss");
            if (bundle!=null) {
                style = (CustomStyles) bundle.getSerializable("style");
                Log.e("sss","sss");

        }
        }
    };


    public void getMeasuremePrice(){
        TAASService mService = ApiUtils.getTAASService();
        Call<MeasureMePrice> call = mService.getMeasureMePrice();
        call.enqueue(new Callback<MeasureMePrice>() {
            @Override
            public void onResponse(Call<MeasureMePrice> call, Response<MeasureMePrice> response) {
                if(response.isSuccessful()){
                    measureMePrice = response.body().getMeasureMePrice();
                }else{
                    getMeasuremePrice();
                }
            }

            @Override
            public void onFailure(Call<MeasureMePrice> call, Throwable t) {
                getMeasuremePrice();
            }
        });
    }
}
