package com.bolaandroid.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.StylesListAdapter;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.CustomStylesPaginatedResult;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.interfaces.RemoveOutOfStockStyles;
import com.bolaandroid.taas.CustomActivity;
import com.bolaandroid.taas.R;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.AppContext;
import utils.EndlessRecyclerViewScrollListener;

import static android.text.TextUtils.isEmpty;

/**
 * A simple {@link Fragment} subclass.
 */
public class StylesFragment extends Fragment implements Step,SwipeRefreshLayout.OnRefreshListener {
    private static final String STATE_ITEMS = "items";
    RecyclerView recyclerView;
    private TAASService mService;
    private SwipeRefreshLayout swipeRefreshLayout;
    public ArrayList<CustomStyles> styles=new ArrayList<>();
    private StylesListAdapter adapter;
    private CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;
    TextView noListView;
    boolean i=false;
    boolean instanceSaved=false;
    FloatingActionButton fab;
    int mini,maxi;
    String gender;
    int designerPk=0;
    StaggeredGridLayoutManager gridLayoutManager;
    private EndlessRecyclerViewScrollListener scrollListener;
    Bundle bundle,savedInstanceStateReplica;
    ProgressBar loadMoreProgressBar;

    int rant=0;
    int page;
    public StylesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        bundle=getArguments();
        if(bundle.containsKey("designerPk")) {
            designerPk = bundle.getInt("designerPk");
        }
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-params"));
    }




    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View v=inflater.inflate(R.layout.fragment_styles, container, false);
        recyclerView=(RecyclerView) v.findViewById(R.id.recycler);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        mService = ApiUtils.getTAASService();
        noListView = (TextView) v.findViewById(R.id.no_list_text);
        fab=(FloatingActionButton) v.findViewById(R.id.fabBtns);
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id
                .layout_main);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChangeLangDialog();
            }
        });

        loadMoreProgressBar = (ProgressBar) v.findViewById(R.id.load_more_progress_bar);
        gridLayoutManager = new StaggeredGridLayoutManager(getGridSpanCount(), StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        if (savedInstanceState == null) {

            if(!bundle.getString("styleCode").equals("No style code")) {
                searchStyleByCode(bundle.getString("styleCode"));
            }else {
                if(styles.size()<1){
                    loadCustomStyles(1,false,"a");
                }else{
                    loadLayout();
                    scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
                        @Override
                        public void onLoadMore(int mPage, int totalItemsCount, RecyclerView view) {
                            loadCustomStyles(page+1,false,"b"); }
                    };
                    recyclerView.addOnScrollListener(scrollListener);
                }

            }
                        } else {
                            //styles.clear();
                            savedInstanceStateReplica=savedInstanceState;
                            styles = (ArrayList<CustomStyles>) savedInstanceState.getSerializable(STATE_ITEMS);
                            page = (int) savedInstanceState.getInt("page");
                            instanceSaved=true;
                            loadLayout();
                            scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
                                @Override
                                public void onLoadMore(int mPage, int totalItemsCount, RecyclerView view) {
                                loadCustomStyles(page+1,false,"b"); }
                            };
                        recyclerView.addOnScrollListener(scrollListener);
                        }
           // Toast.makeText(getContext(), "3:", Toast.LENGTH_SHORT).show();



        return v;
    }

    public void showChangeLangDialog() {
        mini=0;
        maxi=50000000;
        gender="";
        final Context context=getContext();
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_styles_dialog, null);
        dialogBuilder.setView(dialogView);
        final EditText edt = (EditText) dialogView.findViewById(R.id.searchByDesigner);
        final EditText edt2 = (EditText) dialogView.findViewById(R.id.searchByCode);
        final CheckBox checkBox1 = (CheckBox) dialogView.findViewById(R.id.ankaras);
        final Button codeButton = (Button) dialogView.findViewById(R.id.searchByCodebyButton);
        final CheckBox checkBox2 = (CheckBox) dialogView.findViewById(R.id.laces);
        final CheckBox checkBox3 = (CheckBox) dialogView.findViewById(R.id.senators);
        final CheckBox checkBox4 = (CheckBox) dialogView.findViewById(R.id.atikus);
        final CheckBox checkBox5 = (CheckBox) dialogView.findViewById(R.id.ties);
        final CheckBox checkBox6 = (CheckBox) dialogView.findViewById(R.id.others);
        final CrystalRangeSeekbar rangeSeekbar = (CrystalRangeSeekbar) dialogView.findViewById(R.id.rangeSeekbar1);
        final TextView tvMin = (TextView) dialogView.findViewById(R.id.textMin1);
        final Spinner spinner = (Spinner) dialogView.findViewById(R.id.gender_spinner);

        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(context,R.array.gender_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getPosition("Female"));
        //final TextView tvMax = (TextView) dialogView.findViewById(R.id.textMax1);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch(parent.getItemAtPosition(position).toString()) {
                    case "Male":
                        gender="M";
                        break;
                    case "Female":
                        gender="F";
                        break;
                    case "Both":
                        gender="";
                        break;
                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                final String min= NumberFormat.getNumberInstance(Locale.US).format(minValue);
                String max=NumberFormat.getNumberInstance(Locale.US).format(maxValue);
                tvMin.setText("(₦"+ min+" - "+max+")");
            }

        });

        rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                //Toast.makeText(context, String.valueOf(minValue) + " : " + String.valueOf(maxValue), Toast.LENGTH_SHORT).show();
                mini = ((Number) minValue).intValue();
                maxi = ((Number) maxValue).intValue();
                //Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
                //mScrollView.scrollTo(0, mScrollView.getHeight());

            }
        });


        dialogBuilder.setPositiveButton("SEARCH", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                int i=0;
                String c1="",c2="",c3="",c4="",c5="",c6="";
                final String numberArray[] ={c1,c2,c3,c4,c5,c6};
                CheckBox[] checkBoxes={checkBox1,checkBox2,checkBox3,checkBox4,checkBox5,checkBox6};
                while(i<numberArray.length){
                    if (checkBoxes[i].isChecked()) {
                        numberArray[i] = checkBoxes[i].getText().toString();
                    }
                    i++;
                }
                loadFilteredStyles(1,edt.getText().toString(),mini,maxi,gender,numberArray[0],numberArray[1],numberArray[2],numberArray[3],numberArray[4],numberArray[5]);
                recyclerView.removeOnScrollListener(scrollListener);
                scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
                    @Override
                    public void onLoadMore(int mPage, int totalItemsCount, RecyclerView view) {
                        loadFilteredStyles(mPage+1,edt.getText().toString(),mini,maxi,gender,numberArray[0],numberArray[1],numberArray[2],numberArray[3],numberArray[4],numberArray[5]);}
                };
                recyclerView.addOnScrollListener(scrollListener);
                dialog.dismiss();

            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        final AlertDialog b = dialogBuilder.create();
        codeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEmpty(edt2.getText().toString().trim())){
                    edt2.hasFocus();
                    edt2.setError("You have to input a style code");
                    return;
                }
                searchStyleByCode(edt2.getText().toString());
                b.dismiss();
            }
        });
        b.show();
        Button nButton=b.getButton(DialogInterface.BUTTON_NEGATIVE);
        nButton.setTextColor(Color.GRAY);
    }

    public void loadFilteredStyles(
            final int mPage,final String designer,final int min,final int max,final String gender,final String c1,final String c2,final String c3,final String c4,final String c5,final String c6 ){
        if(mPage==1) {
            swipeRefreshLayout.setRefreshing(true);
        }else{
            loadMoreProgressBar.setVisibility(View.VISIBLE);
        }
        int offset = (mPage * 15) - 15;

        Call<CustomStylesPaginatedResult> call=mService.getFilteredStyles(15,offset,designer,min,max,c1,c2,c3,c4,c5,c6,gender);
        call.enqueue(new Callback<CustomStylesPaginatedResult>() {
            @Override
            public void onResponse(Call<CustomStylesPaginatedResult> call,Response<CustomStylesPaginatedResult> response) {

                if(response.isSuccessful()) {
                    loadMoreProgressBar.setVisibility(View.GONE);

                    if(mPage==1){
                        styles.clear();
                        if(response.body().getResults().isEmpty()){
                            recyclerView.setVisibility(View.GONE);
                            noListView.setVisibility(View.VISIBLE);
                            noListView.setText("No style matches search criteria.Swipe down to see all styles or modify your filter terms");
                            Toast.makeText(getContext(), "No styles found", Toast.LENGTH_SHORT).show();
                        }else {
                            noListView.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            Toast.makeText(getContext(), "Successful filtered", Toast.LENGTH_SHORT).show();
                        }
                    }


                    styles.addAll(response.body().getResults());
                    if(mPage==1){
                        adapter.clearAndLoadData(styles);
                        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getContext()){
                            @Override
                            protected int getVerticalSnapPreference(){
                                return LinearSmoothScroller.SNAP_TO_START;
                            }
                        };
                        smoothScroller.setTargetPosition(0);
                        gridLayoutManager.startSmoothScroll(smoothScroller);
                    }else{
                        addNewItems(response.body().getResults(),mPage);
                    }





                    swipeRefreshLayout.setRefreshing(false);

                }else {
                    int statusCode  = response.code();
                    snackbar = Snackbar.make(coordinatorLayout, "Error connecting to server", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    loadFilteredStyles(mPage,designer,min,max,c1,c2,c3,c4,c5,c6,gender);
                                }
                            });
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void onFailure(Call<CustomStylesPaginatedResult> call, Throwable t) {

                final Snackbar snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadFilteredStyles(mPage,designer,min,max,c1,c2,c3,c4,c5,c6,gender);
                            }
                        });
                snackbar.show();
                Log.d("MainActivity", "error loading from API");
                swipeRefreshLayout.setRefreshing(false);
            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try{
            outState.putSerializable(STATE_ITEMS,styles);
            outState.putInt("page",page);


        } catch(Exception e){
            //do nothing
        }
        Log.e("ONSAVE:","SAVE ACTIVATED");

    }





    public void loadLayout(){

        adapter=new StylesListAdapter(styles,designerPk);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);



    }

    public int getGridSpanCount(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            ((CustomActivity) getContext()).getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(displayMetrics);
        }catch (Exception e){}
        int width = displayMetrics.widthPixels;

        if (width>2560){
            return 4;
        }else if(width>1440 && width <= 2560){
            return 3;
        }
        else{
            return 2;
        }
    }

    public void loadCustomStyles(final int mPage, final boolean isForRefresh,String x) {

        if(mPage==1) {
            swipeRefreshLayout.setRefreshing(true);
        }else{
            loadMoreProgressBar.setVisibility(View.VISIBLE);
        }
        Log.e("RAN","RAAAAAAAAANNNNNNNNN ACTIVATED "+x);
        Call<CustomStylesPaginatedResult> call;
        int offset = (mPage * 15) - 15;
        if(designerPk==0) {
            call = mService.getCustomStyles(offset,1);
            if(mPage==1){
                call = mService.getCustomStyles(null,1);
            }
        }else{
            call = mService.getDesignerStyles(designerPk,15,offset);
            if(mPage==1){
                call = mService.getDesignerStyles(designerPk,15,null);
            }


        }
        call.enqueue(new Callback<CustomStylesPaginatedResult>() {
            @Override
            public void onResponse(Call<CustomStylesPaginatedResult> call,Response<CustomStylesPaginatedResult> response) {

                if(response.isSuccessful()) {
                    //styles.clear();
                    //styles = response.body().getResults();

                        page=mPage;
                        loadMoreProgressBar.setVisibility(View.GONE);
                        if(isForRefresh){
                            styles.clear();
                            styles.addAll(response.body().getResults());
                            RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getContext()){
                                @Override
                                protected int getVerticalSnapPreference(){
                                    return LinearSmoothScroller.SNAP_TO_START;
                                }
                            };
                            smoothScroller.setTargetPosition(0);
                            gridLayoutManager.startSmoothScroll(smoothScroller);
                            if(adapter==null){
                                adapter = new StylesListAdapter(styles,designerPk);
                                recyclerView.setAdapter(adapter);
                            }else{
                                adapter.clearAndLoadData(styles);

                            }
                            swipeRefreshLayout.setRefreshing(false);
                            return;
                        }

                        if(mPage==1){
                            styles=response.body().getResults();
                            adapter=new StylesListAdapter(styles,designerPk);
                            recyclerView.setAdapter(adapter);
                        }else{
                            addNewItems(response.body().getResults(),mPage);
                    }


                    if(styles.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                        noListView.setText("No style found.Swipe down to see all styles or search using the 'zoom' button");

                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    swipeRefreshLayout.setRefreshing(false);

                }else {
                    int statusCode  = response.code();
                    snackbar = Snackbar.make(coordinatorLayout, "Error connecting to server", Snackbar.LENGTH_INDEFINITE);
                            snackbar.setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    loadCustomStyles(mPage,false,"c");
                                }
                            });
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void onFailure(Call<CustomStylesPaginatedResult> call, Throwable t) {

                snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadCustomStyles(mPage,false,"d");
                            }
                        });
                snackbar.show();
                Log.d("MainActivity", "error loading from API");
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void addNewItems(ArrayList<CustomStyles> items,int page){
        //the try and catch is to ensure no duplicate data is appended to styles

        styles.addAll(items);
        //Toast.makeText(getContext(),"NUMBER OF ADDED ITEMS: "+ String.valueOf(items.size()),Toast.LENGTH_SHORT).show();
        //Toast.makeText(getContext(),"NUMBER OF WHOLE STYLES: "+ String.valueOf(styles.size()),Toast.LENGTH_SHORT).show();
        adapter.updateData(items,page);
    }

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        loadCustomStyles(1,true,"e");
        recyclerView.removeOnScrollListener(scrollListener);
        scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadCustomStyles(page+1,false,"f");
               }
        };
        recyclerView.addOnScrollListener(scrollListener);
    }

    public BroadcastReceiver mMessageReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            i = intent.getBooleanExtra("boolean", true);

        }
     };



    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        instanceSaved=true;
        return i ? null : new VerificationError("Select a style to proceed");
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.e("PAUSE:","PAUSE ACTIVATED SSS");
        Log.e("page is","ACTIVATED "+String.valueOf(this.page));
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.e("STOP:","STOP ACTIVATED SSS");
        Log.e("page is","ACTIVATED "+String.valueOf(this.page));
        Log.e("style size is","ACTIVATED "+String.valueOf(styles.size()));
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.e("RESUME:","RESUME ACTIVATED");
        Log.e("page is","ACTIVATED "+String.valueOf(this.page));
        Log.e("style size is","ACTIVATED "+String.valueOf(styles.size()));

    }

    @Override
    public void onStart(){
        super.onStart();
        Log.e("START:","START ACTIVATED");
        Log.e("page is","ACTIVATED "+String.valueOf(this.page));


    }


    @Override
    public void onSelected() {
        //update UI when selected
        if(instanceSaved==false){
            if(!bundle.getString("styleCode").equals("No style code")) {
                searchStyleByCode(bundle.getString("styleCode"));
            }else {
                //loadCustomStyles();
                if(savedInstanceStateReplica==null){
                    scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
                        @Override
                        public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                            loadCustomStyles(page+1,false,"g");}
                    };
                    recyclerView.addOnScrollListener(scrollListener);
                }else{
                    styles = (ArrayList<CustomStyles>) savedInstanceStateReplica.getSerializable(STATE_ITEMS);
                    loadLayout();
                }

            }
        }
        else{
            i=false;
        }

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    public void searchStyleByCode(String code){
        swipeRefreshLayout.setRefreshing(true);
        Call<CustomStyles> call = mService.getStyleByCode(code);
        call.enqueue(new Callback<CustomStyles>() {
            @Override
            public void onResponse(Call<CustomStyles> call, Response<CustomStyles> response) {
                if(response.isSuccessful()){
                    styles.clear();
                    styles.add(response.body());
                    ArrayList<CustomStyles> listOfStyles=new ArrayList<>();
                    listOfStyles.add(response.body());
                    recyclerView.removeOnScrollListener(scrollListener);
                    RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getContext()){
                        @Override
                        protected int getVerticalSnapPreference(){
                            return LinearSmoothScroller.SNAP_TO_START;
                        }
                    };
                    smoothScroller.setTargetPosition(0);
                    gridLayoutManager.startSmoothScroll(smoothScroller);
                    if(adapter==null){
                        adapter = new StylesListAdapter(listOfStyles,designerPk);
                        recyclerView.setAdapter(adapter);
                    }else{
                        adapter.clearAndLoadData(listOfStyles);

                    }

                }else{
                    if(response.code()==404){

                        Toast.makeText(getContext(),"Not found, This code is either incorrect or out of stock,contact the issuing fashion designer for more details",Toast.LENGTH_LONG).show();
                        return;
                    }
                    Toast.makeText(getContext(),"Server Error",Toast.LENGTH_SHORT).show();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<CustomStyles> call, Throwable t) {
                Toast.makeText(getContext(),"No internet connection!",Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }


}
