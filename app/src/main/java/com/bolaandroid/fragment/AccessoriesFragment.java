package com.bolaandroid.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.AccessoriesListAdapter;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.taas.CustomActivity;
import com.bolaandroid.taas.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccessoriesFragment extends Fragment implements Step,SwipeRefreshLayout.OnRefreshListener {
    private static final String STATE_ITEMS = "items";
    private static final String STYLE_PK_KEY = "pk";
    private static final String STYLE_GENDER_KEY = "gender";
    RecyclerView recyclerView;
    private TAASService mService;
    public ArrayList<CustomAccessories> accessories=new ArrayList<>();
    AccessoriesListAdapter adapter;
    private CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;
    TextView noListView;
    private SwipeRefreshLayout swipeRefreshLayout;
    int genderPK,oldPk=0;
    String styleGender;
    int height;
    int savedInstancePk=0;
    int width;
    boolean instanceSaved=false;
    View v;

    public AccessoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mService = ApiUtils.getTAASService();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("accessories-params"));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            ((CustomActivity) getContext()).getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(displayMetrics);
        }catch (Exception e){}
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v=inflater.inflate(R.layout.fragment_accessories, container, false);
        recyclerView=(RecyclerView) v.findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        noListView = (TextView) v.findViewById(R.id.no_list_text);
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id
                .layout_main);
        snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE);

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if(swipeRefreshLayout.isRefreshing() || snackbar.isShown()){
                    Toast.makeText(getContext(), "Please wait a moment", Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        if(savedInstanceState!=null){
            swipeRefreshLayout.setRefreshing(true);
            accessories = (ArrayList<CustomAccessories>) savedInstanceState.getSerializable(STATE_ITEMS);
            savedInstancePk=savedInstanceState.getInt("savedInstancePk");
            genderPK = savedInstancePk;
            styleGender=savedInstanceState.getString("styleGender");
            Log.e("STTRRRING","genderPk RETRIEVED: " + String.valueOf(genderPK));
            Log.e("STTRRRING","savedInstancePk RETRIEVED: " + String.valueOf(savedInstancePk));
            instanceSaved=true;
            loadLayout();
            swipeRefreshLayout.setRefreshing(false);
            Log.e("STTRRRING","RAN HERE 1");
        }
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try{
            outState.putSerializable(STATE_ITEMS,accessories);
            savedInstancePk=genderPK;
            outState.putInt("savedInstancePk",savedInstancePk);
            outState.putString("styleGender",styleGender);
            Log.e("STTRRRING","RAN HERE 2");
            Log.e("STTRRRING ","genderPk SAVED: "+String.valueOf(genderPK));
            Log.e("STTRRRING ","savedInstancePk SAVED: "+String.valueOf(savedInstancePk));
        } catch(Exception e){
            //do nothing
        }


    }

    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        instanceSaved=true;
        if(!accessories.isEmpty()) {
            adapter.sendAccessories();
        }else {
            Intent intent=new Intent("accessories_bundle");
            Bundle bundle=new Bundle();
            bundle.putSerializable("selectedAccessories",accessories);
            intent.putExtra("bundle",bundle);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcastSync(intent);
        }
        return null;
    }

    public void onStop(){
        super.onStop();
        instanceSaved=false;
        Log.e("STTRRRING ON STOP","RAN HERE 5");
    }

    @Override
    public void onSelected() {
        //update UI when selected
        if(instanceSaved==false){
            loadCustomAccessories();

        }else{
            if(accessories.size()==0){
                loadCustomAccessories();
            }
            Log.e("STTRRRING","genderPk: "+String.valueOf(genderPK));
            if(savedInstancePk!=genderPK){
                Log.e("STTRRRING","RAN HERE 3");

            }
            Log.e("ONFABRICSELECTED","instanceSaved:"+instanceSaved);
            instanceSaved=false;
        }
    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    public void loadLayout(){
        adapter=new AccessoriesListAdapter(accessories);
        recyclerView.setHasFixedSize(true);

        if (width>2560){
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL));


        }else if(width>1440 && width <= 2560){
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));

        }
        else{
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        };
        recyclerView.setAdapter(adapter);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        swipeRefreshLayout.setRefreshing(false);
    }

    public void loadCustomAccessories() {
        swipeRefreshLayout.setRefreshing(true);
        Call<ArrayList<CustomAccessories>> call=mService.getCustomAccessories(genderPK,styleGender);
        call.enqueue(new Callback<ArrayList<CustomAccessories>>() {
            @Override
            public void onResponse(Call<ArrayList<CustomAccessories>> call, Response<ArrayList<CustomAccessories>> response) {

                if(response.isSuccessful()) {
                    accessories.clear();
                    accessories = response.body();
                    if(accessories.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    loadLayout();
                    swipeRefreshLayout.setRefreshing(false);

                }else {
                    int statusCode  = response.code();
                    snackbar = Snackbar.make(coordinatorLayout, "Error connecting to server", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadCustomAccessories();
                        }
                    });
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CustomAccessories>> call, Throwable t) {
                snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadCustomAccessories();
                    }
                });
                snackbar.show();
                swipeRefreshLayout.setRefreshing(false);
            }

        });
    }

    public BroadcastReceiver mMessageReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!instanceSaved || genderPK==0){
                genderPK=intent.getIntExtra("designerPk",0);
                styleGender=intent.getStringExtra("styleGender");
                Log.e("ACS FRAG RECEIVED:","ACS FRAG RECEIVED:"+styleGender);
                Log.e("ACS FRAG RECEIVED:","DESIGNERPK FRAG RECEIVED:"+String.valueOf(genderPK));
            }



        }
    };

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        loadCustomAccessories();

    }
}
