package com.bolaandroid.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.OrderMeasurementAdapter;
import com.bolaandroid.adapter.SelectedAccessoriesAdapter;
import com.bolaandroid.data.model.CustomAccessories;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.CustomStyles;
import com.bolaandroid.data.model.DeliveryRate;
import com.bolaandroid.data.model.Measurement;
import com.bolaandroid.data.model.UserProfile;
import com.bolaandroid.data.model.Wardrobe;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.interfaces.ShowRegisterDialog;
import com.bolaandroid.interfaces.UpdateWardrobeCount;
import com.bolaandroid.taas.CustomActivity;
import com.bolaandroid.taas.R;
import com.bolaandroid.taas.WardrobeActivity;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.logging.StreamHandler;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;
import utils.PaymentProcessor;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends Fragment implements BlockingStep {

    CustomStyles style;
    CustomFabrics fabric;
    ProgressBar progressBar;
    int deliveryCharge;
    ArrayList<CustomAccessories> accessories;
    TextView fabricPrice,designerAddress,fabricBrand,fabricName,totalPrice,deliveryStatement,
        designerName,styleText,designerRating,stylePrice,userSelected,userSelected2;
    ImageView fabricImage,styleImage;
    RecyclerView recyclerView,recyclerView2;
    ScrollView scrollView;
    ArrayList<Measurement> selectedMeasurements=new ArrayList<>();
    int usersSelected=0;
    TextView comeMeasurePrice;
    CheckBox deliveryCheckBox;
    int totalprice,measuremePrice;
    Button addToBasketButton,orderButton,deliveryOkButton;
    boolean comeandmeasureme = false;
    TextView noAccessoriesText;
    EditText deliveryAddress,deliveryCity,deliveryState;
    RelativeLayout deliveryAddressLayout,comeMeasureLayout;
    LinearLayout addressLayout;
    RelativeLayout deliveryLayout;
    TextView addressView,deliveryRateView;
    String userAddress,userCity, userState,userCountry;
    Spinner countriesSpinner;
    public final String  MSG = "You are not logged in or add temporary address below";

    public ReviewFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Paper.init(getContext());
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("bundle_review"));
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver2,
                new IntentFilter("usersSelected"));
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver3,
                new IntentFilter("get-user-address-in-review-fragment"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v=inflater.inflate(R.layout.fragment_review, container, false);
        styleText=(TextView) v.findViewById(R.id.style_name);
        designerName=(TextView) v.findViewById(R.id.designer_name);
        designerAddress=(TextView) v.findViewById(R.id.designer_address);
        designerRating=(TextView) v.findViewById(R.id.designer_rating);
        fabricBrand=(TextView) v.findViewById(R.id.fabric_brand);
        fabricName=(TextView) v.findViewById(R.id.fabric_name);
        stylePrice = (TextView) v.findViewById(R.id.price);
        comeMeasurePrice = (TextView) v.findViewById(R.id.come_measure_me_price);
        scrollView=(ScrollView) v.findViewById(R.id.parent_container);
        totalPrice = (TextView) v.findViewById(R.id.total_price);
        fabricPrice= (TextView) v.findViewById(R.id.second_price);
        fabricImage=(ImageView) v.findViewById(R.id.fabric_image);
        styleImage=(ImageView) v.findViewById(R.id.image);
        deliveryOkButton = (Button) v.findViewById(R.id.delivery_ok_buttn);
        userSelected=(TextView) v.findViewById(R.id.usersSelected);
        deliveryAddress = (EditText) v.findViewById(R.id.addressEdit);
        deliveryCity = (EditText) v.findViewById(R.id.cityEdit);
        deliveryState = (EditText) v.findViewById(R.id.stateEdit);
        countriesSpinner = (Spinner) v.findViewById(R.id.countries_spinner);
        ArrayAdapter<CharSequence> adapter=
                ArrayAdapter.createFromResource(getContext(),R.array.country_array,
                        android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countriesSpinner.setAdapter(adapter);
        countriesSpinner.setSelection(adapter.getPosition("Nigeria"));
        deliveryAddress.addTextChangedListener(addressEditTextWatcher(deliveryAddress));
        deliveryCity.addTextChangedListener(cityEditTextWatcher(deliveryCity));
        deliveryState.addTextChangedListener(stateEditTextWatcher(deliveryState));
        deliveryCheckBox = (CheckBox) v.findViewById(R.id.address_checkbox);
        noAccessoriesText=(TextView) v.findViewById(R.id.no_accessories_text);
        deliveryStatement=(TextView) v.findViewById(R.id.delivery_statement);
        deliveryAddressLayout = (RelativeLayout) v.findViewById(R.id.address_container);
        userSelected2=(TextView) v.findViewById(R.id.usersSelected2);
        comeMeasureLayout = (RelativeLayout) v.findViewById(R.id.come_measure_me_layout);
        progressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        addressLayout = (LinearLayout) v.findViewById(R.id.customer_addresslayout);
        deliveryLayout = (RelativeLayout) v.findViewById(R.id.delivery_rates);
        addressView = (TextView) v.findViewById(R.id.customer_address);
        addressView.setHorizontallyScrolling(true);
        addressView.setSelected(true);
        deliveryRateView = (TextView) v.findViewById(R.id.delivery_price);
        Log.e("2","GBESKEBE");
        TextView textView1=(TextView) v.findViewById(R.id.delivery_statement);
        designerAddress.setHorizontallyScrolling(true);
        designerAddress.setSelected(true);
        designerName.setHorizontallyScrolling(true);
        designerName.setSelected(true);
        textView1.setHorizontallyScrolling(true);
        textView1.setSelected(true);
        recyclerView2 = (RecyclerView) v.findViewById(R.id.measurement_recycler);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView=(RecyclerView) v.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        orderButton = (Button) v.findViewById(R.id.button_order_products);
        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getActivity().getSharedPreferences("TAG", MODE_PRIVATE);
                if (prefs.contains("token")) {
                    if(!isItReadyToOrder()){
                        Toast.makeText(getContext(),"Please fetch your address and delivery fee first",Toast.LENGTH_LONG).show();
                        return;
                    }
                    String DATE_FORMAT_NOW = "yyyy.MM.dd-HH.mm.ss";
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                    String token=prefs.getString("token","random");
                    String email=prefs.getString("email","john@bosco.com");
                    String reference=style.getName().replaceAll("\\s+","")+fabric.getName().replaceAll("\\s+","")+token.substring(0, 2)+sdf.format(cal.getTime());
                    List<Integer> accessoriesPkArray=new ArrayList<>();
                    for (int counter = 0; counter < accessories.size(); counter++) {
                        accessoriesPkArray.add(accessories.get(counter).getPk());
                    }
                    String address;
                    if(addressView.getText().toString().trim().equals("")){
                        address = null;
                    }else{
                       address =  addressView.getText().toString().trim();
                    }
                    Boolean comeandmeasuremeValueSentToServer=false;
                    if(comeMeasureLayout.getVisibility()==View.VISIBLE){
                        comeandmeasuremeValueSentToServer= true;
                    }
                    PaymentProcessor paymentProcessor=new PaymentProcessor(v.getContext(),address,reference,email,(totalprice+deliveryCharge)*100,style.getPk(),fabric.getPk(),accessoriesPkArray,selectedMeasurements,comeandmeasuremeValueSentToServer,deliveryCharge);
                    paymentProcessor.showDialog();

                }else{
                    ((ShowRegisterDialog) getContext()).showRegisterDialog(true);
                }

            }
        });
        if (savedInstanceState != null) {
            usersSelected=savedInstanceState.getInt("usersSelected",1);
            Bundle bundle=new Bundle();
            bundle=savedInstanceState.getBundle("bundle");
            selectedMeasurements=(ArrayList<Measurement>) bundle.getSerializable("selectedMeasurements");
        }
        addToBasketButton=(Button) v.findViewById(R.id.button_add_to_basket);
        addToBasketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isItReadyToOrder()){
                    if(isLoggedIn()){
                        Toast.makeText(getContext(),"Please fetch your address and delivery fee first",Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getContext(),"Please login or add a temporary delivery address",Toast.LENGTH_LONG).show();
                    }

                    return;
                }
                Boolean comeandmeasuremeValueSentToServer=false;
                if(comeMeasureLayout.getVisibility()==View.VISIBLE){
                    comeandmeasuremeValueSentToServer= true;
                }
                String DATE_FORMAT_NOW = "yyyy.MM.dd-HH.mm.ss";
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                String reference=style.getName().replaceAll("\\s+","")+fabric.getName().replaceAll("\\s+","")+sdf.format(cal.getTime());
                Wardrobe wardrobe= new Wardrobe(reference,style,fabric,accessories,selectedMeasurements,comeandmeasuremeValueSentToServer,addressView.getText().toString(),deliveryCharge,totalprice+deliveryCharge);
                saveToDB(wardrobe,reference);
                ((UpdateWardrobeCount) getContext()).updateWardrobeCount();
            }
        });
        deliveryOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateNewAddressFields()){
                    addressView.setText(deliveryAddress.getText().toString()+", "+ deliveryCity.getText().toString() +", "+deliveryState.getText().toString()+", "+countriesSpinner.getSelectedItem().toString());
                    deliveryOkButton.setText("LOADING...PLEASE WAIT");
                    getDeliveryRate(deliveryCity.getText().toString(),
                            deliveryState.getText().toString(),
                            countriesSpinner.getSelectedItem().toString());
                }
            }
        });
        countriesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(deliveryCheckBox.isChecked()){
                    addressView.setText(deliveryAddress.getText().toString()+", "+ deliveryCity.getText().toString() +", "+deliveryState.getText().toString()+", "+countriesSpinner.getSelectedItem().toString());
                }
                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return v;
    }



    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        return null;
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        selectedMeasurements.clear();
        callback.goToPrevStep();
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onSelected() {
        //update UI when selected
        if(deliveryCheckBox.isChecked()){
            deliveryAddressLayout.setVisibility(View.VISIBLE);
        }else {
            deliveryAddressLayout.setVisibility(View.GONE);
        }
        deliveryCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deliveryCheckBox.isChecked()){
                    deliveryAddressLayout.setVisibility(View.VISIBLE);
                }else {
                    getAddressAndRate();
                    deliveryAddressLayout.setVisibility(View.GONE);
                }
            }
        });
        styleText.setText(style.getName());
        designerName.setText(style.getDesigner().getName());
        stylePrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(style.getCustomPrice()));
        designerAddress.setText(style.getDesigner().getAddress()+", "+style.getDesigner().getCity()+", "+style.getDesigner().getState()+", "+style.getDesigner().getCountry().getName());
        deliveryStatement.setText(style.getDesigner().getDeliveryStatement());
        totalprice = (style.getCustomPrice()+fabric.getPrice())*usersSelected ;
        for (int i = 0; i < accessories.size(); i++){
            int price=accessories.get(i).getPrice();
            totalprice += price;
            Log.d("price1",""+totalprice);
        }
        if(comeandmeasureme){
            comeMeasureLayout.setVisibility(View.VISIBLE);
            totalprice += measuremePrice;
            Log.d("price2",""+totalprice);
            comeMeasurePrice.setText("₦"+NumberFormat.getNumberInstance(Locale.US).format(measuremePrice));
            comeandmeasureme = false;
        }
        totalPrice.setText("₦"+NumberFormat.getNumberInstance(Locale.US).format(totalprice));
        userSelected2.setText("x"+String.valueOf(usersSelected));
        userSelected.setText("x"+String.valueOf(usersSelected));
        Picasso.with(getContext())
                .load(style.getPhoto1())
                .placeholder(new ColorDrawable(ContextCompat.getColor(getContext(),getRandomColor())))
                .into(styleImage);
        Log.e("ddd","which comes first");
        fabricBrand.setText(fabric.getBrand());
        fabricName.setText(fabric.getName());
        designerRating.setText("Rating: "+style.getDesigner().getAverageRating()+"/5.0");
        fabricPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(fabric.getPrice()));
        Picasso.with(getContext())
                .load(fabric.getPhoto1())
                .placeholder(new ColorDrawable(ContextCompat.getColor(getContext(),getRandomColor())))
                .into(fabricImage);
        if(!accessories.isEmpty()) {
            SelectedAccessoriesAdapter adapter = new SelectedAccessoriesAdapter(accessories);
            recyclerView.setAdapter(adapter);
        }else{
            noAccessoriesText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        Log.e("sshhiiitt222",String.valueOf(selectedMeasurements.size()));
        OrderMeasurementAdapter orderMeasurementAdapter=new OrderMeasurementAdapter(selectedMeasurements);
        recyclerView2.setAdapter(orderMeasurementAdapter);
        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView2.setLayoutManager(mLayoutManager2);
        if(isLoggedIn()){
            Log.e("1111","1111");
            getAddressAndRate();
            //if(deliveryCharge==0){
            //    Log.e("1111","2222");
            //    getAddressAndRate();
            //}else {
             //   Log.e("1111","3333");
            //    totalprice+=deliveryCharge;
             //   deliveryRateView.setText("₦"+NumberFormat.getNumberInstance(Locale.US).format(deliveryCharge));
            //    progressBar.setVisibility(View.GONE);
            //    totalPrice.setText("₦"+NumberFormat.getNumberInstance(Locale.US).format(totalprice));
            //}

        }else{
            Log.e("1111","4444");
            addressView.setText(MSG);
        }

    }

    public boolean isLoggedIn(){
        SharedPreferences prefs = getActivity().getSharedPreferences("TAG", MODE_PRIVATE);
        if (prefs.contains("token")) {return true;}
        else {
            return false;
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try{
            outState.putInt("usersSelected",usersSelected);
            Bundle bundle=new Bundle();
            bundle.putSerializable("selectedMeasurements",selectedMeasurements);
            outState.putBundle("bundle",bundle);
        } catch(Exception e){
            //do nothing
        }


    }

    public int getRandomColor(){
        List<Integer> colorz = Arrays.asList(R.color.ms_material_grey_400,
                R.color.ms_black_87_opacity,
                R.color.ms_material_blue_500,
                R.color.ms_errorColor,
                R.color.brown);
        Random random = new Random();
        return colorz.get(random.nextInt(colorz.size()));

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    public BroadcastReceiver mMessageReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle=new Bundle();
            bundle=intent.getBundleExtra("bundle");
            Log.e("ssseee","ssseee");
            if (bundle!=null) {
                style = (CustomStyles) bundle.getSerializable("style");
                fabric = (CustomFabrics) bundle.getSerializable("fabric");
                accessories = (ArrayList<CustomAccessories>) bundle.getSerializable("accessories");
            }
            if(fabric==null) {
                Log.e("F1", "no fabric here");
            }
        }
    };

    public BroadcastReceiver mMessageReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("comeandmeasureme is", String.valueOf(comeandmeasureme));
            Bundle bundle = new Bundle();
            bundle = intent.getBundleExtra("bundle");
            if (bundle != null) {
                if(!comeandmeasureme){
                    comeMeasureLayout.setVisibility(View.GONE);
                    selectedMeasurements.clear();
                    usersSelected = 0;
                }else{
                    comeMeasureLayout.setVisibility(View.VISIBLE);
                    totalprice += measuremePrice;
                    comeMeasurePrice.setText("₦"+NumberFormat.getNumberInstance(Locale.US).format(measuremePrice));
                    // comeandmeasureme = false;
                }
                if(bundle.getBoolean("comeandmeasureme")){
                    selectedMeasurements.clear();
                    usersSelected=0;
                    usersSelected = (int) bundle.getInt("usersSelected", 0);
                    selectedMeasurements = (ArrayList<Measurement>) bundle.getSerializable("selectedMeasurement");
                    Log.e("selctMeasurmets size is", String.valueOf(selectedMeasurements.size()));
                    Log.e("selctMeasurmets sizeis2", String.valueOf((int) ((ArrayList<Measurement>) bundle.getSerializable("selectedMeasurement")).size()));
                    measuremePrice = (int) bundle.getInt("measureMePrice", 1000);
                    comeandmeasureme = true;
                }else{
                    Log.e("comenmeasureme is true", "333333");
                    int receivedUsersSelected = (int) bundle.getInt("usersSelected", 0);
                    Log.e("1usersSelected is ", String.valueOf(usersSelected));
                    usersSelected = usersSelected + receivedUsersSelected;
                    Log.e("2usersSelected is ", String.valueOf(usersSelected));
                    Log.e("receivedUsersSelcted is", String.valueOf(receivedUsersSelected));
                    ArrayList<Measurement> receivedMeasurements = (ArrayList<Measurement>) bundle.getSerializable("selectedMeasurement");
                    Iterator<Measurement> iterator = receivedMeasurements.iterator();
                    while (iterator.hasNext()) {
                        Measurement measurement = iterator.next();
                        selectedMeasurements.add(measurement);
                    }

                    Log.e("2selctMeasurmts size is", String.valueOf(selectedMeasurements.size()));

                }
            }
        }
    };

    public BroadcastReceiver mMessageReceiver3= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getAddressAndRate();
        }
    };

    public void saveToDB(Wardrobe wardrobe,String reference){
        Paper.book("wardrobe").write(reference,wardrobe);
        final Snackbar snackbar = Snackbar.make(scrollView, "Successfully added to wardrobe", Snackbar.LENGTH_LONG);
        snackbar.setAction("VIEW", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getContext(),
                        WardrobeActivity.class);
                getContext().startActivity(myIntent);
            }
        });
        snackbar.show();
    }

    public void getAddressAndRate(){
        try {
            SharedPreferences pref= this.getContext().getSharedPreferences("TAG",MODE_PRIVATE);
            if(!pref.contains("email")){
                addressLayout.setVisibility(View.GONE);
                deliveryLayout.setVisibility(View.GONE);
            }else{
                addressLayout.setVisibility(View.VISIBLE);
                deliveryLayout.setVisibility(View.VISIBLE);
                getAddress();
            }
        }catch (Exception e){ }

    }

    public void getAddress(){
        progressBar.setVisibility(View.VISIBLE);
        deliveryRateView.setVisibility(View.GONE);
        TAASService mService = ApiUtils.getTAASService();
        Call<UserProfile> call = mService.getUserProfile();
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                if(response.isSuccessful()){
                    UserProfile userProfile = response.body();
                    userAddress = userProfile.getAddress();
                    userCity = userProfile.getCity();
                    userState = userProfile.getState();
                    userCountry = userProfile.getCountry().getName();
                    addressView.setText(userAddress+", "+userCity+", "+ userState+", "+ userCountry);
                    addressLayout.setVisibility(View.VISIBLE);
                    getDeliveryRate(userCity,userState,userCountry);
                }else {
                    final Snackbar snackbar = Snackbar.make(scrollView, "Error fetching your address", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getDeliveryRate(userCity,userState,userCountry);
                        }
                    });
                    snackbar.show();
                }

            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                final Snackbar snackbar = Snackbar.make(scrollView, "Error fetching your address", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getAddress();
                    }
                });
                snackbar.show();
            }
        });
    }

    public void getDeliveryRate(final String userCity, final String userState, final String userCountry){
        String designerCity,designerState,designerCountry;
        designerCity = style.getDesigner().getCity();
        designerState = style.getDesigner().getState();
        designerCountry = style.getDesigner().getCountry().getName();
        TAASService mService = ApiUtils.getTAASService();
        final Call<DeliveryRate> deliveryRateCall = mService.getDeliveryRate(userCity,userState,userCountry,designerCity,designerState,designerCountry);
        deliveryRateCall.enqueue(new Callback<DeliveryRate>() {
            @Override
            public void onResponse(Call<DeliveryRate> call, Response<DeliveryRate> response) {
                if (response.isSuccessful()){
                    deliveryOkButton.setText("SUBMIT ADDRESS");
                    Log.d("deliverytotalprice1",totalprice+"");
                    //totalprice -= deliveryCharge;
                    Log.d("deliveryCharge",deliveryCharge+"");
                    Log.d("deliverytotalprice2",totalprice+"");
                    try {
                        deliveryCharge = response.body().getRate();
                        Log.d("deliveryCharge2",deliveryCharge+"");
                    }catch (Exception e){
                        final Snackbar snackbar = Snackbar.make(scrollView, "Unable to get delivery fee for this address", Snackbar.LENGTH_LONG);
                        snackbar.setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getDeliveryRate(userCity, userState, userCountry);
                            }
                        });
                        snackbar.show();
                        return;
                    }
                    progressBar.setVisibility(View.GONE);
                    deliveryRateView.setVisibility(View.VISIBLE);
                    deliveryAddressLayout.setVisibility(View.GONE);
                    deliveryRateView.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(deliveryCharge));
                    totalPrice.setText("₦"+ NumberFormat.getNumberInstance(Locale.US).format(totalprice+deliveryCharge));
                    Log.d("deliverytotalprice3",totalprice+"");
                    deliveryLayout.setVisibility(View.VISIBLE);
                    orderButton.setEnabled(true);
                    addToBasketButton.setEnabled(true);
                }else {
                    final Snackbar snackbar = Snackbar.make(scrollView, "Error fetching delivery rate", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getDeliveryRate(userCity,userState,userCountry);
                        }
                    });
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<DeliveryRate> call, Throwable t) {
                final Snackbar snackbar = Snackbar.make(scrollView, "Error fetching delivery rate", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getDeliveryRate(userCity,userState,userCountry);
                    }
                });
                snackbar.show();
            }
        });
    }

    public TextWatcher addressEditTextWatcher(EditText editText){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(deliveryCheckBox.isChecked()){
                String country="";
                if(countriesSpinner.getSelectedItem()!=null){
                    country=countriesSpinner.getSelectedItem().toString();
                }
                addressView.setText(s.toString()+", "+ deliveryCity.getText().toString()+", "+deliveryState.getText().toString()+"," + country);
                progressBar.setVisibility(View.VISIBLE);
                deliveryRateView.setVisibility(View.GONE);
                if(validateNewAddressFields()){
                }
            }
            }
        };
    }

    public TextWatcher cityEditTextWatcher(EditText editText){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (deliveryCheckBox.isChecked()) {
                    String country = "";
                    if (countriesSpinner.getSelectedItem() != null) {
                        country = countriesSpinner.getSelectedItem().toString();
                    }
                    addressView.setText(deliveryAddress.getText().toString() + ", " + s.toString() + ", " + deliveryState.getText().toString() + ", " + country);
                    progressBar.setVisibility(View.VISIBLE);
                    deliveryRateView.setVisibility(View.GONE);
                    if (validateNewAddressFields()) {
                    }
                }
            }
        };
    }

    public TextWatcher stateEditTextWatcher(EditText editText){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (deliveryCheckBox.isChecked()) {
                    String country = "";
                    if (countriesSpinner.getSelectedItem() != null) {
                        country = countriesSpinner.getSelectedItem().toString();
                    }
                    addressView.setText(deliveryAddress.getText().toString() + ", " + deliveryCity.getText().toString() + ", " + s.toString() + ", " + country);
                    progressBar.setVisibility(View.VISIBLE);
                    deliveryRateView.setVisibility(View.GONE);
                    if (validateNewAddressFields()) {
                    }
                }
            }
        };
    }

    public boolean validateNewAddressFields(){
        if(deliveryAddress.getText().toString().length()!=0){
            deliveryAddress.setError(null);
        }else{
            Log.d("lllldaddress",deliveryAddress.getText().toString()+" ");
            Log.d("lllldaddress",deliveryAddress.getText().toString().length()+" ");
            deliveryAddress.setError("This field is compulsory");
            return false;
        }
        if(deliveryCity.getText().toString().length()!=0){
            deliveryState.setError(null);
        }else{
            Log.d("llllcity",deliveryCity.getText().toString()+" ");
            Log.d("llllcity",deliveryCity.getText().toString().length()+" ");
            deliveryCity.setError("This field is compulsory");
            return false;
        }
        if(deliveryState.getText().toString().length()!=0){
            deliveryState.setError(null);
        }else{
            Log.d("llllstate",deliveryState.getText().toString()+" ");
            Log.d("llllstate",deliveryState.getText().toString().length()+" ");
            deliveryState.setError("This field is compulsory");
            return false;
        }
        if(countriesSpinner.getSelectedItem()==null){
            return false;
        }
        userAddress = deliveryAddress.getText().toString().trim();
        userCity = deliveryCity.getText().toString().trim();
        userCountry = countriesSpinner.getSelectedItem().toString();
        return true;
    }

    private boolean isItReadyToOrder(){
        if(addressView==null || addressView.getText().toString().equals("") || addressView.getText().toString().equals(MSG)){
            return false;
        }
        if(deliveryRateView.getVisibility() != View.VISIBLE){
            return false;
        }
        return true;
    }
}
