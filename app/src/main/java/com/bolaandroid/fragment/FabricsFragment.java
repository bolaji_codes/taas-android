package com.bolaandroid.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bolaandroid.adapter.FabricsListAdapter;
import com.bolaandroid.data.model.CustomFabrics;
import com.bolaandroid.data.model.remote.TAASService;
import com.bolaandroid.taas.CustomActivity;
import com.bolaandroid.taas.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class FabricsFragment extends Fragment implements Step,SwipeRefreshLayout.OnRefreshListener {
    private static final String STATE_ITEMS = "items";
    private static final String STYLE_PK_KEY = "pk";
    RecyclerView recyclerView;
    private TAASService mService;
    public ArrayList<CustomFabrics> fabrics=new ArrayList<>();
    FabricsListAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;
    TextView noListView;
    boolean i=false;
    int savedInstancePk=0;
    boolean instanceSaved=false;
    int stylePk,oldPk=0;
    int height;
    int width;


    public FabricsFragment() {
        // Required empty public constructor


    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("fabric-params"));
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver2,
                new IntentFilter("fabric2-params"));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            ((CustomActivity) getContext()).getWindowManager()
                    .getDefaultDisplay()
                    .getMetrics(displayMetrics);
        }catch (Exception e){}
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        mService = ApiUtils.getTAASService();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v=inflater.inflate(R.layout.fragment_fabrics, container, false);
        recyclerView=(RecyclerView) v.findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        coordinatorLayout = (CoordinatorLayout) v.findViewById(R.id
                .layout_main);
        swipeRefreshLayout.setOnRefreshListener(this);
        noListView = (TextView) v.findViewById(R.id.no_list_text);
        snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE);
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if(swipeRefreshLayout.isRefreshing() || snackbar.isShown()){
                    Toast.makeText(getContext(), "Please wait a moment", Toast.LENGTH_SHORT).show();
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        if(savedInstanceState!=null){
            swipeRefreshLayout.setRefreshing(true);
            fabrics = (ArrayList<CustomFabrics>) savedInstanceState.getSerializable(STATE_ITEMS);
            savedInstancePk=savedInstanceState.getInt("savedInstancePk");
            instanceSaved=true;
            loadLayout();
            swipeRefreshLayout.setRefreshing(false);
        }


        //Toast.makeText(getContext(), "3:", Toast.LENGTH_SHORT).show();
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try{
            outState.putSerializable(STATE_ITEMS,fabrics);
            savedInstancePk=stylePk;
            outState.putInt("savedInstancePk",savedInstancePk);
        } catch(Exception e){
            //do nothing
        }
}



    @Override
    public VerificationError verifyStep() {
        //return null if the user can go to the next step, create a new VerificationError instance otherwise
        instanceSaved=true;
        return i ? null : new VerificationError("Select a fabric to proceed");
    }

    @Override
    public void onStop(){
        super.onStop();
        instanceSaved=false;
        Log.e("onstop","STYLEPK:"+stylePk+","+"OLDPK:"+oldPk);
    }

    @Override
    public void onPause(){
        super.onPause();
        oldPk=stylePk;
        Log.e("onpAUSE","STYLEPK:"+stylePk+","+"OLDPK:"+oldPk);
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.e("ONrESUME","STYLEPK:"+stylePk+","+"OLDPK:"+oldPk);
    }
    @Override
    public void onStart(){
        super.onStart();
        Log.e("ONSTART","STYLEPK:"+stylePk+","+"OLDPK:"+oldPk);
    }

    @Override
    public void onSelected() {
        if(instanceSaved==false){
            loadCustomFabrics();
        }else{
            if(savedInstancePk!=stylePk){
                loadCustomFabrics();
            }
            Log.e("ONFABRICSELECTED","instanceSaved:"+instanceSaved);
            instanceSaved=false;
        }

    }

    @Override
    public void onError(@NonNull VerificationError error) {
        //handle error inside of the fragment, e.g. show error on EditText
    }

    public void loadLayout(){
        adapter=new FabricsListAdapter(fabrics);
        recyclerView.setHasFixedSize(true);

        if (width>2560){
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL));


        }else if(width>1440 && width <= 2560){
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));

        }
        else{
            recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        };
        recyclerView.setAdapter(adapter);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        swipeRefreshLayout.setRefreshing(false);
    }

    public void loadCustomFabrics() {
        swipeRefreshLayout.setRefreshing(true);
        Call<ArrayList<CustomFabrics>> call=mService.getCustomFabrics(stylePk);
        call.enqueue(new Callback<ArrayList<CustomFabrics>>() {
            @Override
            public void onResponse(Call<ArrayList<CustomFabrics>> call, Response<ArrayList<CustomFabrics>> response) {

                if(response.isSuccessful()) {
                    fabrics.clear();
                    fabrics = response.body();
                    loadLayout();
                    if(fabrics.isEmpty()){
                        recyclerView.setVisibility(View.GONE);
                        noListView.setVisibility(View.VISIBLE);
                    }else {
                        noListView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    swipeRefreshLayout.setRefreshing(false);

                }else {
                    int statusCode  = response.code();
                    snackbar = Snackbar.make(coordinatorLayout, "Error connecting to server", Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loadCustomFabrics();
                        }
                    });
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }

            }

            @Override
            public void onFailure(Call<ArrayList<CustomFabrics>> call, Throwable t) {

                snackbar = Snackbar.make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadCustomFabrics();
                    }
                });
                snackbar.show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public BroadcastReceiver mMessageReceiver= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            stylePk=intent.getIntExtra("stylePk",0);
            //Toast.makeText(getContext(), "Heyy Wassup", Toast.LENGTH_SHORT).show();
            //loadCustomFabrics();

        }
    };

    public BroadcastReceiver mMessageReceiver2= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            i=intent.getBooleanExtra("ilm",true);
            //Toast.makeText(getContext(), "Heyy Wassup", Toast.LENGTH_SHORT).show();
            //loadCustomFabrics();

        }
    };

    @Override
    public void onRefresh() {
        // swipe refresh is performed, fetch the messages again
        loadCustomFabrics();


    }

}
